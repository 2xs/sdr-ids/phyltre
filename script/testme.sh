#!/bin/bash

cfreq=2410
frequencies="2402 2404 2406 2408 2410 2412 2414 2416 2418"
infile=output.cfil
outdir=test_extract
fullin=../traces/2410_20_g-3.cfile
all_csv="$outdir/all.csv"
full_csv="$outdir/full.csv"
stream_csv="$outdir/stream.csv"
chunk_csv="$outdir/chunk.csv"

[ -d "$outdir" ] || mkdir "$outdir"

>"$chunk_csv"
>"$stream_csv"
>"$all_csv"
>"$full_csv"

# Extracting individual channels by chunks
echo "**********Individual channels by chunks"
time (for freq in $frequencies; do
	echo "********** Extracting chan $freq"
	# Extract channel from cfil 
	./tools/chan_extract.py -i "$infile" -o /tmp/tmp.iq -p Bluetooth -f ${freq}e6 #> /dev/null
	./tools/bluetooth.py  -r 2e6 -i /tmp/tmp.iq -f 0
	cp /tmp/btsink.txt "$outdir/chunk_${freq}.txt"
	count=$(wc -l < "$outdir/chunk_${freq}.txt")
	echo "$freq;$count" >> "$chunk_csv"
done)

exit 0

# Extract all from cfil
echo "********** Extracting all..."
./tools/chan_extract.py -i "$infile" -o /tmp/all.iq -A 

# Extracting individual channels on full stream
echo "Extracting individual channels on full stream"
time (for freq in $frequencies; do
	echo "********** Extracting chan $freq"
	# Extract channel from cfil 
	./tools/chan_filter.py -i /tmp/all.iq -o /tmp/tmp.iq -p Bluetooth -f ${freq}e6
	./tools/bluetooth.py  -r 2e6 -i /tmp/tmp.iq -f 0 
	cp /tmp/btsink.txt "$outdir/stream_${freq}.txt"
	count=$(wc -l < "$outdir/stream_${freq}.txt")
	echo "$freq;$count" >> "$stream_csv"
done)

# Do all the job with gnuradio on power thresholded samples
echo "Do all the job with gnuradio on power thresholded samples"
time (for freq in $frequencies; do
	echo "counting ALL:$freq"
	./tools/bluetooth.py -r 20e6 -i /tmp/all.iq -f $((freq-cfreq))e6
	cp /tmp/btsink.txt "$outdir/all_${freq}.txt"
	count=$(wc -l < "$outdir/all_${freq}.txt")
	echo "$freq;$count" >> "$all_csv"
done)

# Do all the job with gnuradio on full sample
echo "Do all the job with gnuradio on full sample"
echo "Do all the job with gnuradio"
time (for freq in $frequencies; do
	echo "counting ALL:$freq"
	./tools/bluetooth.py -r 20e6 -i "$fullin" -f $((freq-cfreq))e6
	cp /tmp/btsink.txt "$outdir/full_${freq}.txt"
	count=$(wc -l < "$outdir/full_${freq}.txt")
	echo "$freq;$count" >> "$full_csv"
done)

