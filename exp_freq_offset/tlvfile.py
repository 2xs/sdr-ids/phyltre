#!/usr/bin/python
from struct import unpack, pack
import sys
import os
import numpy as np

class TLVFile:
	def __init__(self, fd):
		self.fd = fd

	def read_complex(self, n):
		x = np.fromfile(self.fd, np.complex64, n)
		return x

	def read_float(self, n):
		return np.fromfile(self.fd, np.float32, n)

	def read(self, n):
		return self.fd.read(n)

	def read_tl(self):
		dat = self.read(8)
		if dat:
			return unpack("<4sI",dat)

	def read_tv(self):
		tl = self.read_tl()
		if tl:
			t,l = tl
			if t == 'CFIL':
				v = unpack("<dddI", self.read(l))
			elif t == 'GTIM':
				v, = unpack("<Q", self.read(l))
			elif t == 'STIM':
				v, = unpack("<Q", self.read(l))
			elif t == 'NOIS':
				v, = unpack("<d", self.read(l))
			elif t == 'EVNT':
				v = list(self.parse_evt(self.read(l)))
			elif t == 'DATA':
				v = self.read_complex(l/8)
			elif t == 'DFFT':
				v = self.read_complex(l/8)
			elif t == 'IFSA':
				v = self.read_complex(l/8)
			elif t == 'PEAK':
				# time_ns, peak dB
				v = []
				assert(l % 16 ) == 0
				for i in range(l/16):
					x = unpack("<dff",self.read(16))
					v.append(x)
			elif t == 'FMDM':
				# time_ns, freq_offset, mod_index
				v = unpack("<dff",self.read(16))
			elif t == 'IFRQ':
				# instantaneous freq (# unk samp rate :())
				assert(l % 4 ) == 0
				v = self.read_float(l/4)
			elif t == 'BTLA':
				assert(l==16)
				# time_ns + bt_lap
				v = unpack("<dII", self.read(16))
			elif t == '8154':
				# 802.15.4 sfd time + crc_good
				v = unpack("<dI",self.read(12))
			elif t == 'LEAV': # LE advertisement
				assert(l>=12)
				c = self.read(l)
				time_ns, crc = unpack("<dI", c[:12])
				v = (time_ns, crc, c[12:])
			else:
				v = self.read(l)
			return t,v
			
	def parse_evt(self, evt):
		l = len(evt)
		assert(l != 0 and (l%16)==0)
		for i in range(0, l, 16):
			res = unpack("<IIff", evt[i:i+16])[:3]
			yield res
