#!/usr/bin/python
import os
import sys
import numpy as np
import random
from matplotlib import pyplot as plt
from tlvfile import TLVFile
from scipy.stats import skew, kurtosis,entropy
from scipy.signal import resample, resample_poly
from scipy import randn
from threading import Thread

OSR=4
SR = 1e6*OSR	# Adapted for bluetooth : bw=1e6 * osr=4

# Only consider time drift in ppm when its absolute value is under this threshold
MAX_HCO = 250 # ppm

DO_PLOT=False

def psk_demod(s):
	p,s = s[:-1], s[1:]
	return np.angle(s * p.conjugate())

def ferr_to_hz(fw,rate):
	return fw*SR / (2*np.pi)

class TraceStat:
	def __init__(self):
		self.prev_time = None
		self.center = np.fromfile("lap2gold.complex64", dtype=np.complex64)
		self.center_dem = psk_demod(self.center)
		#plt.plot(self.center_dem)
		#plt.show()
		self.skip_zero_snum = 0
		self.skip_big_hco = 0
		self.meas = []

	def count(self):
		return len(self.meas)

	def find_offset_sod(self, trace):
		lc = len(self.center_dem)
		dem = psk_demod(trace)
		num_dif = len(dem)-lc
		sod = np.zeros(num_dif,dtype=np.float32)
		for o in range(num_dif):
			sod[o] = np.sum((dem[o:o+lc] - self.center_dem)**2)
		return np.argmin(sod)

	# Poor man's trace synchronisation : sum-of-differences on fm demodulated signal
	def add_trace_cor(self, chan_num, time_ns, trace):
		center = self.center

		trace /= np.linalg.norm(trace)

		# Get sod on psk
		dem = psk_demod(trace)
		idx_sod = self.find_offset_sod(trace)

		# Preamble duration in nanoseconds
		Pdur = idx_sod*1e9/SR

		# Compute time drift based on sync time
		time_ns += Pdur
		
		if self.prev_time is None:
			self.prev_time = time_ns
			return
		td = time_ns - self.prev_time
		self.prev_time = time_ns
		SdurNs = 1e9/3200.
		Snum = round(td/SdurNs) # nsec
		Texp = Snum*SdurNs
		if Snum == 0:
			print ("Skip zero Snum td = %.1f usec"%(td / 1000))
			self.skip_zero_snum += 1
			return
		hco = 1e6*(td - Texp)/Texp # ppm
		if abs(hco) > MAX_HCO:
			self.skip_big_hco += 1
			return
		tsel = trace[idx_sod:idx_sod+len(center)]

		# Compute frequency offset
		ferr = np.average(psk_demod(tsel)) - np.average(psk_demod(center))
		chan_freq = (2402+chan_num) * 1e6
		freq_err_hz = ferr_to_hz(ferr, SR)

		# Converti hz / ppm
		freq_err_ppm = 1e6 * freq_err_hz / chan_freq

		#self.meas.append((time_ns, chan_num, freq_err_hz, Pdur, hco))
		self.meas.append((time_ns, chan_num, freq_err_ppm, Pdur, hco))

laps = set()

# Extract raw samples for all received burst 
# containing a bluetooth GIAC
def read_tlv(fname):
	# Ouvre le fichier tlv
	f = TLVFile(open(fname, 'rb'))
	# Parcours les enregistrements (type, valeur)
	while 1:
		tv = f.read_tv()
		if tv is None: 	break
		t,v = tv
		#print (t)
		if t == "IFSA": ifsa = v
		elif t == "FMDM":
			t, fof, midx = v
		elif t == "BTLA":
			t, pos, lap = v
			laps.add(lap)
			# Bluetooth reserved GIAC  = 0x9e8b33 (envoye pendant un scan)
			if lap == 0x9e8b33:
				print ("LAP: %x, fof=%.2f, midx=%.2f, size=%d"%(lap, fof, midx,len(ifsa)/OSR))
				yield t, ifsa

def main():
	# Repertoire des TLV source
	d = "/mnt/ramdisk/"

	# Cree l'object qui va stocker toutes les traces
	ta = TraceStat()

	# Pour chaque fichier du dossier de traces
	for fn in os.listdir(d):
		if not fn.startswith("Bluetooth"):
			continue
		# Recupere le numero de canal dans le nom
		chan_num = int(fn.split(".")[0].split("-")[1])

		for t, ifsa in read_tlv(d+fn):
			ta.add_trace_cor(chan_num, t, ifsa)
			if DO_PLOT:
				mag = np.abs(ifsa)
				fm = psk_demod(ifsa)
				f, ax = plt.subplots(2)
				ax[0].plot(mag)
				ax[1].plot(fm)
				plt.show()

	for l in laps:
		print("Lap: %x"%l)

	print ("%d packets"%ta.count())

	meas = sorted(ta.meas, key=lambda m: m[0])
	print( "(#%d packets, #%d invalid hco, #%d zero snum) "%(ta.count(), ta.skip_big_hco,ta.skip_zero_snum))
	t, chan, ferr, pdur, hco = zip(*meas)
	tsec = np.array(t)/1e9
	if ta.count():
		for (s, m) in (
			("Ferr", ferr),
			("Pdur", pdur),
			("HCO", hco)):
			print ("  %s: avg: %.4f, med: %.4f, std: %.4f, min: %.4f, max: %.4f"%(
				s, np.average(m), np.median(m), np.std(m), np.min(m), np.max(m)))
			

if __name__ == '__main__': main()
