cmake_minimum_required (VERSION 2.6)
project (PHYDET)

SET (CMAKE_C_COMPILER             "/usr/bin/gcc")

list(APPEND CMAKE_MODULE_PATH "${PROJECT_SOURCE_DIR}/cmake/modules")
add_definitions("-Wno-multichar -O3 -g -Wall -D_GNU_SOURCE")

find_package(FFTW3F REQUIRED)
find_package(SoapySDR REQUIRED)
find_package(Volk REQUIRED)
find_package(LIQUIDDSP)
find_package(BTBB REQUIRED)
find_package(Threads REQUIRED)

include_directories(
		lib/include
		${FFTW3F_INCLUDE_DIRS}
		${SOAPYSDR_INCLUDE_DIRS}
		${VOLK_INCLUDE_DIRS}
		${LIQUIDDSP_INCLUDE_DIRS}
		${BTBB_INCLUDE_DIRS}
		${THREADS_INCLUDE_DIRS}
	)
link_directories(${FFTW3F_LIBRARY_DIRS}
		${SOAPYSDR_LIBRARY_DIRS}
		${VOLK_LIBRARY_DIRS}
		${LIQUIDDSP_LIBRARY_DIRS}
		${BTBB_LIBRARY_DIRS}
		${THREADS_LIBRARY_DIRS}
	)

add_subdirectory(apps)
add_subdirectory(lib)
