#!/usr/bin/python
import sys
import numpy as np
from libphy.L1Conf import PhyDef, find_chan
from libphy.L1Parser import L1Parser
from optparse import OptionParser

MAX_CHUNK=0x10000

def main(options):
	chan = find_chan(options.chan_proto, options.chan_freq)
	chan.phy.set_cap_param(2410e6,20e6, -27.4)
	
	data = np.fromfile(options.infile,dtype=np.complex64)
	print("len data: %d"%len(data))
	data = chan.filter(data)
	data = chan.rotate(data)
	print("len data2: %d"%len(data))
	data.tofile(options.outfile)
	
if __name__ == '__main__':
	parser = OptionParser( usage="%prog: [options]")
	parser.add_option("-i",	type="str",		dest="infile")
	parser.add_option("-o",	type="str",		dest="outfile")
	parser.add_option("-p", type="str",		dest="chan_proto")
	parser.add_option("-f", type="float",	dest="chan_freq")
	(options, args) = parser.parse_args()
	if (options.infile is None or options.infile is None or 
		options.chan_proto is None or options.chan_freq is None):
		parser.print_help()
		sys.exit(1)
	main(options)
