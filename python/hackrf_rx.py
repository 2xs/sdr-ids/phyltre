#!/usr/bin/env python2
# -*- coding: utf-8 -*-
##################################################
# GNU Radio Python Flow Graph
# Title: Hackrf Rx
# Generated: Thu Nov 15 11:27:16 2018
##################################################

from gnuradio import blocks
from gnuradio import eng_notation
from gnuradio import filter
from gnuradio import gr
from gnuradio.eng_option import eng_option
from gnuradio.filter import firdes
from optparse import OptionParser
import osmosdr
import time
import sys


class hackrf_rx(gr.top_block):

	def __init__(self, options):
		gr.top_block.__init__(self, "Hackrf Rx")

		##################################################
		# Variables
		##################################################
		self.samp_rate = options.samp_rate
		self.freq = options.freq
		self.gain = options.gain
		self.outfile = options.outfile

		##################################################
		# Blocks
		##################################################
		self.osmosdr_source_0 = osmosdr.source( args="numchan=1")
		self.osmosdr_source_0.set_sample_rate(self.samp_rate)
		self.osmosdr_source_0.set_center_freq(self.freq, 0)
		self.osmosdr_source_0.set_freq_corr(0, 0)
		self.osmosdr_source_0.set_dc_offset_mode(0, 0)
		self.osmosdr_source_0.set_iq_balance_mode(0, 0)
		self.osmosdr_source_0.set_gain_mode(False, 0)
		self.osmosdr_source_0.set_gain(self.gain, 0)
		self.osmosdr_source_0.set_if_gain(0, 0)
		self.osmosdr_source_0.set_bb_gain(0, 0)
		self.osmosdr_source_0.set_antenna('', 0)
		self.osmosdr_source_0.set_bandwidth(0, 0)
		  
		self.dc_blocker_xx_0 = filter.dc_blocker_cc(128, True)
		self.blocks_file_sink_0 = blocks.file_sink(gr.sizeof_gr_complex*1, self.outfile, False)
		self.blocks_file_sink_0.set_unbuffered(False)

		##################################################
		# Connections
		##################################################
		self.connect((self.dc_blocker_xx_0, 0), (self.blocks_file_sink_0, 0))	
		self.connect((self.osmosdr_source_0, 0), (self.dc_blocker_xx_0, 0))	

	def get_samp_rate(self):
		return self.samp_rate

	def set_samp_rate(self, samp_rate):
		self.samp_rate = samp_rate
		self.osmosdr_source_0.set_sample_rate(self.samp_rate)

	def get_freq(self):
		return self.freq

	def set_freq(self, freq):
		self.freq = freq
		self.osmosdr_source_0.set_center_freq(self.freq, 0)


def main(top_block_cls=hackrf_rx, options=None):
	tb = top_block_cls(options)
	tb.start()
	try:
		raw_input('Press Enter to quit: ')
	except EOFError:
		pass
	tb.stop()
	tb.wait()


if __name__ == '__main__':
	parser = OptionParser(option_class=eng_option, usage="%prog: [options]")
	parser.add_option("-s", type="float", dest="samp_rate")
	parser.add_option("-f", type="float", dest="freq")
	parser.add_option("-g", type="int", dest="gain", default=8)
	parser.add_option("-o", type="str", dest="outfile")
	(options, args) = parser.parse_args()
	if None in (options.freq, options.samp_rate, options.outfile):
		print("Usage: %s -f freq -s samp_rate -g gain -o outfile"%sys.argv[0]);
		exit(1)
	main(options=options)
