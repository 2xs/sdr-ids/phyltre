#!/usr/bin/python
import sys
import csv
from matplotlib import pyplot as plt
from matplotlib.animation import FuncAnimation

def pb(b):
	return "".join(map(str,b))

class Line:
	def __init__(self,samp_time, bit_count, freq, msk_bt, snr, bits, lap):
		self.samp_time, self.bit_count, self.freq, self.msk_bt, self.snr, self.bits, self.lap = \
			samp_time, bit_count, freq, msk_bt, snr, bits, lap

	@staticmethod
	def parse(l):
		l = map(lambda s: s.strip(), l)
		samp_time, bit_count, freq, msk_bt, snr, bits, lap = l
		samp_time,bit_count,freq,msk_bt,snr = int(samp_time), int(bit_count),float(freq),float(msk_bt),float(snr)
		lap = int(lap,16) if lap else 0
		bits = map(int,bits)
		return Line(samp_time, bit_count, freq, msk_bt, snr, bits, lap)

	def __str__(self):
		return "@%9d| size %4d | freq %- 2.3f | bt %.4f | snr %- 2.2f | lap %8x | %s" %(
			self.samp_time, self.bit_count, self.freq,
			self.msk_bt, self.snr, self.lap, pb(self.bits[:100]))

colors = [
	[0,1,1],
	[0.5,0.5,0.5],
	[0.25,0.25,0.25],
	[.5,0,0],
	[1,.5,.5],
	[.5,1,.5],
]
laps = {0:[0,0,0],
	0xb2c866:[1,0,0],
	0x2f3b17:[0,1,0],
	0x73e25:[0,0,1],
	0xc27f8d:[1,0,1],
	}
	
lap_idx = 0
def get_color(lap):
	global laps, lap_idx
	col = laps.get(lap)
	if col is None:
		col = colors[lap_idx]
		laps[lap] = col
		lap_idx += 1
		print("lap (%x): col = %s"%(lap,repr(col)));
	return col

def main():
	global r, prev, z
	f = open(sys.argv[1])
	# skip header
	f.readline()
	r = csv.reader(f, delimiter=";")
	itv = 10
	if len(sys.argv) > 2:
		itv = int(sys.argv[2])

	pos = []
	#z = [(1,0,0)]
	z = []
	sizes = []
	fig, ax = plt.subplots(1,1)
	# freq
	ax.set_xlim(-0.3,0.3)
	#ax.set_xticks([])
	# bt
	#ax.set_xlim(0.5, .8), ax.set_xticks([])
	# adev
	#ax.set_xlim(0, 9), ax.set_xticks([])
	ax.set_ylim(0, 50)
	#, ax.set_yticks([])


	xs, ys, sizes, cols = [],[],[],[]

	for l in r:
		m = Line.parse(l)
		xs.append(m.freq)
		ys.append(m.snr)
		sizes.append(m.bit_count/2)
		col = get_color(m.lap)
		cols.append(col)
		

	scat = ax.scatter(xs,ys,c=cols,s=[sizes],alpha=0.5)
	plt.show()
	
		

if __name__ == '__main__': main()
