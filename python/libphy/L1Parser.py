#!/usr/bin/python
from struct import unpack, pack
import sys
import os
import numpy as np
from matplotlib import pyplot as plt
from scipy.misc import imresize
import scipy.signal
import scipy.misc
import ConfigParser
from tlvfile import TLVFile
from L1Conf import PhyDef

class L1Parser(TLVFile):
	def __init__(self, fd):
		TLVFile.__init__(self,fd)
		t,v = self.read_tv()
		assert(t == 'CFIL')
		self.freq, self.bw, self.noise_floor, self.fft_bin = v
		print("freq: %e, bw: %e, nf: %.2f dBm"%(self.freq, self.bw, self.noise_floor))
		self.chans = []
		while True:
			t,v = self.read_tv()
			if t != 'CDEF':
				break
			#print("chan %d: %s"%(len(self.chans),v))
			if len(v)==0:
				break
			phy,chan = v.split('/')
			self.chans.append(PhyDef.INST[phy].chans[chan])
		t,v = self.read_tv()
		assert(t == 'GTIM')
		self.gmt_time = v
		#print("GMT time: %d"%self.gmt_time)
		self.sample_time = 0
		PhyDef.set_cap_param(self.freq,self.bw,self.noise_floor)

	def read_all(self):
		while True:
			tv = self.read_tv()
			if tv is None: return
			t,v = tv
			r = 0
			if t == 'STIM':
				self.sample_time = v
			elif t == 'EVNT':
				r = self.handle_EVNT(v)
			elif t == 'DATA':
				r = self.handle_DATA(v)
				self.sample_time += len(v)
			elif t == 'DFFT':
				line = v
				r = self.handle_DFFT(v)
			if r:
				return r
