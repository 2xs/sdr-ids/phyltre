#!/usr/bin/python
from matplotlib import pyplot as plt
import numpy as np
from scipy.misc import imresize
import scipy.signal
import scipy.misc

if 1:
	from gnuradio import filter as gr_filter

	def make_filter(cap_bw, cutoff, twidth):
		print("make filter(cutoff=%e, twidth=%e)"%(cutoff,twidth))
		return gr_filter.firdes.low_pass(1.0, cap_bw, cutoff, twidth, gr_filter.firdes.WIN_HAMMING)
else:
	def make_filter(cap_bw, cutoff, twidth):
		print("make filter(cutoff=%e, twidth=%e)"%(cutoff,twidth))
		nyq_rate = cap_bw / 2.
		n,beta = scipy.signal.kaiserord(65, twidth*2/nyq_rate)
		return scipy.signal.firwin(n, cutoff/nyq_rate, window=('hann'))

def rolavg(it, n):
	return np.convolve(it, np.ones((n,))/n, mode='same')

def mag2(chunk):
	return chunk.real*chunk.real+chunk.imag*chunk.imag

def dofft(chunk):
	f = np.fft.fftshift(np.fft.fft(chunk))
	return f

def find_windows(y, threshold, min_dist):
	sup = np.array(y >= threshold,dtype=np.int)
	sup[0] = 0

	diff = np.diff(sup)
	state = 0
	last_d = last_u = -1
	down = []
	up = []
	up = list(np.nonzero(diff==1)[0])
	down = list(np.nonzero(diff==-1)[0])
	if len(down) < len(up):
		down.append(len(y)-1)
	windows = map(list,zip(up,down))
	i = 1
	while i < len(windows):
		prev = windows[i-1]
		cur=windows[i]
		dist = cur[0]-prev[1]
		if dist < min_dist:
			prev[1] = cur[1]
			windows.pop(i)
		else:
			i += 1
	return sorted(windows,key=lambda x:x[1]-x[0],reverse=True)

def easy_find_1(chunk, threshold, plot=False):
	D = 5
	A = 10
	cdec = chunk[::D]
	
	print("threshold: %f"%(threshold))
	# adec is mag squared of chunk decimated
	adec = mag2(cdec)

	# test vector is averaged mag2
	test = rolavg(adec,A)

	# find peaks 
	win = find_windows(test, threshold, A)

	if not win:
		return

	s,e=win[0]
	#s += A/4
	#e -= A/4
	s,e=s*D,e*D

	# Plotting
	if plot:
		f,ax=plt.subplots(1)
		#F = lambda a: 10*np.log10(a)
		F = lambda a: a
		ax.plot(F(mag2(chunk)),'b')
		ax.plot(np.arange(0,len(chunk),D),F(test), 'r')
		ax.axhline(y=F(threshold),color='r')
		ax.axvline(x=s,color='g')
		ax.axvline(x=e,color='r')
		plt.show()
	return s,e

def quad_demod(samples, osr):
	gain = osr*2/np.pi
	r1 = gain*np.angle(samples[1:]*samples[:-1].conjugate())
	return r1

def beta_pc_est(chunk_fft, bw):
	fft_bin = chunk_fft.shape[1]
	fft_avg = np.sum(chunk_fft,axis=0)

	# FIXME : keeping only 2401-2403 there
#	fft_avg = fft_avg[fft_bin/20:fft_bin*3/20]
	# FIXME : keeping only 2404-2404 there here 
	fft_avg = fft_avg[:fft_bin*4/20]
	total = np.sum(fft_avg)
	bound = total * 0.05 * 2

	ac = 0
	start, end = -1, -1
	for i in range(len(fft_avg)):
		ac += fft_avg[i]
		if ac > bound:
			start = i
			break
	ac = 0
	for i in range(len(fft_avg)-1,-1,-1):
		ac += fft_avg[i]
		if ac > bound:
			end = i
			break

	reso = bw / fft_bin

	width = reso * (end-start)

	if 0:
		print("fft avg (%d): "%(len(chunk_fft)/fft_bin))
		print("sum: %f"%total)
		print("start: %d, end: %d"%(start,end))
		print("width: %.2fMhz"%(width/1e6))
		su1 = np.cumsum(fft_avg)
		su2 = np.cumsum(fft_avg[::-1])[::-1]
		plt.plot(fft_avg,color='b')
		plt.plot(su1, color='g')
		plt.plot(su2, color='g')
		plt.axhline(y=bound, color='r')
		plt.show()
	return width
