#!/usr/bin/python
from struct import unpack, pack
import sys
import os
import numpy as np

class TLVFile:
	def __init__(self, fd):
		self.fd = fd

	def read_complex(self, n):
		x = np.fromfile(self.fd, np.float32, 2*n)
		x.resize(n,2)
		i,q = x.transpose()
		x = i+q*1j
		return x

	def read_float(self, n):
		return np.fromfile(self.fd, np.float32, n)

	def read(self, n):
		return self.fd.read(n)

	def read_tl(self):
		dat = self.read(8)
		if dat:
			return unpack("<4sI",dat)

	def read_tv(self):
		tl = self.read_tl()
		if tl:
			t,l = tl
			if t == 'CFIL':
				v = unpack("<dddI", self.read(l))
			elif t == 'GTIM':
				v, = unpack("<Q", self.read(l))
			elif t == 'STIM':
				v, = unpack("<Q", self.read(l))
			elif t == 'NOIS':
				v, = unpack("<d", self.read(l))
			elif t == 'EVNT':
				v = list(self.parse_evt(self.read(l)))
			elif t == 'DATA':
				v = self.read_complex(l/8)
			elif t == 'DFFT':
				v = self.read_complex(l/8)
			else:
				v = self.read(l)
			return t,v
			
	def parse_evt(self, evt):
		l = len(evt)
		assert(l != 0 and (l%12)==0)
		for i in range(0, l, 12):
			res = unpack("<IIf", evt[i:i+12])
			#print("chan: %d, evt: %d, val: %f"%res)
			yield res
