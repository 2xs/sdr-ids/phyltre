#!/usr/bin/python
import numpy as np
import ConfigParser
import os
from sigutil import make_filter
from gnuradio.filter import firdes
from gnuradio import filter

PHY_PATH="conf/phy"

class PhyChan:
	def __init__(self, phy, name, freq):
		self.phy = phy
		self.name = name
		self.freq = freq

	def build_filter1(self, cap_freq, cap_bw, taps):
		foff = self.freq - cap_freq
		taps2 = taps * np.exp(np.arange(0,len(taps),1.)*1j*fwT0)
		self.taps = np.array(taps2,dtype=np.complex64)
		self.fwT0 = 2 * np.pi * foff / cap_bw
		self.phase = 1

	def build_filter2(self, cap_freq, cap_bw, taps):
		foff = self.freq - cap_freq
		print("my_freq: %e, cap_freq: %e, cap_bw: %e, foff: %e"%(self.freq,cap_freq,cap_bw,foff))
		taps2 = firdes.complex_band_pass(1.0, cap_bw, foff-500e3, foff+500e3, 200e3, firdes.WIN_HAMMING, 6.76)
		self.taps = np.array(taps2,dtype=np.complex64)
		print("%d taps"%len(self.taps))
		self.fwT0 = 2 * np.pi * foff / cap_bw
		self.phase = 1

	def build_filter(self, cap_freq, cap_bw, taps):
		return self.build_filter2(cap_freq, cap_bw, taps)

	def filter(self, samples):
		return np.convolve(samples, self.taps, mode='same')

	def rotate(self, samples, dec=None):
		if dec is None: dec = self.phy.decimation
		ls = len(samples)
		lr = ls/dec
		res = np.zeros(lr, dtype=np.complex64)
		pi = np.exp(-1j * self.fwT0 * dec)
		pi /= np.abs(pi)
		i = 0
		while i < lr:
			res[i] = samples[i*dec] * self.phase
			i += 1
			self.phase *= pi
			if (i & 0x1ff) == 0: 	self.phase /= np.abs(self.phase)
		return res

	def __str__(self):
		return "%s.%s"%(self.phy.name,self.name)

class PhyDef:
	@staticmethod
	def load_all(path):
		d = {}
		for fn in os.listdir(path):
			if not fn.endswith(".ini"): continue
			pdef = PhyDef("%s/%s"%(path,fn))
			d[pdef.name] = pdef
		return d

	@staticmethod
	def set_cap_param(cap_freq,cap_bw, nf):
		for k,i in PhyDef.INST.items():
			i.set_cap_param_(cap_freq,cap_bw,nf)

	def __init__(self, fn):
		conf = ConfigParser.ConfigParser()
		conf.read(fn)
		self.name = conf.get('PhyDefinition','name')
		self.bwOut = conf.getfloat('PhyDefinition','bwOut')
		self.bwIn = conf.getfloat('PhyDefinition','bwIn')
		self.osr = conf.getint('PhyDefinition','osr')
		self.minDuration = conf.getfloat('PhyDefinition','minDuration')
		self.minSNR = conf.getfloat('PhyDefinition','minSNR')
		self.bitrate = conf.getfloat('PhyDefinition','bitrate')
		self.chans={}
		for k,v in conf.items('PhyChannels'):
			chan,freq=k,float(v)
			self.chans[chan] = PhyChan(self,chan,freq)

	def chan_by_freq(self,freq):
		for c in self.chans.values():
			if c.freq == freq:
				return c

	def set_cap_param_(self, cap_freq, cap_bw, nf):
		# Make proto taps for freq xlating filter
		print("set cap param %s"%repr(self.name))
		print("bwIn: %e, bwOut: %e, nf: %.2f"%( self.bwIn,self.bwOut, nf))
		#cutoff=self.bwIn/2.
		#twidth=self.bwOut-self.bwIn
		# FIXME: fix filter
		cutoff, twidth = 500e3, 200e3
		taps = make_filter(cap_bw, cutoff, twidth) 

		for c in self.chans.values():
			if not (c.freq - self.bwOut/2 < cap_freq-cap_bw/2
				or c.freq+self.bwOut/2 > cap_freq+cap_bw/2):
				c.build_filter(cap_freq,cap_bw,taps)
		self.decimation = cap_bw / (self.osr*self.bitrate)
		idec=int(self.decimation)
		if self.decimation != idec:
			print("Warning: Invalid decimation ratio %f"%self.decimation)
		print("decimation: %d"%idec)
		self.decimation = idec
		self.minSamples = int(self.minDuration * cap_bw)
		#print("Phy %s: minSamples=%d"%(self.name, self.minSamples))

		apwr = 10**(nf/10.)
		self.est_nf  = self.bwIn*apwr/cap_bw
		#print("apwr: %e, est nf: %e"%(apwr, self.est_nf))

	def __str__(self):
		return "PhyDef '%s': bw=%e"%(self.name,self.bwOut)
PhyDef.INST = PhyDef.load_all(PHY_PATH)

def find_chan(proto, freq):
	proto = PhyDef.INST.get(proto)
	if proto is None:
		print("Unknown proto %s, valid: %s"%(proto, ",".join(PhyDef.keys())))
		return None
	chan = proto.chan_by_freq(freq)
	if chan is None:
		print("Unknown freq %e for proto %s"%proto)
		return None
	return chan
