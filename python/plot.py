#!/usr/bin/python
import sys
import csv
from matplotlib import pyplot as plt
from matplotlib.animation import FuncAnimation

def pb(b):
	return "".join(map(str,b))

class Line:
	def __init__(self,samp_time, bit_count, freq, msk_bt, snr, bits, lap):
		self.samp_time, self.bit_count, self.freq, self.msk_bt, self.snr, self.bits, self.lap = \
			samp_time, bit_count, freq, msk_bt, snr, bits, lap

	@staticmethod
	def parse(l):
		l = map(lambda s: s.strip(), l)
		samp_time, bit_count, freq, msk_bt, snr, bits, lap = l
		samp_time,bit_count,freq,msk_bt,snr = int(samp_time), int(bit_count),float(freq),float(msk_bt),float(snr)
		lap = int(lap,16) if lap else 0
		bits = map(int,bits)
		return Line(samp_time, bit_count, freq, msk_bt, snr, bits, lap)

	def __str__(self):
		return "@%9d| size %4d | freq %- 2.3f | bt %.4f | snr %- 2.2f | lap %8x | %s" %(
			self.samp_time, self.bit_count, self.freq,
			self.msk_bt, self.snr, self.lap, pb(self.bits[:100]))

colors = [
	[1,0,0],
	[0,1,0],
	[0,0,1],
	[0,1,1],
	[1,0,1],
	[0.5,0.5,0.5],
	[0.25,0.25,0.25],
	[.5,0,0],
	[1,.5,.5],
	[.5,1,.5],
]
laps = {0:[0,0,0]}
lap_idx = 0
def get_color(lap):
	global laps, lap_idx
	col = laps.get(lap)
	if col is None:
		col = colors[lap_idx]
		laps[lap] = col
		lap_idx += 1
		print("lap (%x): col = %s"%(lap,repr(col)));
	return col

def main():
	global r, prev, z
	f = open(sys.argv[1])
	# skip header
	f.readline()
	r = csv.reader(f, delimiter=";")
	itv = 10
	if len(sys.argv) > 2:
		itv = int(sys.argv[2])

	pos = []
	#z = [(1,0,0)]
	z = []
	sizes = []
	fig, ax = plt.subplots(1,1)
	# freq
	ax.set_xlim(-0.3,0.3), ax.set_xticks([])
	# bt
	#ax.set_xlim(0.5, .8), ax.set_xticks([])
	# adev
	#ax.set_xlim(0, 9), ax.set_xticks([])
	ax.set_ylim(0, 50), ax.set_yticks([])

	scat = ax.scatter([],[],c=z,s=[sizes])
	prev = 0.
	def update(fn):
		global r, prev, z
		if r is None:
			return
		try:
			l = r.next()
		except StopIteration:
			print("Done reading")
			r = None
			f.close()
			return False
		m = Line.parse(l)
		
		#if m.lap == 0x9e8b33:
		#if m.lap == 0x7f139e:
	#	if m.lap != 0:
		if 1:
			dif_ms = round((m.samp_time - prev)/20.)
			prev = m.samp_time
			#print("+%5d usec | %s"%(dif_ms,str(m)))
			x = m.freq
	#		x = m.msk_bt
			y = m.snr
			pos.append((x,y))
			sizes.append(m.bit_count/2)
			#if len(sizes) == 1:
			#	z = [(1,0,0)] + z
			#elif len(sizes) == 2:
			#	z = [(0,1,0)] + z
			#else:
			#	z = [(0,0,1)] + z
			col = get_color(m.lap)
			z += [col]
			#sizes.append(20)
			scat.set_offsets(pos)
			scat.set_edgecolors(z)
			scat.set_sizes(sizes)
	anim = FuncAnimation(fig, update,interval=itv)
	plt.show()
	
		

if __name__ == '__main__': main()
