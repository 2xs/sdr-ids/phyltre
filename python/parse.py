#!/usr/bin/python
from struct import unpack, pack
import sys
import numpy as np
from matplotlib import pyplot as plt
from scipy.misc import imresize
import scipy.signal
import scipy.misc
import ConfigParser
from libphy.sigutil import *
from libphy.tlvfile import TLVFile
from libphy.L1Conf import PhyDef
from libphy.L1Parser import L1Parser

MAX_LINE = 0x10000
MAX_CHUNK=0x80000
MIN_H = 1024
#WIN = np.hamming(self.fft_bin)
PLOT=1
WATERFALL=1

class CapFile(L1Parser):
	def __init__(self, fd):
		L1Parser.__init__(self, fd, MAX_CHUNK)
		# FIXME
		self.prev_time = 0
		self.cur_match_count = 0
		self.image_idx = 0
		self.one_freq = self.more_freq = 0
		if WATERFALL:
			self.image = np.zeros((MAX_LINE, self.fft_bin), dtype=np.float32)
			self.image[::] = -100
		self.out_fd = open("/tmp/output.cfile","wb")
		self.out_phase = open("/tmp/output.phase","wb")

		self.cur_chunk = np.zeros((0),dtype=np.float32)
		self.cur_fft = np.zeros((0, self.fft_bin), dtype=np.float32)
		self.cur_det = set()

	def write_chunk(self, samples):
		for s in samples:
			self.out_fd.write(pack("<ff", s.real,s.imag))
		self.out_fd.flush()

	def write_phase(self, samples):
		samples.tofile(self.out_phase)

	@staticmethod
	def evt2str(samp_time,chan,val):
		return "%-28s"%("%s: %.2f"%(chan,val))

	def handle_EVNT(self, evts):
		for chan,event,value in evts:
			chan = self.chans[chan]
			if event:
				self.cur_match_count+=1
				self.cur_det.add((self.sample_time, chan,value))
			else:
				self.cur_match_count-=1

	def handle_DFFT(self, line):
		self.cur_fft = np.concatenate((self.cur_fft, [line]))

	def handle_DATA(self, chunk):
		self.cur_chunk = np.concatenate((self.cur_chunk, chunk))
		#print("lc: %d"%len(self.cur_chunk))
		maxed = len(self.cur_chunk) > MAX_CHUNK
		if maxed: print("maxed chunk!")
		if self.cur_match_count == 0 or maxed:
			# Chunk done, pass it to upper layer & reset 
			det = self.cur_det
			#det = filter(lambda a:a[2] >=1, det)
			det = sorted(det, key=lambda x:x[2],reverse=True)
			#print("det: %s"%repr(det))
			#print("det: %s"%repr(det))
			#det = self.cur_det
			if self.handle_full_chunk(self.cur_chunk, self.cur_fft, det):
				return True
			self.cur_chunk = np.array((),dtype=np.float32)
			self.cur_fft = np.zeros((0, self.fft_bin))
			self.cur_det = set()

	def handle_full_chunk(self, chunk, cfft, cdet):

		if len(cdet) == 0:
			return

		if WATERFALL:
			#for i in range(5):
				self.image_idx += 1
				if self.image_idx == MAX_LINE:
					return True

		if len(chunk) < 2000:
			return

		freq = list(c[1].freq for c in cdet)
		if len(set(freq)) > 1:
			#print("idx %5d| several freq: %s"%(self.image_idx,
			#	", ".join("%e: %.2f"%(c.freq,v) for c,v in cdet)))
			self.more_freq += 1
			return
		else:
			self.one_freq += 1

		det_str = "".join(CapFile.evt2str(*i) for i in cdet)
		print("idx %5d|chunk size: %5d | freq(%d) %4dMhz, Matches:    %s"%(self.image_idx,len(chunk), len(freq),freq[0]/1e6,det_str))

		#print("len(chunk): %d, freq=%.1fMhz"%(len(chunk),freq[0]/1e6))

		chan = cdet[0][1]

		# FIXME: hardcode bluetooth parameters
		chan = PhyDef.INST['Bluetooth'].chan_by_freq(chan.freq)

		#if len(chunk) > 100 and chan.freq == 2402e6:
		#	bw_est = beta_pc_est(cfft, self.bw)
		#else:
		bw_est = 0

		# Filter and rotate the chunk
		chunk = chan.filter(chunk)
		#chunk = chan.rotate(chunk, 1)

		win = 0, len(chunk)
		# Peak analysis
		sample_size = PhyDef.INST['Bluetooth'].minSamples
		win = easy_find_1(chunk, chan.phy.est_nf*2, True)#, self.image_idx > 8888)
		if win is None:
			return
		
		######## Phase analysis #######
		if 1:
			sel_start,sel_end=start,end = win
			peak_time_us = (start+self.sample_time)*1e6/self.bw

			peak_size = end-start
			bit_count = peak_size / (self.bw/chan.phy.bitrate)

			if peak_size < chan.phy.minSamples:
				print("xxxxxxxxxxxxxxxxxxxxxxxx short read %d"%bit_count)
				start, end = 0, len(chunk)
				#return

			# apply decimation to bounds
			start,end = start/D,end/D

			# quad demod
			chunk_dec = chunk[::D]
			chunk_freq = quad_demod(chunk_dec, (self.bw/D)/chan.phy.bitrate)

			# search psk modulation index 
			chunk_freq_samp = chunk_freq[start:end]
			psk_idx = np.std(chunk_freq_samp) / 2
			phase_med = np.average(chunk_freq_samp)
			chunk_freq -= phase_med
			print("%5d | %6d usec (+%6d)| %4dMhz | estBW %.2fMhz | psk idx: %.2f, med=% .1e, bitcount=%d"%(
				self.image_idx, peak_time_us, peak_time_us-self.prev_time,
				chan.freq/1e6, bw_est/1e6,
				psk_idx, phase_med, bit_count))
			self.prev_time = peak_time_us

		if WATERFALL:
			start,end = win
			start = start & ~(self.fft_bin-1)
			end = (end+self.fft_bin-1) & ~(self.fft_bin-1)
			if start < end:
				for i in range(start,end,self.fft_bin):
					self.image[self.image_idx] = 10*np.log10(self.cur_fft[i/self.fft_bin])
					self.image_idx += 1
					if self.image_idx == MAX_LINE:
						return True
	def plot(self):
		img = self.image
		if img.shape[1] < MIN_H:
			img = scipy.misc.imresize(img[:self.image_idx], (self.image_idx, MIN_H), interp='cubic')
		print("shape: %s"%repr(img.shape))
		plt.imshow(img.transpose())
		plt.show()

class WFFile(L1Parser):
	def __init__(self, fd, phase=False):
		L1Parser.__init__(self, fd)
		self.image_idx = 0
		self.image = np.zeros((MAX_LINE, self.fft_bin), dtype=np.complex64)
		self.phase = phase

	def handle_EVNT(self, evts):
		return
		for chan,event,value in evts:
			chan = self.chans[chan]
			print("%6d | evt=%d, val=%.2f, chan=%s"%(self.image_idx, event, value, chan))

	def handle_DFFT(self, line):
		line = np.fft.fftshift(line)
		self.image[self.image_idx] = line
		self.image_idx += 1
		if self.image_idx >= MAX_LINE:
			return True

	def handle_DATA(self, chunk):
		pass

	def plot(self, ax):
		img=self.image[:self.image_idx]
		if self.phase:
			#img = (img+np.pi)%(2*np.pi)
	#		img = np.diff(img,n=1,axis=1)
	#		img = np.diff(img,n=1,axis=0)
			#img=np.diff(img**2,axis=1)
			img = np.angle(img[1:]*img[:-1].conj())
			pass
		else:
			img = np.log(1e-38+mag2(img))
		img2=img
		if img2.shape[1] < MIN_H:
			img2 = scipy.misc.imresize(img2, (self.image_idx, MIN_H), interp='cubic')
		print("shape: %s"%repr(img2.shape))
		ax.imshow(img2.transpose())
		return img

class ChanExtractor(L1Parser):
	def __init__(self, fd, chan, out_fd):
		L1Parser.__init__(self, fd)
		self.out_fd = out_fd
		self.chan = chan

	def handle_EVNT(self, evts):
		return

	def handle_DFFT(self, line):
		pass

	def write_chunk(self, samples):
		#for s in samples:
		#	self.out_fd.write(pack("<ff", s.real,s.imag))
		samples.tofile(self.out_fd)
		self.out_fd.flush()

	def handle_DATA(self, chunk):
		#chunk = self.chan.filter(chunk)
		#chunk = self.chan.rotate(chunk)
		self.write_chunk(chunk)

def main(argv):
	if len(argv) != 2:
		print("Usage: %s <file>"%sys.argv[0])
		return 1

	with open(argv[1],'r') as f:
		cap = WFFile(f, phase=False)
		cap.read_all()
	f,ax=plt.subplots(2, sharex=True, sharey=True)
	img=cap.plot(ax[0])
	plt.show()
	#total = cap.more_freq + cap.one_freq
	#print("more freq: %d/%d ~ %.2f%%"%(cap.more_freq,total,cap.more_freq*100./(total+1)))
	return

	
def main2(argv):
	if len(argv) != 5:
		print("Usage: %s <in.cfil> <out.iq> <proto> <chan_num>"%sys.argv[0])
		return 1
	fi = open(argv[1],'r')
	fo = open(argv[2],'w')

	chan = PhyDef.INST[sys.argv[3]].chan_by_freq(float(sys.argv[4]))
	cap = ChanExtractor(fi,chan,fo)
	cap.read_all()
	cap.plot()
	plt.show()
	
if __name__ == '__main__': main(sys.argv)
