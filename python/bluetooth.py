#!/usr/bin/env python2
# -*- coding: utf-8 -*-
##################################################
# GNU Radio Python Flow Graph
# Title: Bluetooth
# Generated: Sat Oct  6 13:24:41 2018
##################################################

from gnuradio import analog
from gnuradio import blocks
from gnuradio import digital
from gnuradio import eng_notation
from gnuradio import filter
from gnuradio import gr
from gnuradio.eng_option import eng_option
from gnuradio.filter import firdes
from optparse import OptionParser
import btbr
import math
import pmt
import sys
from optparse import OptionParser

class bluetooth(gr.top_block):
	def __init__(self, options):
		gr.top_block.__init__(self, "Bluetooth")
		self.options = options

		##################################################
		# Variables
		##################################################
		self.samp_rate = samp_rate = int(self.options.samp_rate)

		self.taps = taps = firdes.low_pass(1.0, samp_rate, 500e3, 200e3, firdes.WIN_HAMMING, 6.76)

		self.samp_per_sym = samp_per_sym = 2
		self.decimation = int(self.samp_rate/(self.samp_per_sym*1e6))
		self.gain_mu = gain_mu = 0.175

		##################################################
		# Blocks
		##################################################
		self.do_filter = self.options.freq_offset != 0 or self.decimation != 1
		if self.do_filter:
			self.xlating_filter = filter.freq_xlating_fir_filter_ccc(self.decimation, (taps), self.options.freq_offset, samp_rate)
		self.digital_clock_recovery_mm_xx_0 = digital.clock_recovery_mm_ff(samp_per_sym, 0.25*gain_mu*gain_mu, 0.32, gain_mu, 0.005)
		self.digital_binary_slicer_fb_0 = digital.binary_slicer_fb()
		self.btbr_btsink_0 = btbr.btsink()
		self.btbr_btdecode_0 = btbr.btdecode(1)
		self.file_source = blocks.file_source(gr.sizeof_gr_complex*1, self.options.infile, False)
		self.file_source.set_begin_tag(pmt.PMT_NIL)
		self.analog_quadrature_demod_cf_0 = analog.quadrature_demod_cf(samp_per_sym/(2*math.pi))

		##################################################
		# Connections
		##################################################
		if 1:
			self.throttle = blocks.throttle(gr.sizeof_gr_complex*1,self.samp_rate*6)
			self.connect(self.file_source, self.throttle)
			self.src = self.throttle
		else:
			self.src = self.file_source
		if self.do_filter:
			self.connect(self.src, self.xlating_filter)
			self.connect(self.xlating_filter, self.analog_quadrature_demod_cf_0)
		else:
			self.connect(self.src, (self.analog_quadrature_demod_cf_0, 0))
		self.connect((self.analog_quadrature_demod_cf_0, 0), (self.digital_clock_recovery_mm_xx_0, 0))
		self.connect((self.digital_clock_recovery_mm_xx_0, 0), (self.digital_binary_slicer_fb_0, 0))
		self.connect((self.digital_binary_slicer_fb_0, 0), (self.btbr_btdecode_0, 0))
		self.msg_connect((self.btbr_btdecode_0, 'out'), (self.btbr_btsink_0, 'in'))

	def get_samp_rate(self):
		return self.samp_rate

	def set_samp_rate(self, samp_rate):
		self.samp_rate = samp_rate

	def get_taps(self):
		return self.taps

	def set_taps(self, taps):
		self.taps = taps
		self.xlating_filter.set_taps((self.taps))

	def get_samp_per_sym(self):
		return self.samp_per_sym

	def set_samp_per_sym(self, samp_per_sym):
		self.samp_per_sym = samp_per_sym
		self.digital_clock_recovery_mm_xx_0.set_omega(self.samp_per_sym)
		self.analog_quadrature_demod_cf_0.set_gain(self.samp_per_sym/(2*math.pi))

	def get_gain_mu(self):
		return self.gain_mu

	def set_gain_mu(self, gain_mu):
		self.gain_mu = gain_mu
		self.digital_clock_recovery_mm_xx_0.set_gain_omega(0.25*self.gain_mu*self.gain_mu)
		self.digital_clock_recovery_mm_xx_0.set_gain_mu(self.gain_mu)


def main(top_block_cls=bluetooth, options=None):
	tb = top_block_cls(options)
	tb.start()
	tb.wait()


if __name__ == '__main__':
	parser = OptionParser(option_class=eng_option, usage="%prog: [options]")
	parser.add_option("-r", type="float", dest="samp_rate")
	parser.add_option("-i", type="str", dest="infile")
	parser.add_option("-f", type="float", dest="freq_offset", default=0)
	(options, args) = parser.parse_args()
	main(options=options)
