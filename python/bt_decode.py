#!/usr/bin/env python2
# -*- coding: utf-8 -*-
##################################################
# GNU Radio Python Flow Graph
# Title: Bluetooth
# Generated: Sat Oct  6 13:24:41 2018
##################################################

from gnuradio import analog
from gnuradio import blocks
from gnuradio import digital
from gnuradio import eng_notation
from gnuradio import filter
from gnuradio import gr
from gnuradio.eng_option import eng_option
from gnuradio.filter import firdes
from optparse import OptionParser
import btbr
import math
import pmt
import sys
from optparse import OptionParser

class bluetooth(gr.top_block):
	def __init__(self, options):
		gr.top_block.__init__(self, "Bluetooth")
		self.options = options

		##################################################
		# Variables
		##################################################
		samp_per_sym = options.osr
		gain_mu = 0.175
		max_deviation = 50e-6

		##################################################
		# Blocks
		##################################################
		self.src = blocks.file_source(gr.sizeof_float, self.options.infile, False)
		self.symbol_sync = digital.symbol_sync_ff(digital.TED_ZERO_CROSSING, samp_per_sym, gain_mu, 1, 1.0, max_deviation, 1, digital.constellation_bpsk().base(), digital.IR_MMSE_8TAP, 32, ([]))
		self.binary_slicer = digital.binary_slicer_fb()
		self.btdecode = btbr.btdecode(1)
		self.btsink = btbr.btsink()

		##################################################
		# Connections
		##################################################
		self.connect(self.src, self.symbol_sync)
		self.connect(self.symbol_sync, self.binary_slicer)
		self.connect(self.binary_slicer, self.btdecode)
		self.msg_connect((self.btdecode, 'out'), (self.btsink, 'in'))


def main(top_block_cls=bluetooth, options=None):
	tb = top_block_cls(options)
	tb.start()
	tb.wait()

if __name__ == '__main__':
	parser = OptionParser(option_class=eng_option, usage="%prog: [options]")
	parser.add_option("-i", type="str", dest="infile")
	parser.add_option("-o", type="int", dest="osr",default=2)
	(options, args) = parser.parse_args()
	main(options=options)
