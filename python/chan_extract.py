#!/usr/bin/python
import sys
import numpy as np
from libphy.L1Conf import PhyDef, find_chan
from libphy.L1Parser import L1Parser
from libphy.sigutil import *
from optparse import OptionParser

MAX_CHUNK=0x10000

class ChanExtractor(L1Parser):
	def __init__(self, fd, out_fd, chan=None):
		L1Parser.__init__(self, fd)
		self.out_fd = out_fd
		self.chan = chan
		self.cur_match = 0
		self.stop_match = 0
		self.cur_chunk = np.zeros((MAX_CHUNK), dtype=np.complex64)
		self.cur_len = 0
		self.chunk_count = 0

	def set_chan(self, chan):
		self.chan = chan

	def handle_EVNT(self, evts):
		for chan,event,value in evts:
			if self.chans[chan] == self.chan:
				if event:
					self.cur_match = 1
				else:
					if self.cur_match:
						self.stop_match = 1

	def handle_DFFT(self, line):
		pass

	def write_chunk(self, samples):
		samples.tofile(self.out_fd)

	def handle_full_chunk(self, chunk):	
		CROP=28
		self.chunk_count += 1
		print("chunk %d"%self.chunk_count)
		chunk = self.chan.filter(chunk)
		chunk = self.chan.rotate(chunk,dec=1)
		win = easy_find_1(chunk, self.chan.phy.est_nf*2, True)
		if win is not None:
			s,e = win
			chunk = chunk[s:e]
		self.write_chunk(chunk)
		phase = quad_demod(chunk, 20)
		pavg,pstd=np.average(phase[CROP:-CROP]),np.std(phase[CROP:-CROP])
		print("phase: avg=%.4f, std=%.4f"%(pavg,pstd))
		if abs(pstd-0.5) <= 0.1:
			f,ax=plt.subplots(3)
			ax[0].plot(mag2(chunk))
			phase2 = phase - pavg
			pulses = np.diff(phase2 > 0)**2
			ax[1].plot(phase2)
			ax[1].plot(pulses)
			fft = np.fft.fft(pulses)[:30]
			ax[2].plot(np.abs(fft))
			plt.show()

	def handle_DATA(self, chunk):
		if self.cur_match:
			n = min(len(chunk), MAX_CHUNK-self.cur_len)
			self.cur_chunk[self.cur_len:self.cur_len+n] = chunk[:n]
			self.cur_len += n
			if self.cur_len == MAX_CHUNK or self.stop_match:
				self.handle_full_chunk(self.cur_chunk[:self.cur_len])
				self.cur_chunk = np.zeros((MAX_CHUNK), dtype=np.complex64)
				self.cur_len = 0
				self.stop_match = 0
				self.cur_match = 0

def main(options):
	fi = open(options.infile,'r')
	fo = open(options.outfile,'w')
	cap = ChanExtractor(fi,fo)
	if options.extract_all:
		chan = None
	else:
		chan = find_chan(options.chan_proto, options.chan_freq)
		if chan is None:
			return 1
		if chan not in cap.chans:
			print("Channel %s not present in capture file %s"%(chan,cap))
			return 1
		print("Selected chan: %s"%chan)
		cap.set_chan(chan)
	cap.read_all()
	
if __name__ == '__main__':
	parser = OptionParser( usage="%prog: [options]")
	parser.add_option("-i",	type="str",		dest="infile")
	parser.add_option("-o",	type="str",		dest="outfile")
	parser.add_option("-A", action="store_true",	dest="extract_all",	default=False)
	parser.add_option("-p", type="str",		dest="chan_proto")
	parser.add_option("-f", type="float",	dest="chan_freq")
	(options, args) = parser.parse_args()
	if ((options.infile is None or options.infile is None) or 
		(not options.extract_all and(options.chan_proto is None or options.chan_freq is None))):
		parser.print_help()
		sys.exit(1)
	main(options)
