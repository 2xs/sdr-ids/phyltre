#!/usr/bin/python
import numpy as np
from matplotlib import pyplot as plt
from scipy.misc import imresize
import sys

MAX_LENGTH = 50000

def main():
	if len(sys.argv) != 3:
		print("Usage: %s <width> <file.raw>"%(sys.argv[0]))
		return 1
	width = int(sys.argv[1])

	with open(sys.argv[2]) as fd:
		f = np.fromfile(fd, np.float32, width*MAX_LENGTH)

	# split lines
	f.resize((f.shape[0]/width, width))

	f = 10 * np.log10(f)

	MIN_H = 1024
	if width < MIN_H:
		f = imresize(f, (f.shape[0], MIN_H))

	print(repr(f))

	# rotate 90 so that x=time & y=freq
	f=f.transpose()
	print("shape: %s"%repr(f.shape))
	plt.imshow(f)
	plt.show()


if __name__ == '__main__': main()
