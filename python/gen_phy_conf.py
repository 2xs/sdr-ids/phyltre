#!/usr/bin/python

def gen_btle1M():
	cmap = [37] + range(0,11) + [38] + range(11,37) + [39]
	print("[PhyChannels]")
	for i in range(40):
		print'%d=%de6'%(cmap[i],2402+2*i)

def gen_802_15_4():
	print("[PhyChannels]")
	for i in range(15):
		print('%d=%de6'%(i+11,2405+i*5))

def gen_bt():
	print("[PhyChannels]")
	for i in range(79):
		print('%d=%de6'%(i,2402+i))

def main():
	gen_btle1M()
	gen_802_15_4()
	gen_bt()

if __name__ == '__main__': main()
