#!/usr/bin/env python2
# -*- coding: utf-8 -*-
##################################################
# GNU Radio Python Flow Graph
# Title: Bluetooth
# Generated: Sat Oct  6 13:24:41 2018
##################################################

from gnuradio import analog
from gnuradio import blocks
from gnuradio import digital
from gnuradio import eng_notation
from gnuradio import filter
from gnuradio import gr
from gnuradio.eng_option import eng_option
from gnuradio.filter import firdes
from optparse import OptionParser
import msgutil
import numpy as np
import btbr
import math
import pmt
import sys
from optparse import OptionParser
from struct import unpack
import os.path
from time import sleep

USE_TAG_DEBUG = False
USE_BT_DECODE = True

class btdemod(gr.top_block):
	def __init__(self):
		gr.top_block.__init__(self)

		##################################################
		# Variables
		##################################################
		samp_per_sym = 2
		gain_mu = 0.175
		max_deviation = 50e-6

		self.msgq_in = gr.msg_queue(32)
		self.msgq_out = gr.msg_queue(32)

		##################################################
		# Blocks
		##################################################

		self.src = msgutil.msgsrc(gr.sizeof_float, "pdu_start", "pdu_end", self.msgq_in)
		self.sink = msgutil.msgsink(gr.sizeof_char, "pdu_start", 'pdu_end', 0x1000, self.msgq_out)
		self.symbol_sync = digital.symbol_sync_ff(digital.TED_ZERO_CROSSING,
				samp_per_sym, gain_mu, 1, 1.0, max_deviation, 1, digital.constellation_bpsk().base(),
				digital.IR_MMSE_8TAP, 32, ([]))
		self.binary_slicer = digital.binary_slicer_fb()
		if USE_TAG_DEBUG:
			self.tag_debug = blocks.tag_debug(gr.sizeof_float*1, 'input', "")
			self.tag_debug.set_display(True)
			self.tag_debug2 = blocks.tag_debug(gr.sizeof_float, 'output', "")
			self.tag_debug2.set_display(True)

		if USE_BT_DECODE:
			self.btdecode = btbr.btdecode(1)
			self.btsink = btbr.btsink()

		##################################################
		# Connections
		##################################################

		self.connect(self.src, self.symbol_sync)
		self.connect(self.symbol_sync, self.binary_slicer)
		#self.connect(self.src, self.binary_slicer)
		self.connect(self.binary_slicer, self.sink)
		if USE_BT_DECODE:
			self.connect(self.binary_slicer, self.btdecode)
			self.msg_connect((self.btdecode, 'out'), (self.btsink, 'in'))
		if USE_TAG_DEBUG:
			self.connect(self.src, self.tag_debug)
			self.connect(self.symbol_sync, self.tag_debug2)

	def tx_data(self, data):
		if self.msgq_in.full_p():
			return False
		bdata = data.tobytes()
		#print("tx data: len = %d (%d)"%(len(data),len(bdata)))
		msg = gr.message_from_string(bdata)
		self.msgq_in.insert_tail(msg)
		return True

	def finalize(self):
		# No choice ? flush pipeline 
		pad = np.zeros(0x40)
		while not self.tx_data(pad):		sleep(.01)
		while not self.tx_data(pad):		sleep(.01)
		while not self.tx_data(pad):		sleep(.01)
		while not self.tx_data(pad):		sleep(.01)
		# close source  
		self.msgq_in.insert_tail(gr.message(1,0,0,0))

	def rx_data(self):
		if self.msgq_out.empty_p():
			return None
		data = self.msgq_out.delete_head().to_string()
		return data#list(map(ord,data))

class Pkt:
	def __init__(self,samp_time,ferr,pwr,win):
		self.samp_time,self.ferr,self.pwr,self.win=samp_time,ferr,pwr,win

	@staticmethod
	def read(fp):
		dat = fp.read(20)
		if not dat: return
		samp_time,ferr,pwr,win_sz = unpack("<QffI",dat)
		win=np.fromfile(fp,np.float32,win_sz)
		return Pkt(samp_time,ferr,pwr,win)

	def __str__(self):
		return "@%- 8d| freq=%- .3f, pwr=%- .2fdB, size=%d"%(self.samp_time,self.ferr,self.pwr,len(self.win))

def main(infile):
	tb = btdemod()
	tb.start()
	fp = open(infile)
	pktq = []
	next_pkt = None
	stopped = False
	prev_time = 0
	while True:
		if fp is not None:
			if next_pkt is None:
				next_pkt = Pkt.read(fp)
				if next_pkt is None:
					fp.close()
					fp = None
		if next_pkt is not None:
				if tb.tx_data(next_pkt.win):
					pktq.append(next_pkt)
					next_pkt = None
		else:
			# Flush the pipeline
			if not stopped:
				tb.finalize()
				stopped = True
		if not pktq:
			break
		else:
			bits = tb.rx_data()
			if bits is not None:
				pkt = pktq.pop(0)
				tdelta = pkt.samp_time - prev_time
				tdelta /= 20000.
				prev_time = pkt.samp_time
				print("+%-3.2f ms | %s -> %d : %s"%(tdelta,pkt,len(bits),"".join(map(lambda k:str(ord(k)),bits[:164]))))
	#tb.stop()
	tb.wait()

if __name__ == '__main__':
	parser = OptionParser(option_class=eng_option, usage="%prog: [options]")
	parser.add_option("-i", type="str", dest="infile")
	(options, args) = parser.parse_args()
	if options.infile is None or not os.path.isfile(options.infile) :
		parser.print_help()
		sys.exit(1)
	main(options.infile)

