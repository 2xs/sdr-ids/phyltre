#!/usr/bin/python
USE_PYFFTW=True
from matplotlib import pyplot as plt
from matplotlib import animation
if USE_PYFFTW:
	import fftw3f
import numpy as np
import sys

INTERVAL=1
N = 512

def load(f,n):
	s = np.fromfile(f,np.complex64,n)
	return s

if USE_PYFFTW:
        #create arrays
	inputa = np.zeros(N, dtype=np.complex64)
	outputa = np.zeros(N, dtype=np.complex64)

	# create a forward and backward fft plan
	fftp = fftw3f.Plan(inputa,outputa, direction='forward', flags=['measure'])
	ifftp = fftw3f.Plan(outputa, inputa, direction='backward', flags=['measure'])

	#initialize the input array
	inputa[:] = 0

	#perform a forward transformation
	fftp.execute() # alternatively fft.execute() or fftw.execute(fft)



tmp = np.zeros(N,np.float32)
def do_fft(s):
	global tmp
	s = s/N
	if USE_PYFFTW:
		inputa[:] = s
		fftp.execute()
		f = outputa
	else:
		f = np.fft.fft(s)
#	f[0] = (f[1]+f[-1])/2
	f = np.abs(f)**2
	
	if 1:
		tmp[:] = 10*np.log10(f)
	#	tmp *= 10
		return np.fft.fftshift(tmp)
#10*np.log10(f))
	else:
		return np.fft.fftshift(10*np.log10(f))

if False:
	while True:
		s = load(sys.stdin, N)
		if len(s) != N:
			sys.exit(0)
		y = do_fft(s)

x=np.arange(N)
fig = plt.figure()
line, = plt.plot([],[])

plt.xlim(0,N)
plt.ylim(-100,0)

alpha = 1
if len(sys.argv)>1:
	alpha = float(sys.argv[1])

def init():
	line.set_data([],[])
	return line,

val = np.zeros(N)

def animate(i):
	global val
	s = load(sys.stdin, N)
	if len(s) != N:
		exit(0)
	y = do_fft(s)
	val = y*(alpha)+val*(1-alpha)
	line.set_data(x,val)
	return line,

ani = animation.FuncAnimation(fig,animate,init_func=init,interval=INTERVAL,repeat=False)
plt.show()
