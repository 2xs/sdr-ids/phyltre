#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <assert.h>
#include <getopt.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/sendfile.h>
#include <phydet/tlv_file.h>
#include <phydet/cfil.h>
#include <phydet/phydef.h>
#include <phydet/chan_phy.h>
#include <phydet/config.h>
#include <phydet/log.h>

#define PHY_CONF "conf/phy"

#define W2C(c) (c&0xff),(((c)>>8)&0xff),(((c)>>16)&0xff),(((c)>>24)&0xff)

typedef struct {
	size_t fcount;
	size_t ecount;
	int state;
} chan_info_t;

static int extract_all(cfil_p cfil, int outfd)
{
	int rc = 0;
	u32 fbufsize = cfil->hdr.fft_bin*sizeof(complex_t);
	complex_t *fbuf = malloc(fbufsize);

	printf("Extracting all channels\n");
	while (1){
		if(cfil_read_chunk(cfil, fbuf, cfil->hdr.fft_bin, NULL))
			break;

		if(write(outfd, fbuf, fbufsize) != fbufsize)
		{
			LOG_ERROR("Copy error");
			rc = -1;	
			break;
		}
	}
	free(fbuf);
	return rc;
}

static int extract_stats(cfil_t *cfil, int show_events)
{
	u32 cid;
	u32 i, total_chunk = 0, total_evt = 0;
	chan_info_t *infos = calloc(cfil->chan_count,sizeof(chan_info_t));
	u32 fbufsize = cfil->hdr.fft_bin*sizeof(complex_t);
	complex_t *fbuf = malloc(fbufsize);

	while (1){
		if(cfil_read_chunk(cfil, fbuf, cfil->hdr.fft_bin, NULL))
		{
			/* EOF: print stats */
			printf("Total: %d chunks\n", total_chunk);
			for(i=0; i<cfil->chan_count; i++)
			{
				printf("  %-20s : event count = %-8lu, occupation = %3lu/%-8u ~ %2.2f%%\n", 
					cfil->chans[i],  infos[i].ecount, infos[i].fcount, total_chunk, infos[i].fcount*100.f / total_chunk);
			}
			printf("total: %u events\n",total_evt);
			free(infos);
			free(fbuf);
			return 0;
		}
		for(i=0;i<cfil->event_count;i++){
			cid = cfil->events[i].chan_index;
			if(cfil->events[i].event){
				infos[cid].state = 1;
				infos[cid].ecount++;
				if(show_events)
					printf("%20s  -  start (%- 2.2f)\n", cfil->chans[cid], cfil->events[i].value);
			}
			else{
				infos[cid].state = 0;
				infos[cid].fcount++;
				if(show_events)
					printf("%20s  -  stop\n", cfil->chans[cid]);
			}
		}
		for(i=0;i<cfil->chan_count;i++)
			if (infos[i].state)
				infos[i].fcount++;
		total_evt += cfil->event_count;
		total_chunk++;
	}
}

static int extract_chan(cfil_p cfil, int outfd, int chan_sel, chan_phy_adapt_p cadapt, long unsigned pad)
{
	int i, do_output = 0, stop_output=0;
	u32 chan_evt_count = 0;
	u32 wcount, fcount = cfil->hdr.fft_bin;
	u32 amax=0, acount;
	complex_t *abuf = NULL, *fbuf, *wbuf;
	#define NULCNT 0x2000
	const complex_t *nulbuf = NULL;
	float resamp_rate;

	if (pad)
		nulbuf = calloc(1, sizeof(complex_t)*pad);
	

	printf("Extracting channel %d\n", chan_sel);

	fbuf = malloc(fcount*sizeof(complex_t));
	if (cadapt != NULL)
	{
		resamp_rate = cadapt->out_sr / cadapt->in_sr;
		amax = (u32)ceil(resamp_rate * fcount);
		abuf = malloc(amax*sizeof(complex_t));
	}
	
	while (1){
		if(cfil_read_chunk(cfil, fbuf, cfil->hdr.fft_bin, NULL))
		{
			printf("%d events\n", chan_evt_count);
			free(fbuf);
			break;
		}
		for(i=0;i<cfil->event_count;i++)
		{
			if (cfil->events[i].chan_index == chan_sel)
			{
				if(cfil->events[i].event)
				{
					do_output = 1;
					chan_evt_count++;
				}
				else
					if(do_output)
						stop_output = 1;
				/* There is maximum one event per channel, we can break*/
				break;
			}
		}
		if(do_output)
		{
			if (cadapt != NULL)
			{
				/* adapt samples */
				acount = chan_phy_adapt_samples(cadapt, abuf, fbuf, fcount, amax);
				wbuf = abuf;
				wcount = acount;
			}
			else
			{
				wbuf = fbuf;
				wcount = fcount;
			}
			if(write(outfd, wbuf, wcount*sizeof(complex_t)) < 0)
			{
				perror("write error");
				return 1;
			}
			if(stop_output)
			{
				do_output=0;
				stop_output=0;
				if(pad)
					write(outfd, nulbuf, pad*sizeof(complex_t));
			}
		}
	}
	if(pad)
		write(outfd, nulbuf, pad*sizeof(complex_t));
	return 0;
}

static void usage(char*cmd)
{
	printf("Usage: %s [options] input.cfil\n"
			"  -o : output file (.iq)\n"
			"  -s : get channel statistics\n"
			"  -p : insert padding \n"
			"  -e : do events \n"
			"  -a : do adapt \n"
			"  -c : selected channel\n", cmd);
}

int main(int argc, char **argv)
{
	int opt;
	char *chan = NULL, *infname = NULL, *outfname = NULL;
	int chan_sel = -1;
	int infd, outfd;
	int do_stat = 0, do_events=0, do_adapt = 0;
	long unsigned pad=0;
	cfil_p cfil;
	phydef_t *phy_defs;
	phydef_chan_t *cdef;
	chan_phy_adapt_p cadapt = NULL;

	while ((opt=getopt(argc, argv, "i:o:c:p:seha"))!=-1){
		switch(opt){
		case 'o':	outfname = optarg;	break;
		case 'c':	chan = optarg;		break;
		case 'p':	pad = strtoul(optarg,NULL,10);	break;
		case 's':	do_stat = 1;		break;
		case 'e':	do_events = 1;		break;
		case 'a':	do_adapt = 1;		break;
		case '?': case 'h':
			usage(*argv);
			return 1;
		default:
			abort();
		}
	}
	if (argc-optind != 1){
		usage(*argv);
		return 1;
	}
	infname = argv[optind];

	if ((infd=open(infname, O_RDONLY))==-1)
	{
		perror("open input");
		return 1;
	}
	if (cfil_create(&cfil, infd))
		return 1;

	if (do_events){
		extract_stats(cfil,1);
		lseek(infd, 0, 0);
	}
	else if (do_stat)
	{
		extract_stats(cfil,0);
		lseek(infd, 0, 0);
	}
	if (chan){
		if ((chan_sel=cfil_find_chan(cfil, chan))==-1){
			LOG_ERROR("Invalid chan selected '%s'", chan);
			return 1;
		}
	}
	if (outfname == NULL)
		return 0;	
	if (!strcmp(outfname,"-"))
		outfd = 1;
	else {
		if(-1==(outfd=open(outfname, O_WRONLY|O_TRUNC|O_CREAT, 0766))){
			perror("open output");
			return 1;
		}
	}
	if (chan_sel == -1)
		return extract_all(cfil, outfd);
	else
	{
		if (do_adapt)
		{
			/* chan adapt */
			if (!(phy_defs = config_load_phydef(PHY_CONF)))
				return 1;
			if(!(cdef = phydef_find_chan(phy_defs, chan))){
				LOG_ERROR("chan %s not found", chan);
				return 1;
			}
			cadapt = calloc(1, sizeof(*cadapt));
			if(chan_phy_adapt_init(cadapt, cdef, cfil->hdr.freq, cfil->hdr.bw))
			{
				LOG_ERROR("Unable to create chphy_adapt");
				return 1;
			}
		}
		return extract_chan(cfil, outfd, chan_sel, cadapt, pad);
	}
}
