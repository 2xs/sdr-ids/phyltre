#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <signal.h>
#include <sys/time.h>
#include <phydet/mem.h>
#include <phydet/acq_src.h>

#define SRC_CONF "conf/acq_src.ini"

#define MAX_CHAN 512

/* signal */
static int exiting = 0;

void sig_hdl(int n)
{
	exiting = 1;
}

void usage(char*arg0)
{
	printf("Usage: %s <out.iq> [max_samp]\n", arg0);
	exit(1);
}

int main(int argc, char**argv)
{
	int output_file = -1;
	unsigned bufsize = 4096;
	acq_src_parm_t *src_conf;
	acq_src_drv_t *drv;
	u64 time_ns;
	complex_t *buf;
	static u64 total_samples = 0, max_samp = 0;
	static struct timeval start_time, end_time, runtime;
	double tusec;

	if (argc < 2)
	{
		usage(*argv);
	}
	if (!(output_file = open(argv[1], O_WRONLY|O_TRUNC|O_CREAT, 0600)))
	{
		perror("open");
		return 1;
	}
	if (argc > 2)
	{
		max_samp = strtoull(argv[2], NULL, 10);
	}

	/* Load configuration */
	if (!(src_conf = config_load_acq_src(SRC_CONF)))
		return 1;

	/* Debug stuff */
	acq_src_print(src_conf);

	/* Initialize acquisition source */
	if (!(drv = acq_src_create(src_conf)))
		return 1;
	config_del_acq_src(src_conf);

	signal(2, sig_hdl);

	/* Prepare stream */
	if (!(buf = MEM_ALLOC_ALIGN(sizeof(complex_t)*bufsize))){
		perror("malloc");
		return 1;
	}

	if(acq_src_start_stream(drv))
		return 1;

	gettimeofday(&start_time, NULL);

	/* Do the streaming */
	while(!exiting)
	{
		if (acq_src_read_stream(drv, buf, bufsize, &time_ns) != bufsize)
		{
			printf("invalid read\n");
			break;
		}
		write(output_file, buf, sizeof(complex_t)*bufsize);
		total_samples += bufsize;
		if (max_samp != 0 && max_samp <= total_samples)
			break;
	}
	gettimeofday(&end_time, NULL);
	timersub(&end_time, &start_time, &runtime);
	tusec = runtime.tv_sec * 1e6 + runtime.tv_usec;
	printf("Elapsed %.1f seconds, %llu samples, %.3fMsps\n", tusec/1e6, total_samples, (total_samples/tusec));

	acq_src_stop_stream(drv);
	acq_src_destroy(drv);
	close(output_file);

	return 0;
}
