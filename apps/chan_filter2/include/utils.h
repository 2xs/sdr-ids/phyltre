#ifndef DEF_CHAN_UTIL_H_
#define DEF_CHAN_UTIL_H_
#include "chan.h"

int chan_find_peaks(chan_reader_t *r);

int chan_handle_msk(chan_reader_t *r);

#define compute_bt est_bt_fast_

#endif
