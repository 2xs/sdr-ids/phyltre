#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <math.h>
#include <assert.h>
#include <sys/sendfile.h>
#include <phydet/tlv_file.h>
#include <phydet/config.h>
#include <phydet/chan_phy.h>
#include <phydet/pool.h>
#include "chan.h"

//#define DEBUG_EVENTS

#define PHY_CONF "conf/phy"

static int outfd;
static int match_count = 0;
static int extract_chan(chan_phy_p chphy, cfil_p cfil, int chan_sel)
{
	int i, do_output = 0, stop_output=0;
	complex_t *fbuf = malloc(cfil->hdr.fft_bin*sizeof(complex float));
	u64 time_ns, bidx=0;
	sbuf_p sb;
	unsigned b_dur_ns;
	int evt;

	fprintf(stderr,"Extracting channel %d\n", chan_sel);

	b_dur_ns = (unsigned)(1e9 * cfil->hdr.fft_bin / cfil->hdr.bw);

	printf("b_dur_ns: %d\n", b_dur_ns);
	
	while (1){
		/* Read one chunk and its events */
		if(cfil_read_chunk(cfil, fbuf, cfil->hdr.fft_bin, &time_ns))
			return -1;

		/* default event */
		evt = DET_EVENT_MATCH_CONT;
		for(i=0;i<cfil->event_count;i++){
			if (cfil->events[i].chan_index == chan_sel){
				if((evt=cfil->events[i].event)){
					do_output = 1;
				}
				else
					if(do_output)
						stop_output = 1;
				/* There is maximum one event per channel, we can break*/
				break;
			}
		}
		if(do_output){
			match_count++;
			if(!(sb = sbuf_alloc())){
				puts("no sbuf");
				return -1;
			}
			sbuf_fill(sb, fbuf, NULL, NULL, time_ns, 0);
			bidx = (u64)(time_ns/b_dur_ns);
			chan_phy_rcv(chphy, bidx, evt, sb);
			sbuf_release(sb);
			/* write samples to file */
			chan_phy_write_buf(chphy, outfd);
			
			if(stop_output){
				do_output=0;
				stop_output=0;
			}
		}
	}
	return 0;
}

static phydef_chan_t *find_chan_in_def(phydef_t *phy_defs, char *chan_str)
{
	phydef_t *def;
	phydef_chan_t *chan;
	char *proto, *chan_name;

	proto = strtok(chan_str, "/");
	chan_name = strtok(NULL, "/");

	fprintf(stderr, "proto: %s, chan: %s\n", proto, chan_name);

	for(def=phy_defs;def;def=def->next)
	{
		if(!strcmp(def->name, proto)){
			for(chan=def->channels;chan;chan=chan->next){
				if(!strcmp(chan->name, chan_name)){
					return chan;
				}
			}
			break;
		}
	}

	return NULL;
}

static void usage(char*cmd)
{
	printf("Usage: %s [options] input.cfil\n"
			"  -o : output file (.iq)\n"
			"  -c : selected channel\n", cmd);
}

int main(int argc, char **argv)
{
	int opt, rc;
	char *chan_str = NULL, *infname = NULL;
	char *outfname = "/dev/null";
	int chan_sel = -1;
	int infd;
	cfil_p cfil;
	phydef_t *phy_defs;
	phydef_chan_t *cdef;
	chan_phy_p chphy;

	if (!(phy_defs = config_load_phydef(PHY_CONF)))
		return 1;

	while ((opt=getopt(argc, argv, "i:o:c:h"))!=-1){
		switch(opt){
		case 'c':	chan_str = optarg;	break;
		case 'o':	outfname = optarg;	break;
		case 'h': 	default:
			usage(*argv);
			return 1;
		}
	}
	if (argc-optind != 1){
		usage(*argv);
		return 1;
	}
	infname = argv[optind];

	if ((infd=open(infname, O_RDONLY))==-1)
	{
		perror("open input");
		return 1;
	}
	if((outfd=open(outfname, O_WRONLY|O_CREAT|O_TRUNC, 0600))==-1)
	{
		perror("open output");
		return 1;
	}
	if(cfil_create(&cfil, infd))
		return 1;

	if (!chan_str){
		fprintf(stderr, "Chan argument required\n");
		return 1;
	}

	if ((chan_sel=cfil_find_chan(cfil, chan_str))==-1){
		fprintf(stderr, "Invalid chan selected '%s'\n", chan_str);
		return 1;
	}
	if(!(cdef=find_chan_in_def(phy_defs, chan_str))){
		return 1;
	}
	sbuf_pool_init(10, cfil->hdr.fft_bin);
	if(chan_phy_create(&chphy, cdef, cfil->hdr.freq, cfil->hdr.bw, cfil->hdr.fft_bin))
	{
		puts("Unable to create chphy");
		return 1;
	}
	if((rc = extract_chan(chphy, cfil, chan_sel)))
		printf("error: %d\n", rc);
	sbuf_pool_destroy();
	cfil_destroy(cfil);
	return rc;
}
