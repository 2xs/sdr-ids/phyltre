#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <signal.h>
#include <sys/time.h>
#include <phydet/phydef.h>
#include <phydet/acq_src.h>
#include <phydet/det.h>
#include <phydet/dsp_util.h>
#include <phydet/config.h>
#include <phydet/log.h>
#include <phydet/tlv_file.h>
#include <phydet/mem.h>
#include <phydet/pool.h>
#include <phydet/chan_phy.h>

#define PHY_CONF "conf/phy"
#define SRC_CONF "conf/acq_src.ini"
#define DET_CONF "conf/pwr_det.ini"

#define MAX_CHAN 512

/* signal */
static int exiting = 0;

/* stats */
static int total_match = 0;
static int total_width = 0;
static int count_write = 0;
static int total_put = 0;
static u64 total_samples = 0;
static struct timeval start_time, end_time, runtime;

/* threads */
static chan_phy_thr_p chan_threads[MAX_CHAN];
static unsigned chan_cnt = 0;

void sig_hdl(int n)
{
	exiting = 1;
}

void start_stats(void)
{
	gettimeofday(&start_time, NULL);
}

void stop_stats(void)
{
	double t;
	gettimeofday(&end_time, NULL);
	timersub(&end_time, &start_time, &runtime);
	t = runtime.tv_sec * 1e6 + runtime.tv_usec;
	printf("\nMatched %d/%d ~ %.2f%%\n", total_match, total_width, total_match*100.f/total_width);
	printf("count write: %d ~ %.2f%%\n", count_write, count_write*100.f/total_width);
	printf("Avg channel activity: %.2f%%\n", total_put*100.f/total_width/chan_cnt);
	printf("Elapsed %.3f seconds, %llu samples, %.3fMsps\n", t/1e6, total_samples, (total_samples/t));
}

static pwr_det_ctx_t* create_detector_with_conf(
	phydef_t *phy,
	acq_src_drv_t *src,
	pwr_det_parm_t *det_conf,
	acq_src_parm_t *acq_conf
){
	pwr_det_ctx_t *ctx;
	phydef_chan_t *cdef;
	char buf[128];
	float noise_floor;
	int tech_idx;

	printf("Source detector: fft bins=%d\n", det_conf->fft_bin_count);
	if (acq_conf->nf_type == NF_AUTO)
	{
		printf("Noise floor: ");
		acq_src_determine_noise_floor(src,  det_conf->fft_bin_count, det_conf->win_type, 4096, &noise_floor);
	}
	else
	{
		noise_floor = acq_conf->noise_floor;
	}
	printf("%.2f dBm\n", noise_floor);
	
	if(pwr_det_create(&ctx, det_conf, src->cur_freq, src->cur_bw, noise_floor))
		return NULL;

	phydef_print(phy);

	/* Try to add all specified channels, ignore out-of-band channels */
	for(; phy; phy = phy->next)
	{
		tech_idx = pwr_det_tech_add(ctx, phy->name, &phy->bandsel);
		if (tech_idx < 0)
			continue;
		for(cdef = phy->channels; cdef; cdef = cdef->next)
		{
			snprintf(buf, sizeof(buf), "%s/%s", phy->name, cdef->name);
			if(!pwr_det_tech_chan_add(ctx, tech_idx, buf, cdef->center_freq))
			{
				LOG_INFO("Added chan %2d : %s", chan_cnt, buf);
				if(chan_cnt == MAX_CHAN){
					LOG_ERROR("Too many channels");
					return NULL;
				}
				if(chan_phy_thr_create(&chan_threads[chan_cnt], cdef,
					src->cur_freq, src->cur_bw, 2048))
				{
					LOG_ERROR("Unable to create thread chan");
					return NULL;
				}
				chan_cnt++;
			}
		}
	}
	if (chan_cnt==0)
	{
		LOG_ERROR("No known channel in band");
		pwr_det_destroy(ctx);
		return NULL;
	}
	LOG_INFO("Added %d channels", chan_cnt);

	return ctx;
}

void stop_threads(void)
{
	int i;
	void *rv;

	LOG_INFO("Stopping threads...");
	for(i=0;i<chan_cnt;i++)
	{
		chan_phy_thr_stop(chan_threads[i]);
	}
	for(i=0;i<chan_cnt;i++)
	{
		chan_phy_thr_join(chan_threads[i], &rv);
		chan_phy_thr_destroy(chan_threads[i]);
	}
}

int main(int argc, char**argv)
{
	int i, rc, evt_cnt;
	unsigned bin_count;
	phydef_t *phy_defs;
	acq_src_parm_t *src_conf;
	pwr_det_parm_t *det_conf;
	acq_src_drv_t *drv;
	pwr_det_ctx_t *ctx;
	u64 time_ns, bid;
	complex_t *buf;
	sbuf_p sb;
	pwr_det_out_evt_t events[MAX_CHAN];
	chan_phy_chunk_p chunk;
	int blocking = 0;

	/* Load configuration */
	if (!(phy_defs = config_load_phydef(PHY_CONF)))
		return 1;
	if (!(det_conf = config_load_pwr_det(DET_CONF)))
		return 1;
	if (!(src_conf = config_load_acq_src(SRC_CONF)))
		return 1;
	bin_count = det_conf->fft_bin_count;

	/* Debug stuff */
	acq_src_print(src_conf);

	/* Never drop samples when using file input */
	if (!strcmp(src_conf->driver, "file"))
	{
		blocking = 1;
		LOG_INFO("Using blocking output");
	}


	/* Initialize acquisition source */
	if (!(drv = acq_src_create(src_conf)))
		return 1;

	/* Initialize the snr detector */
	if (!(ctx = create_detector_with_conf(phy_defs,drv,det_conf, src_conf)))
		return 1;

	config_del_acq_src(src_conf);
	config_del_pwr_det(det_conf);

	/* Initialize (big) shared buffers pool */
	if(sbuf_pool_init(0x20000, bin_count))
		return 1;

	if (argc == 2)
	{
		// Override output file for pwr_det
		det_conf->log_path = strdup(argv[1]);
	}

	/* Prepare stream */
	if (!(buf = MEM_ALLOC_ALIGN(sizeof(complex_t)*bin_count))){
		perror("malloc");
		return 1;
	}
	signal(2, sig_hdl);

	if(acq_src_start_stream(drv))
		return 1;

	/* Do the streaming */
	start_stats();
	for(bid=0;;bid++)
	{
		if (acq_src_read_stream(drv, buf, bin_count, &time_ns) != bin_count)
		{
			LOG_ERROR("invalid read");
			break;
		}
		total_samples += bin_count;
		rc = pwr_det_execute(ctx, buf, bin_count, time_ns);
		if (rc)
		{
			total_match ++;
		}
		total_width ++;

		/* Read detector's output in a sb */
		if ((evt_cnt = pwr_det_out_block(ctx, &sb, events, MAX_CHAN))<0)
		{
			LOG_ERROR("No mem");
			break;
		}
		else if (evt_cnt > 0)
		{
			count_write++;
			for(i=0;i<evt_cnt;i++)
			{
				//printf("%8llu | event on chan %d\n", bid, events[i].chan_index);
				if(chan_phy_chunk_create(&chunk, bid, events[i].event, sb) == 0)
				{
					if (!blocking)
					{
						if(chan_phy_thr_send(chan_threads[events[i].chan_index], chunk) != 0)
						{
							/* Error queue full */
							fputc('o',stderr);
							chan_phy_chunk_destroy(chunk);
						}
					}
					else
					{
						chan_phy_thr_send_blocking(chan_threads[events[i].chan_index], chunk);
					}
					total_put++;
				}
			}
			sbuf_release(sb);
		}
		if(exiting)
		{
			LOG_INFO("exiting");
			break;
		}
	}
	stop_threads();

	stop_stats();

	/* Cleanup */
	if(acq_src_stop_stream(drv))
		return 1;
	MEM_FREE(buf);
	pwr_det_destroy(ctx);
	sbuf_pool_dump_state();
	sbuf_pool_destroy();
	phydef_destroy(phy_defs);
	acq_src_destroy(drv);

	return 0;
}
