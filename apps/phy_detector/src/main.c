#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <signal.h>
#include <sys/time.h>
#include <phydet/phydef.h>
#include <phydet/acq_src.h>
#include <phydet/det.h>
#include <phydet/dsp_util.h>
#include <phydet/config.h>
#include <phydet/log.h>
#include <phydet/tlv_file.h>
#include <phydet/mem.h>

#define PHY_CONF "conf/phy"
#define SRC_CONF "conf/acq_src.ini"
#define DET_CONF "conf/pwr_det.ini"

static int exiting = 0;
u32 bin_count;

static pwr_det_ctx_t* create_detector_with_conf(
	phydef_t *phy,
	acq_src_drv_t *src,
	pwr_det_parm_t *det_conf
){
	pwr_det_ctx_t *ctx;
	phydef_chan_t *chan;
	char buf[128];
	int cnt;
	float noise_floor;
	int tech_idx;

	printf("Source detector: fft bins=%d\n", det_conf->fft_bin_count);
	printf("Noise floor: ");
	if(acq_src_determine_noise_floor(src, det_conf->fft_bin_count, det_conf->win_type, 4096, &noise_floor))
		return NULL;
	printf("%.2f dBm\n", noise_floor);

	if(pwr_det_create(&ctx, det_conf, src->cur_freq, src->cur_bw, noise_floor))
		return NULL;

	bin_count = det_conf->fft_bin_count;

	/* Try to add all possible channels, ignore errors for out-of-bounds channels*/
	for(cnt=0; phy; phy = phy->next)
	{
		tech_idx = pwr_det_tech_add(ctx, phy->name, &phy->bandsel);
		if (tech_idx < 0)
			continue;
		for(chan = phy->channels; chan; chan = chan->next)
		{
			snprintf(buf, sizeof(buf), "%s/%s", phy->name, chan->name);
			if(!pwr_det_tech_chan_add(ctx, tech_idx, buf, chan->center_freq))
				cnt++;
		}
	}
	if (cnt==0)
	{
		printf("No known channel in band\n");
		return NULL;
	}
	printf("Added %d channels\n", cnt);

	return ctx;
}

static int output_file;
static int total_match = 0;
static int total_width = 0;
static int count_write = 0;
static u64 total_samples = 0;

int test_rx_cb(acq_src_drv_t *drv, complex_t *samples, u32 count, u64 timestamp, void *data)
{
	pwr_det_ctx_t *ctx = (pwr_det_ctx_t*) data;
	u32 idx;
	int rc;
	double ns_per_buf, bw, nsoff;

	acq_src_get_bw(drv, &bw);
	ns_per_buf = bin_count*1e9/bw;
	nsoff=0.f;

	if (exiting)
	{
		return 1;
	}


	//printf("add %d samples\n", count);
	total_samples += count;

	for(idx=0; idx+bin_count<=count; idx+=bin_count, nsoff+=ns_per_buf)
	{
		rc = pwr_det_execute(ctx, &samples[idx], bin_count,
			timestamp+(unsigned)round(nsoff));
		if (rc)
		{
#ifdef LOG_DET
			printf ("\n<- (%8d) || ", total_match);
#endif
			total_match ++;
		}
		total_width ++;
		/* FIXME: count selected events */
	}
	if (idx != count)
	{
		LOG_ERROR("Unexpected unaligned buffer");
		return -827;
	}
	return 0;
}

void sig_hdl(int n)
{
	exiting = 1;
}

int main(int argc, char**argv)
{
	int rc,ret = 0;
	phydef_t *phy_defs;
	acq_src_parm_t *src_conf;
	pwr_det_parm_t *det_conf;
	acq_src_drv_t *drv;
	pwr_det_ctx_t *ctx;
	struct timeval start_time, end_time, runtime;
	double t;

	if (argc != 2)
	{
		fprintf(stderr, "Usage: %s output.new\n", *argv);
		return 1;
	}

	signal(2, sig_hdl);
	
	/* Load configuration */
	if (!(phy_defs = config_load_phydef(PHY_CONF)))
		return 1;
	if (!(det_conf = config_load_pwr_det(DET_CONF)))
		return 1;
	if (!(src_conf = config_load_acq_src(SRC_CONF)))
		return 1;

	/* Debug stuff */
	acq_src_print(src_conf);

	/* Initialize acquisition source */
	if (!(drv = acq_src_create(src_conf)))
		return 1;
	config_del_acq_src(src_conf);

	/* Override pwr_det output */
	det_conf->log_path = strdup(argv[1]);

	/* Initialize the snr detector */
	if(!(ctx = create_detector_with_conf(phy_defs,drv,det_conf)))
		exit(1);
	MEM_FREE(det_conf);
	
	if (ctx){
		/* Create riff output file */
		if (-1==(output_file = open(argv[1], O_CREAT|O_WRONLY|O_TRUNC, 0774)))
		{
			perror("opening output new");
			return 1;
		}
		gettimeofday(&start_time, NULL);
		if (acq_src_stream_rx(drv, test_rx_cb, &rc, ctx) < 0)
		{
			ret = 1;
		}
		gettimeofday(&end_time, NULL);
	}

	timersub(&end_time, &start_time, &runtime);
	t = runtime.tv_sec * 1e6 + runtime.tv_usec;
	printf("runtime: %f sec\n", t/1e6);


	printf("\nMatched %d/%d ~ %.2f%%\n", total_match, total_width, total_match*100.f/total_width);
	printf("count write: %d ~ %.2f%%\n", count_write, count_write*100.f/total_width);
	printf("Elapsed %.1f seconds, %llu samples, %.3fMsps\n", t/1e6, total_samples, (total_samples/t));

	pwr_det_destroy(ctx);
	phydef_destroy(phy_defs);
	acq_src_destroy(drv);
	close(output_file);

	return ret;
}
