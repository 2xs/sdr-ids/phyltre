set(tool zigbee_demod)
file(GLOB SOURCES "*.c")
add_executable(${tool} ${SOURCES})
target_link_libraries(${tool} phydet_static
	m
	${VOLK_LIBRARIES}
	${LIQUIDDSP_LIBRARIES}
	)
