#include <string.h>
#include <assert.h>
#include <btbb.h>
#include <phydet/mem.h>
#include <phydet/log.h>
#include <phydet/chan_decode.h>
#include <phydet/tlv_file.h>


typedef struct PACKED {
	double time_ns;
	unsigned of;
	u32 lap;
} btbr_meta_t;

int btbr_decode_create(struct chan_decode_s *dp, const gen_conf_t *conf)
{
	/* Initialize btbb */
	btbb_init(4);
	return SUCCESS;
}

void btbr_decode_destroy(struct chan_decode_s *d)
{
	/* Nothing to destroy */
}

int btbr_decode_execute(struct chan_decode_s *c, double time_ns, const u8 *syms, size_t sym_count,
	tlv_field_t **metas, size_t *metas_count)
{
	int offset;
	double time_ms, bt_time;
	size_t max_metas;
	btbb_packet *pkt = NULL;
	u32 lap;
	btbr_meta_t feta;
	// double bitrate = c->cdef->phy_def->adapt.bitrate;

	max_metas = *metas_count;
	*metas_count = 0;

	if (sym_count < 64)
		return SHORT_READ;

	if ((offset = btbb_find_ac((char*)syms, sym_count-64, LAP_ANY, 4, &pkt)) >= 0)
	{
		lap = btbb_packet_get_lap(pkt);
		time_ms = time_ns/1e6;
		bt_time = (time_ns/625000.f);
		LOG_INFO("(%2.3fms -> %.1f) | BT Lap (at %d): %x", time_ms, bt_time, offset, btbb_packet_get_lap(pkt));
		feta.time_ns = time_ns + offset*1e9 / c->cdef->phy_def->adapt.bitrate;
		feta.lap = lap;
		feta.of = offset;
		// TODO: time of event = time_ns + offset*1e9 / bitrate;
		assert(*metas_count+1 <= max_metas);
		metas[(*metas_count)++] = tlv_field_alloc_init('ALTB', &feta, sizeof(feta));
		btbb_packet_unref(pkt);

		return SUCCESS;
	}
	return DECODE_FAILED;
}

const chan_decode_intf_t chan_decode_btbr_intf = {
	.create	 = btbr_decode_create,
	.destroy = btbr_decode_destroy,
	.execute = btbr_decode_execute,
};
