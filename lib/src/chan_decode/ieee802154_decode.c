#include <string.h>
#include <assert.h>
#include <btbb.h>
#include <phydet/mem.h>
#include <phydet/log.h>
#include <phydet/chan_decode.h>
#include <phydet/tlv_file.h>
#include <phydet/ieee802154bb.h>

typedef struct {
	int log_payload;
	double bitrate;
} *ieee802154_decode_p;

#define MAX_PAYLOAD 255
typedef struct PACKED {
	double time_ns;	// time of sfd
	int crc_good;
} *ieee802154_meta_p;

int ieee802154_decode_create(struct chan_decode_s *cd, const gen_conf_t *dconf)
{
	int i;
	ieee802154_decode_p d;

	if (!(d = MEM_ALLOC(sizeof(*d))))
		return NO_MEM;

	d->log_payload = 0;
	d->bitrate = cd->cdef->phy_def->adapt.bitrate;

	for (i=0;i<dconf->num_args;i++)
	{
		if(!strcmp(dconf->args[i].name, "logPayload"))
			d->log_payload = !strcasecmp(dconf->args[i].value, "true");
		else
			LOG_ERROR("Unknown parameter %s", dconf->args[i].name);
	}
	cd->data = d;
	return SUCCESS;
}

void ieee802154_decode_destroy(struct chan_decode_s *cd)
{
	MEM_FREE(cd->data);
}

int ieee802154_decode_execute(struct chan_decode_s *c, double time_ns, const u8 *syms, size_t sym_count,
	tlv_field_t **metas, size_t *metas_count)
{
	int rc;
	int offset=0, last_offset, bytes_count;
	u8 payload[255];
	u16 crc = 0, crc_cal;
	u8 length;
	size_t max_metas;
	ieee802154_decode_p p = c->data;
	tlv_field_t *field;
	ieee802154_meta_p meta;

	max_metas = *metas_count;
	*metas_count = 0;

	/* We need at least 4 bytes (sfd, len, crc) (*2chips*32bits)
	 * for a valid frame */
	if((last_offset = sym_count - 4*2*32)<0)
		return SHORT_READ;

#if 0
	/* debug: find preamble */
	offset = ieee802154_find_chip(0, syms, sym_count);
	if (offset != -1)
		LOG_INFO("Found Preamble chip at %d", offset);
#endif

	/* Find sfd */
	offset = ieee802154_find_sfd(syms, sym_count);
	if (offset == -1)
	{
		/* sfd, skip */
		LOG_DEBUG("ieee802154_decode: sfd not foud");
		return  DECODE_FAILED;
	}
	LOG_DEBUG("Found SFD at %d", offset);
	if(!(field = tlv_field_alloc(sizeof(*meta))))
		return NO_MEM;
	field->hdr.tag = '4518';
	meta = (ieee802154_meta_p)field->data;
	meta->time_ns = time_ns + (1e9*offset)/p->bitrate;
	meta->crc_good = 0;
	assert(*metas_count+1 <= max_metas);
	metas[(*metas_count)++] = field;

	/* Skip SFD */
	offset += 64;

	/* Decode full frame */
	bytes_count = (sym_count - offset)/64 - 1; // (-1 bytes for len )
	if (bytes_count < 0)
	{
		LOG_DEBUG("Short read 2 :(");
		/* no enough bytes for len(1)+crc(2) */
		return SHORT_READ;
	}
	if((rc=ieee802154_decode_bytes(&syms[offset], &length, 1)))
	{
		LOG_DEBUG("Length decode failed");
		return rc;
	}

	offset += 64;
	if (length > bytes_count)
	{
		LOG_DEBUG(" Short read 3 (length=0x%x, bcount=0x%x)", length, bytes_count);
		return SHORT_READ;
	}
	
	if((rc=ieee802154_decode_bytes(&syms[offset], payload, length)))
	{
		LOG_DEBUG("Payload decode failed");
		return rc;
	}
	if (p->log_payload)
	{
		assert (*metas_count+1 <= max_metas);
		metas[(*metas_count)++] = tlv_field_alloc_init('  LP', payload, length);
	}
	if (length >= 2)
	{
		crc = (payload[length-1]<<8) | payload[length-2];
		crc_cal = ieee802154_calccrc(payload, length-2);
		meta->crc_good = crc_cal==crc;
		LOG_INFO("%f, Decoded ieee802154 frame (raw=%d , len=%d), CRC: %d", time_ns/1e6, bytes_count, length, meta->crc_good);
	}
#if 1
	int i;
	for (i=0; i<length; i++)
	{
		fprintf(stderr, " %02x", payload[i]);
	}
	fprintf(stderr, "\n");
#endif

	return SUCCESS;
}

const chan_decode_intf_t chan_decode_ieee802154_intf = {
	.create	 = ieee802154_decode_create,
	.destroy = ieee802154_decode_destroy,
	.execute = ieee802154_decode_execute,
};
