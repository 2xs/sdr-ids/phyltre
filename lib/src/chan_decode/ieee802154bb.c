#include <phydet/crc.h>
#include <phydet/ieee802154bb.h>

/*
 * Based on ucla_ieee802_15_4_packet_sink.cc by Thomas Schmid, Leslie Choong
 */

// this is the mapping between chips and symbols if we do
// a fm demodulation of the O-QPSK signal. Note that this
// is different than the O-QPSK chip sequence from the
// 802.15.4 standard since there there is a translation
// happening.
// See "CMOS RFIC Architectures for IEEE 802.15.4 Networks",
// John Notor, Anthony Caviglia, Gary Levy, for more details.
#define IEEE802154_CHIP_0	0b01100000011101111010111001101100
#define IEEE802154_CHIP_1	0b01001110000001110111101011100110
#define IEEE802154_CHIP_2	0b01101100111000000111011110101110
#define IEEE802154_CHIP_3	0b01100110110011100000011101111010
#define IEEE802154_CHIP_4	0b00101110011011001110000001110111
#define IEEE802154_CHIP_5	0b01111010111001101100111000000111
#define IEEE802154_CHIP_6	0b01110111101011100110110011100000
#define IEEE802154_CHIP_7	0b00000111011110101110011011001110
#define IEEE802154_CHIP_8	0b00011111100010000101000110010011
#define IEEE802154_CHIP_9	0b00110001111110001000010100011001
#define IEEE802154_CHIP_A	0b00010011000111111000100001010001
#define IEEE802154_CHIP_B	0b00011001001100011111100010000101
#define IEEE802154_CHIP_C	0b01010001100100110001111110001000
#define IEEE802154_CHIP_D	0b00000101000110010011000111111000
#define IEEE802154_CHIP_E	0b00001000010100011001001100011111
#define IEEE802154_CHIP_F	0b01111000100001010001100100110001
static const unsigned IEEE802154_CHIP_MAPPING[] = {
	IEEE802154_CHIP_0,
	IEEE802154_CHIP_1,
	IEEE802154_CHIP_2,
	IEEE802154_CHIP_3,
	IEEE802154_CHIP_4,
	IEEE802154_CHIP_5,
	IEEE802154_CHIP_6,
	IEEE802154_CHIP_7,
	IEEE802154_CHIP_8,
	IEEE802154_CHIP_9,
	IEEE802154_CHIP_A,
	IEEE802154_CHIP_B,
	IEEE802154_CHIP_C,
	IEEE802154_CHIP_D,
	IEEE802154_CHIP_E,
	IEEE802154_CHIP_F,
};

#define IEEE802154_CHIP_THRESHOLD 10  // detect access code with up to DEFAULT_THRESHOLD bits wrong

// FIXME: we can store the last chip
// ignore the first and last chip since it depends on the last chip.
#define CHIP_WEIGHT(n, chip)		hamming32((n&0x7FFFFFFE)^(IEEE802154_CHIP_MAPPING[chip]&0x7FFFFFFE))
#define IEEE802154_MATCH_CHIP(n,chip)	(CHIP_WEIGHT(n,chip) <= IEEE802154_CHIP_THRESHOLD)

static inline unsigned char ieee802154_decode_chips(unsigned int chips)
{
	unsigned char i;
	unsigned char chip = 0xff;
	int w, min_w = 32;

	for(i=0; i<16; i++) {
		w = CHIP_WEIGHT(chips, i);
		if (w < IEEE802154_CHIP_THRESHOLD && w < min_w)
		{
			chip = i;
			min_w = w;
		}
	}
	return chip;
}

u32 ieee802154_crctbl[256];

void __attribute__((constructor)) ieee802154_init(void)
{
	crc_tbl_init(ieee802154_crctbl, 0x8408);
}

int ieee802154_decode_bytes(const u8 *bits, u8 *bytes, u8 bytes_count)
{
	size_t i;
	u8 b;
	u8 chip;

	for (i=0;i<bytes_count;i++,bits+=64)
	{
		
		if((chip=ieee802154_decode_chips(pack32bits(bits)))==0xff)
		{
			return -1;
		}
		b = chip;
		if((chip=ieee802154_decode_chips(pack32bits(&bits[32])))==0xff)
			return -1;
		b |= chip<<4;
		*bytes++ = b;
	}
	return 0;
}

int ieee802154_find_chip(u8 chip, const u8 *syms, size_t syms_count)
{
	u32 reg;
	int offset=0, last_offset;

	if((last_offset = syms_count - 32)<0)
		return -1;

	reg = pack32bits(syms);
	offset += 32;

	while (offset <= last_offset)
	{
		if (IEEE802154_MATCH_CHIP(reg, chip))
		{
			return offset;
		}
		else
		{
			reg = (reg<<1)|syms[offset++];
		}
	}
	return -1;
}

int ieee802154_find_sfd(const u8 *syms, size_t sym_count)
{
	static const u64 search = ((((u64)IEEE802154_CHIP_7)<<32) | (u64)IEEE802154_CHIP_A)^0x8000000080000000;
	int i, err;
	u64 reg = 0;

	if (sym_count < 64)
		return -1;
	reg = ((u64)pack32bits(syms))<<32;
	reg |= pack32bits(&syms[32]);

	for (i=64;i<sym_count;i++)
	{
		reg = (reg<<1)|syms[i];
		if ((err=hamming64(reg ^ search)) < 12)
		{
			return i+1 - 64;
		}
	}
	return -1;
}
