#include <string.h>
#include <assert.h>
#include <btbb.h>
#include <phydet/mem.h>
#include <phydet/dsp_util.h>
#include <phydet/log.h>
#include <phydet/chan_decode.h>
#include <phydet/tlv_file.h>

const u32 GSM_TSC[8] = {
	0b00100101110000100010010111,
	0b00101101110111100010110111,
	0b01000011101110100100001110,
	0b01000111101101000100011110,
	0b00011010111001000001101011,
	0b01001110101100000100111010,
	0b10100111110110001010011111,
	0b11101111000100101110111100,
};

#define TSC_TRESHOLD 3

static int test_tsc(const u8 *syms, size_t sym_count, u32 seq, int *err)
{
	u32 reg, i;
	u32 search = (1<<27)|(seq<<1)|1;

	for(reg=i=0; i<sym_count; i++)
	{
		if (i >= 28)
		{
			*err = hamming32((reg&((1<<28)-1))^search);
			if (*err <= TSC_TRESHOLD)
			{
				fprintf(stderr, "Found tsc %8x at %d (%d err)\n",
					seq, i-28, *err);
				//return i-26;
			}
		}
		reg = (reg<<1)|syms[i];
	}
	return ERROR;
}

#define SEARCH_OFFSET 0
#define SEARCH_LENGTH 100

static int find_tsc(const u8 *syms, size_t sym_count, int *offset, int *err)
{
	int tsc;

	if (sym_count < SEARCH_OFFSET + SEARCH_LENGTH + 28)
		return -1;
	for (tsc=0; tsc<8; tsc++)
	{
		*offset = test_tsc(&syms[SEARCH_OFFSET], SEARCH_LENGTH+28, GSM_TSC[tsc], err);
		if (*offset >= 0)
			return tsc;
	}

	return ERROR;
}

int gsm_decode_create(struct chan_decode_s *dp, const gen_conf_t *conf)
{
	dp->data = NULL;
	return SUCCESS;
}

void gsm_decode_destroy(struct chan_decode_s *d)
{
}

int gsm_decode_execute(struct chan_decode_s *c, double time_ns, const u8 *syms, size_t sym_count,
	tlv_field_t **metas, size_t *metas_count)
{
	int tsc_offset, tsc_err;
	int tsc;

	*metas_count = 0;
	LOG_INFO("gsm_decode (%lu syms)", sym_count);

	if (sym_count < 140)
	{
		LOG_DEBUG("Short read");
		return SHORT_READ;
	}
	if ((tsc = find_tsc(syms, sym_count, &tsc_offset, &tsc_err)) >= 0)
	{
		LOG_INFO("Found TSC %d at %u (%u errors)",
			tsc, tsc_offset, tsc_err);
	}
	return SUCCESS;
}

const chan_decode_intf_t chan_decode_gsm_intf = {
	.create	 = gsm_decode_create,
	.destroy = gsm_decode_destroy,
	.execute = gsm_decode_execute,
};
