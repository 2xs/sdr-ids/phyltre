#include <string.h>
#include <phydet/mem.h>
#include <phydet/log.h>
#include <phydet/chan_decode.h>

extern chan_decode_intf_t chan_decode_btbr_intf;
extern chan_decode_intf_t chan_decode_btle_intf;
extern chan_decode_intf_t chan_decode_null_intf;
extern chan_decode_intf_t chan_decode_ieee802154_intf;
extern chan_decode_intf_t chan_decode_gsm_intf;

static struct chan_decode_lut_s {
	char *name;
	chan_decode_intf_t *intf;
} decode_lut[] = {
	{"btbr", &chan_decode_btbr_intf},
	{"btle", &chan_decode_btle_intf},
	{"ieee802154", &chan_decode_ieee802154_intf},
	{"gsm", &chan_decode_gsm_intf},
	{"null", &chan_decode_null_intf},
	{NULL},
};

int chan_decode_create(chan_decode_p *cp, const phydef_chan_t *cdef)
{
	int rc;
	struct chan_decode_lut_s *l;
	chan_decode_p c;
	gen_conf_t *dconf = &cdef->phy_def->decode;

	if(!(c = MEM_ALLOC(sizeof(*c))))
		return NO_MEM;
	c->intf = NULL;
	for(l=decode_lut; l->name; l++)
	{
		if (!strcmp(l->name, dconf->type))
		{
			c->intf = l->intf;
			break;
		}
	}
	if (c->intf == NULL)
	{
		LOG_ERROR("Unknown decode %s", dconf->type);
		MEM_FREE(c);
		return CONF_ERROR;
	}
	c->cdef = cdef;
	if((rc = c->intf->create(c, dconf)))
	{
		MEM_FREE(c);
		return rc;
	}
	*cp = c;
	return 0;
}
