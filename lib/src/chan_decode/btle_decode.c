#include <string.h>
#include <assert.h>
#include <btbb.h>
#include <phydet/mem.h>
#include <phydet/log.h>
#include <phydet/chan_decode.h>
#include <phydet/tlv_file.h>
#include <phydet/btlebb.h>

typedef struct {
	unsigned chan_num;
} btle_decode_t, *btle_decode_p;

typedef struct PACKED {
	double time_ns;	// time of address
	int crc;
	u8 data[0];
} *btleadv_meta_p;

int btle_decode_create(struct chan_decode_s *d, const gen_conf_t *conf)
{
	btle_decode_p b;

	if(!(b = MEM_ALLOC(sizeof(*b))))
		return NO_MEM;

	b->chan_num = strtoul(d->cdef->name, NULL, 10);
	d->data = b;

	btlebb_init();

	return 0;
}

void btle_decode_destroy(struct chan_decode_s *d)
{
	MEM_FREE(d->data);
}


int btle_decode_execute(struct chan_decode_s *c, double time_ns, const u8 *syms, size_t sym_count,
	tlv_field_t **metas, size_t *metas_count)
{
	phdr_t phdr;
	btle_decode_p p = c->data;
	unsigned err;
	int offset;
	unsigned datalen, i;
	size_t max_metas;
	u8 pdu[2+255+3];
	u32 crccal, crcval;
	tlv_field_t *field;
	btleadv_meta_p meta;

	max_metas = *metas_count;
	*metas_count = 0;

	if (sym_count < BTLEBB_DETECT_WINDOW)
		return SHORT_READ;

	if ((offset = btlebb_detect_adv_preamble(p->chan_num, syms, sym_count, &phdr, &err)) < 0)
	{
	/* TODO: dewhiten, check crc */
		return DECODE_FAILED;
	}
	LOG_INFO("BLE Advertisement detected (chan %d) at %d (phdr.len=%d)", p->chan_num,offset, phdr.len);
	/* Advertisement found, enough data for pdu header + pdu len + crc ? */
	datalen = 2 + phdr.len + 3;
	if (offset + BTLEBB_PDUHDR_OFFSET + 8*datalen > sym_count)
	{
		LOG_ERROR("short read (need %u > %lu)", offset+BTLEBB_PDUHDR_OFFSET+8*datalen, sym_count);
		return DECODE_FAILED;
	}
	btlebb_unpack(pdu, syms + offset + BTLEBB_PDUHDR_OFFSET, datalen);
	btlebb_whiten(p->chan_num, pdu, datalen);
	crccal = btlebb_calccrc(0xAAAAAA, pdu, 2+phdr.len);
	crcval = 0xffffff & *(uint32_t*)(pdu+2+phdr.len);

	LOG_INFO("decoded btle packet, crc: %d (val %x, cal %x)", crcval==crccal, crcval, crccal);
	fprintf(stderr, "BTLE frame: ");
	for (i=0;i<datalen;i++)
		fprintf(stderr, "%02x ", pdu[i]);
	fprintf(stderr, "\n");

	assert(*metas_count+1 <= max_metas);
	if(!(field = tlv_field_alloc(sizeof(*meta)+datalen)))
		return NO_MEM;
	field->hdr.tag = 'VAEL';
	meta = (btleadv_meta_p)field->data;
	meta->time_ns = time_ns + offset*1e9/c->cdef->phy_def->adapt.bitrate;
	meta->crc = crcval == crccal;
	memcpy(meta->data, pdu, datalen);
	// LE AdVertising
	metas[(*metas_count)++] = field;

	return SUCCESS;
}

const chan_decode_intf_t chan_decode_btle_intf = {
	.create	 = btle_decode_create,
	.destroy = btle_decode_destroy,
	.execute = btle_decode_execute,
};
