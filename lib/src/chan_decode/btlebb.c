#include <stdio.h>
#include <phydet/dsp_util.h>
#include <phydet/btlebb.h>

static u8 whitentbl[256*2];
u32 btle_crctbl[256];

static void whiten_init(void)
{
	unsigned i, bv;
	u8 cl, cx;

	for (i=0; i<256; i++)
	{
		cx = 0;
		cl = i;
		for(bv=1;bv<0x100;bv<<=1)
		{
			if (cl & 1)
			{
				cx ^= bv;
				cl ^= 0x88;
			}
			cl >>= 1;
		}
		whitentbl[i*2] = cx;
		whitentbl[i*2+1] = cl;
	}
}

static int btlebb_init_done = 0;
void __attribute__((constructor)) btlebb_init(void)
{
	if (!btlebb_init_done)
	{
		btlebb_init_done = 1;
		crc_tbl_init(btle_crctbl, 0xda6000);
		whiten_init();
	}
}

u8 btlebb_whiten(u8 seed, u8 *buf, unsigned len)
{
	unsigned i;
	u8 lfsr;

	lfsr = seed | 0x40;

	for(i=0;i<len;i++)
	{
		buf[i] ^= whitentbl[lfsr*2];
		lfsr = whitentbl[lfsr*2+1];
	}
	return lfsr;
}

void btlebb_unpack(u8 *dst_bytes, const u8 *src_syms, u8 byte_count)
{
	unsigned i,j, b;

	for(i=0;i<byte_count;i++)
	{
		for(b=0, j=0; j<8; j++)
		{
			b |= *src_syms++<<j;
		}
		dst_bytes[i] = b;
	}
}

int btlebb_detect_adv_preamble(u32 channel_num, const u8 *syms, unsigned syms_count, phdr_t *phdr, unsigned *err)
{
	unsigned i;
	uint64_t shift_reg, det40;
	uint16_t dw2;
	unsigned errcnt;

	if (syms_count < BTLEBB_DETECT_WINDOW)
		return -1;
	
	/* 40 bits of preamble to look for */
	det40 = ((uint64_t)ADV_ACCESS_ADDR<<8)|0xAA;

	/* Precompute whiten values for the pdu header*/
	dw2 = 0;
	btlebb_whiten(channel_num, (u8*)&dw2, 2);

	for(shift_reg = 0, i=0; i<syms_count; i++, shift_reg>>=1)
	{
		shift_reg |= (uint64_t)syms[i]<<(BTLEBB_DETECT_WINDOW-1);

		/* Check hamming distance of preamble  */	
		if(i >= (BTLEBB_DETECT_WINDOW-1) && (errcnt=hamming64((shift_reg&((1LL<<40)-1))^det40)) <= BTLEBB_DETECT_MAX_ERR)
		{
			/* Get dewhitened pdu hdr */
			phdr->u16 = (0xffff&(shift_reg>>40)) ^ dw2;

			/* More sanity checks */
			if (phdr->rfu != 0 || phdr->type > 8)
			{
				continue;
			}
			*err = errcnt;

			/* Return position of detected preamble */
			return i + 1 - BTLEBB_DETECT_WINDOW;
		}
	}
	return -1;
}
