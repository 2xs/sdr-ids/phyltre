#include <string.h>
#include <assert.h>
#include <btbb.h>
#include <phydet/mem.h>
#include <phydet/log.h>
#include <phydet/chan_decode.h>
#include <phydet/tlv_file.h>

int null_decode_create(struct chan_decode_s *dp, const gen_conf_t *conf)
{
	return SUCCESS;
}

void null_decode_destroy(struct chan_decode_s *d)
{
}

int null_decode_execute(struct chan_decode_s *c, double time_ns, const u8 *syms, size_t sym_count,
	tlv_field_t **metas, size_t *metas_count)
{
	*metas_count = 0;
	return SUCCESS;
}

const chan_decode_intf_t chan_decode_null_intf = {
	.create	 = null_decode_create,
	.destroy = null_decode_destroy,
	.execute = null_decode_execute,
};
