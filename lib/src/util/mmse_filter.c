#include <math.h>
#include <string.h>
#include <phydet/mem.h>
#include <phydet/mmse_filter.h>

/* simple mmse interpolator with 8 taps adapted from gnuradio */
int mmse_filter_f_create(mmse_filter_f_p *mp)
{
	mmse_filter_f_t *m;

	m = MEM_ALLOC(sizeof(*m));
	m->nsteps = MMSE_NSTEPS;
	m->ntaps = MMSE_NTAPS;
	m->in_buf = MEM_ALLOC_ALIGN(sizeof(float)*m->ntaps);
	m->out_buf = MEM_ALLOC_ALIGN(sizeof(float));

	*mp = m;

	return 0;
}

int mmse_filter_f_destroy(mmse_filter_f_p m)
{
	MEM_FREE(m->in_buf);
	MEM_FREE(m->out_buf);
	MEM_FREE(m);

	return 0;
}

float mmse_filter_f_interp(mmse_filter_f_p m, const float *input, float mu)
{
	int imu = (int) m->nsteps-rint(mu*m->nsteps);

	memcpy(m->in_buf, input, sizeof(float)*m->ntaps);
	volk_32f_x2_dot_prod_32f(m->out_buf, m->in_buf, mmse_taps[imu], m->ntaps);

	return *m->out_buf;
}
