#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <assert.h>
#include <asm/unistd.h>
#include <sys/mman.h>
#include <phydet/circbuf.h>
#include <phydet/mem.h>

/* https://lo.calho.st/quick-hacks/employing-black-magic-in-the-linux-page-table/ */

int __attribute__((weak)) memfd_create(const char *name, unsigned int flags) {
    return syscall(__NR_memfd_create, name, flags);
}

int cbuf_create(cbuf_p *cbp, size_t size)
{
	size_t pagesize = getpagesize();
	size_t size_align = (size + pagesize-1) &~ (pagesize-1);
	cbuf_p cb;

	if (!(cb = MEM_ALLOC(sizeof(*cb))))
		return -1;
	cb->size = size_align;
	cb->head = cb->tail = 0;

	if((cb->fd = memfd_create("cbuf", 0))==-1)
	{
		perror("memfd_create");
		return -1;
	}
	ftruncate(cb->fd, size_align);

	cb->buffer = mmap(NULL, size_align*2, PROT_READ|PROT_WRITE, MAP_SHARED|MAP_ANONYMOUS, -1, 0);
	if(cb->buffer==MAP_FAILED)
		return -1;
	if(MAP_FAILED == mmap(cb->buffer, size_align, PROT_READ|PROT_WRITE, MAP_SHARED|MAP_FIXED, cb->fd, 0)
	|| MAP_FAILED == mmap(cb->buffer+size_align, size_align, PROT_READ|PROT_WRITE, MAP_SHARED|MAP_FIXED, cb->fd, 0))
		return -1;

	*cbp = cb;
	return 0;
}
void cbuf_destroy(cbuf_p cb)
{
	munmap(cb->buffer, cb->size * 2);
	close(cb->fd);
	MEM_FREE(cb);
}

int cbuf_write(cbuf_p cb, void *data, size_t sz)
{
	if (cbuf_free(cb) < sz)
		return -1;
	memcpy(cbuf_tail(cb), data, sz);
	cb->tail += sz;
	return 0;
}

int cbuf_read(cbuf_p cb, void *data, size_t sz)
{
	if (cbuf_avail(cb) < sz)
		return -1;
	memcpy(data, cbuf_head(cb), sz);
	cb->head += sz;
	if(cb->head >= cb->size)
	{
		cb->head -= cb->size;
		cb->tail -= cb->size;
	}
	return 0;
}
