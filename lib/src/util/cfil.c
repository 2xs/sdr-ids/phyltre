#include <unistd.h>
#include <string.h>
#include <assert.h>
#include <phydet/log.h>
#include <phydet/mem.h>
#include <phydet/tlv_file.h>
#include <phydet/cfil.h>

#define W2C(c) (c&0xff),(((c)>>8)&0xff),(((c)>>16)&0xff),(((c)>>24)&0xff)

static int cfil_skip_bytes(cfil_p c, unsigned len)
{
	if(-1==lseek(c->fd, len, SEEK_CUR)){
		perror("lseek");
		return -1;
	}
	return 0;
}

int cfil_read_chunk(cfil_p c, complex_t *samples, unsigned count, u64 *time_ns)
{
	int rc=0;
	u32 type = 0, len;
	u64 stim;

	c->event_count = 0;

	/* Read up to next event */
	while (1){
		if (tlv_read_TL(c->fd, &type, &len))
			return -1;
		if (type == 'MITS')
			break;
		else if(cfil_skip_bytes(c, len))
			return -1;
	}
	if (len != sizeof(stim) || sizeof(stim) !=(rc=read(c->fd, &stim, sizeof(stim))))
	{
		LOG_ERROR("Read error (STIM %d %d)",len,rc);
		return -1;
	}
	/* Output time nanoseconds */
	if(time_ns)
		*time_ns = stim;

	if (tlv_read_TL(c->fd, &type, &len))
	{
		LOG_ERROR("Unexpected EOF");
		return -1;
	}
	if (type == 'TNVE')
	{
		/* Read events */
		if (len > c->evt_size || len%sizeof(cfil_evt_t) != 0 || read(c->fd, c->events, len)!=len){
			LOG_ERROR("Invalid EVNT");
			return -1;
		}
		c->event_count = len / sizeof(cfil_evt_t);

		if (tlv_read_TL(c->fd, &type, &len))
		{
			LOG_ERROR("Unexpected EOF");
			return -1;
		}
	}
	if (type != 'ATAD')
	{
		LOG_ERROR("Unexpected tag %c%c%c%c", W2C(type));
		return -1;
	}
	/* Read data */
	assert(len == sizeof(complex_t)*count);
	if(len != read(c->fd, samples, len))
	{
		LOG_ERROR("Invalid read");
		return -1;
	}
	return 0;
}

static int cfil_read_header(cfil_p c)
{
	int rc;
	u32 type=0,len=0;
	char buf[512];

	c->chan_max = 32;
	c->chan_count = 0;
	if (!(c->chans = (char**)MEM_ALLOC(sizeof(char*)*c->chan_max)))
	{
		LOG_ERROR("no mem");
		return -1;
	}

	/* Read header */
	if ((rc=tlv_read_TL(c->fd, &type, &len)) || type != 'LIFC' || len != sizeof(c->hdr)){
		LOG_ERROR("Invalid file (%d) (%c%c%c%c, %d!=%ld)",rc,W2C(type),len,sizeof(c->hdr));
		return 1;
	}
	if ((rc=read(c->fd, &c->hdr, len))!=len){
		perror("read header");
		return -1;
	}
	LOG_INFO("Capture File: freq=%eHz, bw=%eHz, nf=%.1fdBm, %d fft bins\nChannels:",
			c->hdr.freq, c->hdr.bw, c->hdr.nf, c->hdr.fft_bin);

	/* Read channel definitions */
	while(1)
	{
		if (tlv_read_TL(c->fd, &type, &len) || type != 'FEDC'
			|| len > sizeof(buf) || read(c->fd, buf, len)!=len){
			LOG_ERROR("Invalid CDEF");
			return -1;
		}
		if (len == 0)
			return 0;
		if (c->chan_count == c->chan_max){
			c->chan_max*=2;
			c->chans = (char**)MEM_REALLOC(c->chans,sizeof(char*)*c->chan_max);
		}
		c->chans[c->chan_count] = strndup(buf, len);
		LOG_INFO("\t%-3d :  %s",c->chan_count, c->chans[c->chan_count]);
		c->chan_count++;
	}

	return 0;
}

int cfil_find_chan(cfil_p c, const char *chan)
{
	int i;

	for (i=0; i<c->chan_count;i++)
		if (!strcmp(chan, c->chans[i]))
			return i;

	return -1;
}

int cfil_create(cfil_p *cp, int fd)
{
	cfil_p c;

	if (!(c=MEM_ALLOC(sizeof(*c))))
		return -1;
	memset(c, 0, sizeof(*c));

	c->fd = fd;
	if(cfil_read_header(c))
		goto err;
	
	c->evt_size = sizeof(cfil_evt_t)*c->chan_count;
	if(!(c->events = MEM_ALLOC(c->evt_size)))
		goto err;

	*cp = c;

	return 0;
err:
	cfil_destroy(c);

	return -1;
}

void cfil_destroy(cfil_p c)
{
	int i;
	close(c->fd);
	if (c->chans){
		for(i=0;i<c->chan_count;i++)
			MEM_FREE(c->chans[i]);
		MEM_FREE(c->chans);
	}
	if(c->events)
		MEM_FREE(c->events);
	MEM_FREE(c);
}
