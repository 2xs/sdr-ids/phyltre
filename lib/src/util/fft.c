#include <stdlib.h>
#include <string.h>
#include <volk/volk.h>
#include <math.h>
#include <phydet/log.h>
#include <phydet/fft.h>
#include <phydet/mem.h>

/* Scale window to get normalized fft mag output */
static void phydet_fft_win_scale(float *win, unsigned n)
{
	unsigned i;
	float cpg = 0;

	for (i=0; i<n; i++)
		cpg += win[i]*win[i];
	cpg = sqrt(cpg*n);
	for(i=0;i<n;i++)
		win[i] /= cpg;
}

static void phydet_fft_win_hamming_a(float *win, unsigned n, float a)
{
	unsigned i;
	float o = n-1;
	float b = 1-a;
	
	for (i=0;i<n;i++)
		win[i] = a - b*cos(M_PI*2*i/o);
}

static void phydet_fft_win_hamming(float *win, unsigned n)
{
	return phydet_fft_win_hamming_a(win, n, 0.54);
}

static void phydet_fft_win_hann(float *win, unsigned n)
{
	return phydet_fft_win_hamming_a(win, n, 0.5);
}

static void phydet_fft_win_rect(float*win, unsigned n)
{
	unsigned i;
	for (i=0;i<n;i++)
		win[i] = 1.f;
}

int phydet_fft_execute(phydet_fft_p ctx, complex_t *samples, float *magbuf)
{
	unsigned len = ctx->bin_count;

	/* Apply window */
	volk_32fc_32f_multiply_32fc(ctx->plan_in, (__complex__ float *)samples, ctx->window, len);

	/* Execute FFT */
	fftwf_execute(ctx->plan);

	if (magbuf != NULL)
	{
		/* compute mag^2 */
		volk_32fc_magnitude_squared_32f(ctx->magbuf, (const __complex__ float*)ctx->plan_out, len);

		/* perform fft shifting */
		memcpy(magbuf, &ctx->magbuf[len/2], sizeof(float)*len/2);
		memcpy(&magbuf[len/2], ctx->magbuf, sizeof(float)*len/2);

		if (ctx->ignore_dc)
		{
			/* Ignore DC bin (interpolate) */
			magbuf[len/2] = (magbuf[len/2-1]+magbuf[len/2+1])/2;
		}
	}

	return 0;
}

int phydet_fft_create(phydet_fft_p *ctxp, unsigned num_bins, win_type_t win_type, u32 ignore_dc)
{
	phydet_fft_p ctx = MEM_ALLOC(sizeof(*ctx));

	ctx->bin_count = num_bins;
	ctx->plan_in = fftwf_malloc(sizeof(fftwf_complex)*num_bins);
	ctx->plan_out = fftwf_malloc(sizeof(fftwf_complex)*num_bins);
	ctx->plan = fftwf_plan_dft_1d(num_bins, ctx->plan_in, ctx->plan_out, FFTW_FORWARD, FFTW_ESTIMATE);
	ctx->window = (float*)MEM_ALLOC_ALIGN(num_bins * sizeof(float));
	ctx->magbuf = (float*)MEM_ALLOC_ALIGN(sizeof(float)*num_bins);
	ctx->ignore_dc = ignore_dc;
	switch(win_type){
	case WIN_RECT:
		phydet_fft_win_rect(ctx->window, num_bins);
		break;
	case WIN_HAMMING:
		phydet_fft_win_hamming(ctx->window, num_bins);
		break;
	case WIN_HANN:
		phydet_fft_win_hann(ctx->window, num_bins);
		break;
	default:
		LOG_ERROR("win type %d nyi", win_type);
		return 1;
	}
	phydet_fft_win_scale(ctx->window, num_bins);

	*ctxp = ctx;

	return 0;
}

void phydet_fft_destroy(phydet_fft_p ctx)
{
	fftwf_destroy_plan(ctx->plan);
	fftwf_free(ctx->plan_in);
	fftwf_free(ctx->plan_out);
	MEM_FREE(ctx->window);
	MEM_FREE(ctx->magbuf);
	MEM_FREE(ctx);
}
