#include <assert.h>
#include <math.h>
#include <phydet/log.h>
#include <phydet/mem.h>
#include <phydet/nf_est.h>
#include <phydet/fft.h>

#define NOISE_RANGE 15.f // dB

int nfe_create(nfe_p *nfep, unsigned fft_size, unsigned win_size)
{
	nfe_p nfe = MEM_ALLOC(sizeof(*nfe));
	nfe->fft_size = fft_size;
	nfe->win_size = win_size;
	nfe->widx = 0;
	//phydet_fft_create(&nfe->fft, fft_size, WIN_RECT, 0);
	nfe->win = MEM_ALLOC(sizeof(float)*win_size);
	nfe->magbuf = MEM_ALLOC(sizeof(float)*fft_size);
	nfe->medbuf = MEM_ALLOC(sizeof(float)*fft_size);

	*nfep = nfe;

	return 0;
}

void nfe_destroy(nfe_p nfe)
{
	//phydet_fft_destroy(nfe->fft);
	MEM_FREE(nfe->win);
	MEM_FREE(nfe->magbuf);
	MEM_FREE(nfe->medbuf);
	MEM_FREE(nfe);
}

static void nfe_exec_once(nfe_p nfe, float *magbuf)
{
	size_t i;
	float floor;

	/* FIXME: log10f */
	for (i=0;i<nfe->fft_size;i++){
		nfe->magbuf[i] = 10*log10f(magbuf[i]);
	}
	volk_32f_s32f_calc_spectral_noise_floor_32f(&floor, 
		nfe->magbuf, NOISE_RANGE, nfe->fft_size);
	nfe->win[nfe->widx] = floor;
	nfe->widx = (nfe->widx+1) % nfe->win_size;
}

int cmp_float(const void *ap, const void *bp)
{
	float a =*(float*)ap, b=*(float*)bp;

	return a < b ? -1 : a > b ? 1 : 0;
}

float nfe_median(nfe_p nfe)
{
	float val;

	qsort(nfe->win, nfe->win_size, sizeof(float), cmp_float);
	if (nfe->win_size & 1)
	{
		val = nfe->win[nfe->win_size/2];
	}
	else
	{
		val = 0.5f * (nfe->win[nfe->win_size/2] + nfe->win[nfe->win_size/2-1]);
	}
	return val;
}

float nfe_average(nfe_p nfe)
{
	float val;
	size_t i;

	for (val=0.f, i=0;i<nfe->win_size;i++)
	{
		val += nfe->win[i];
	}
	return val / nfe->win_size;
}

int nfe_execute(nfe_p nfe, float *psd, size_t count)
{
	assert(count == nfe->fft_size);
	nfe_exec_once(nfe, psd);

	return 0;
}
