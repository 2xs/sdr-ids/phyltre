#include <phydet/dsp_util.h>
#include <assert.h>

/* calc mag2 average on iq data */
float dsp_util_calc_time_power(const complex_t *in, u32 count)
{
	#define MAX_TIME 16384
	float magbuf[MAX_TIME];
	float p; 

	volk_32fc_magnitude_squared_32f(magbuf, (const __complex__ float*)in, count & (MAX_TIME-1));
	volk_32f_accumulator_s32f(&p, magbuf, count);

	return p / count;
}

/* calc mag2 average on scaled fft mag^2*/
float dsp_util_calc_freq_power(const float *in, u32 count)
{
	float p;

	volk_32f_accumulator_s32f(&p, in, count);

	return p;
}

int find_bin_indexes(double fft_freq, double fft_bw, unsigned fft_size, double freq_lo, double freq_hi, urange_t *range)
{
	double reso = fft_bw / fft_size;
	double bw = freq_hi - freq_lo;
	double flo = fft_freq - fft_bw/2;
	int first, last;

	if (bw <= (reso / 2))
	{
		LOG_ERROR("FFT resolution (%eHz) too low for %eHz\n", reso, bw);
		return -1;
	}

	first = (int)round((freq_lo - flo) / reso);
	last =  (int)round((freq_hi - flo) / reso) - 1;

	if (first < 0 || first >= fft_size || last < 0 || last >= fft_size)
		return -1;

	range->first = (unsigned)first;
	range->last = (unsigned)last;

	return 0;
}
