#include <stdio.h>
#include <string.h>
#include <phydet/phydef.h>
#include <phydet/mem.h>

void phydef_print(phydef_t *defs)
{
	phydef_t *def;
	phydef_chan_t *chan;

	for(def = defs; def; def = def->next)
	{
		printf("Phy def '%s': bw=%e \n",
			def->name, def->adapt.filter_bw);
		printf("Channels:\n");
		for(chan = def->channels; chan; chan = chan->next)
		{
			printf("\t%s: center_freq=%e\n",
				chan->name, chan->center_freq);
		}
	}
}

void phydef_destroy(phydef_t *defs)
{
	phydef_t *def_next, *def;
	phydef_chan_t *ch_next, *chan;

	for(def = defs; def; def = def_next)
	{
		def_next = def->next;
		for(chan = def->channels; chan; chan = ch_next)
		{
			ch_next = chan->next;
			MEM_FREE(chan->name);
			MEM_FREE(chan);
		}
		MEM_FREE(def->name);
		MEM_FREE(def);
	}
}

phydef_chan_t *phydef_find_chan(phydef_t *phy_defs, char *chan_str)
{
	phydef_t *def;
	phydef_chan_t *chan;
	char *proto, *chan_name;

	proto = strtok(chan_str, "/");
	chan_name = strtok(NULL, "/");

	fprintf(stderr, "proto: %s, chan: %s\n", proto, chan_name);

	for(def=phy_defs;def;def=def->next)
	{
		if(!strcmp(def->name, proto)){
			for(chan=def->channels;chan;chan=chan->next){
				if(!strcmp(chan->name, chan_name)){
					return chan;
				}
			}
			break;
		}
	}
	return NULL;
}
