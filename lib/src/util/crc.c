#include <phydet/types.h>

void crc_tbl_init(u32 *tbl, u32 poly)
{
	u32 c, b, x, i;

	for (x=0; x<256; x++)
	{
		c = x;
		for(i=0;i<8;i++)
		{
			b = c & 1;
			c >>= 1;
			if(b)
				c ^= poly;
		}
		tbl[x] = c;
	}
}

u32 crc_exec(const u32 *tbl, u32 init, const u8 *data, u32 size)
{
	unsigned i;
	u32 crc = init;

	for (i=0;i<size;i++)
		crc = (crc>>8)^tbl[(data[i]^crc)&0xff];

	return crc;
}
