#include <math.h>
#include <string.h>
#include <stdio.h>
#include <phydet/mem.h>
#include <phydet/peak_det.h>


int peak_det_create(peak_det_p *dp, unsigned n, unsigned max_length, float min_snr)
{
	peak_det_p d;
	
	if (!(d = MEM_ALLOC(sizeof(*d))))
		return -1;

	d->n = n;
	d->min_snr = min_snr;
	d->max_length = max_length;
	d->min_snr_p = powf(10, min_snr/10.f);
	if(!(d->buf = MEM_ALLOC(sizeof(float)*n*2)))
		return -1;
	peak_det_reset(d);

	*dp = d;
	return 0;
}

void peak_det_reset(peak_det_p d)
{
	memset(d->buf, 0, sizeof(float)*d->n*2);
	d->idx = 0;
	d->sum_a = d->sum_b = 0.f;
	d->state = PEAK_DET_WAIT_PEAK;
}

void peak_det_destroy(peak_det_p d)
{
	MEM_FREE(d->buf);
	MEM_FREE(d);
}

int peak_det_update(peak_det_p d, float mag2, peak_t *peak, float *cur_snr)
{
/* Helpers to replace log10 when manipulating dB internally */
#define AEST(a,b)	((a)>(b)?(a)/(b):-(b)/(a))
#define AABS(a)		((a)<0?-1.f/(a):(a))
	unsigned idx_b = (d->idx+d->n)%(d->n*2);
	float snrp, snrpa;

	/* Update rolling averages */
	d->sum_a -= d->buf[idx_b];
	d->sum_b += d->buf[idx_b];
	d->sum_b -= d->buf[d->idx];
	d->sum_a += d->buf[d->idx] = mag2;
	d->idx = (d->idx+1) % (d->n*2);

	/* Compute SNR and its absolute value */
	snrp = AEST(d->sum_a, d->sum_b);
	snrpa = fabs(snrp);

	if (cur_snr)
		/* The whole goal of the function design
		 * is to avoid calling log10 on each sample.
		 * so be careful when using this */
		*cur_snr = 10.f*log10f(AABS(snrp));

	switch(d->state)
	{
	case PEAK_DET_WAIT_PEAK:
		if (snrpa >= d->min_snr_p)
		{
			/* Start of peak */
			d->cur_peak.length = 1;
			d->cur_peak.max_idx = 0;
			d->cur_peak.max_val = snrp;
			d->cur_peak.abs_val = d->sum_a;
			d->state = PEAK_DET_IN_PEAK;
		}
		break;
	case PEAK_DET_IN_PEAK:
		if (snrpa < d->min_snr_p || d->cur_peak.length == d->max_length)
		{
			/* End of peak */
			peak->length = d->cur_peak.length;
			peak->max_idx = d->cur_peak.max_idx;
			peak->max_val = 10*log10f(AABS(d->cur_peak.max_val));
			peak->abs_val = 10*log10f(d->cur_peak.abs_val / d->n);
			d->state = PEAK_DET_WAIT_PEAK;

			return 1;
		}
		else {
			if (snrpa > fabs(d->cur_peak.max_val))
			{
				/* Update peak max value */
				d->cur_peak.max_idx = d->cur_peak.length;
				d->cur_peak.max_val = snrp;
				d->cur_peak.abs_val = d->sum_a;
			}
			d->cur_peak.length++;
		}
		break;
	default: break;
	}
	return 0;
}

/* Featuring too many calls to log10 */
int peak_det_update_old(peak_det_p d, float mag2, peak_t *peak, float *cur_snr)
{
	unsigned idx_b = (d->idx+d->n)%(d->n*2);
	float snr, snr_abs;

	/* Update both rolling averages */
	d->sum_a -= d->buf[idx_b];
	d->sum_b += d->buf[idx_b];
	d->sum_b -= d->buf[d->idx];
	d->sum_a += d->buf[d->idx] = mag2;
	d->idx = (d->idx+1) % (d->n*2);

	/* Compute SNR and its absolute value */
	snr = 10.f*log10f(d->sum_a / d->sum_b);
	snr_abs = fabs(snr);

	if (cur_snr)
		*cur_snr = snr;

	switch(d->state)
	{
	case PEAK_DET_WAIT_PEAK:
		if (snr_abs >= d->min_snr)
		{
			/* Start of peak */
			d->cur_peak.length = 1;
			d->cur_peak.max_idx = 0;
			d->cur_peak.max_val = snr;
			d->state = PEAK_DET_IN_PEAK;
		}
		break;
	case PEAK_DET_IN_PEAK:
		if (snr_abs < d->min_snr)
		{
			/* End of peak */
			*peak = d->cur_peak;
			d->state = PEAK_DET_WAIT_PEAK;

			return 1;
		}
		else {
			if (snr_abs > fabs(d->cur_peak.max_val))
			{
				/* Update peak max value */
				d->cur_peak.max_val = snr;
				d->cur_peak.max_idx = d->cur_peak.length;
			}
			d->cur_peak.length++;
		}
		break;
	default: break;
	}
	return 0;
}
