#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <phydet/mem.h>
#include <phydet/log.h>
#include <phydet/clock_recov.h>

//#define USE_MM
#define USE_ZC

/* Clock recov adapted from gnuradio */

static inline float slice(float f)
{
	return f <= 0.f ? -1.f : 1.f;
}

static inline u8 bslice(float f)
{
	return f <= 0.f ? 0 : 1;
}

static inline float brc(float x, float clip)
{
	float x1, x2;

	x1 = fabsf(x+clip);
	x2 = fabsf(x-clip);

	x1 -= x2;

	return 0.5f * x1;
}

int clock_recov_create(clock_recov_p *cp, float omega, float gain_omega, float gain_mu, float omega_relative_limit)
{
	clock_recov_t *c;

	if (!(c=MEM_ALLOC(sizeof(*c))))
		return -1;

	memset(c,0,sizeof(*c));
	if(mmse_filter_f_create(&c->interp))
		return -1;
	c->omega = omega;
	c->gain_omega = gain_omega;
	c->gain_mu = gain_mu;
	c->omega_lim = omega*omega_relative_limit;
	c->req_hist = 1 + c->interp->ntaps;
#ifdef USE_ZC
	c->req_hist++;
#endif

	*cp = c;

	return 0;
}

void clock_recov_destroy(clock_recov_p c)
{
	mmse_filter_f_destroy(c->interp);
	MEM_FREE(c);
}

int clock_recov_exec_(clock_recov_p c, float mu, u8 *output, const float *input, u32 input_len, float *out_dbg)
{
	int oo,ii,max_in;
	float err, cur, prev = 0.f;
#ifdef USE_ZC
	float pprev = 0.f;
#endif
	float omega, mu_next;

	max_in = input_len - c->interp->ntaps;

	ii=oo=0;
	prev=0.f;
	omega = c->omega;

	while(ii<max_in){
		cur = mmse_filter_f_interp(c->interp, &input[ii], mu); 

		output[oo++] = bslice(cur);
		if (out_dbg)
			*out_dbg++ = cur;
#ifdef USE_MM
		/* Mueller-Muller error */
		err = slice(prev) * cur - slice(cur) * prev;
#elif defined (USE_ZC)
		/* Zero-crossing error */
		err = (slice(pprev) - slice(cur)) * prev;
#else
#error "Clock recovery not defined"
#endif
#ifdef USE_ZC
		pprev = prev;
#endif
		prev = cur;
		
		/* spec phase increment */
		omega += err * c->gain_omega;
		omega = c->omega + brc(omega-c->omega, c->omega_lim);
		/* phase offset */
		mu += omega + err * c->gain_mu;
		mu_next = floorf(mu);
		ii += (int)mu_next;
		mu -= mu_next;
	}
	return oo;
}
