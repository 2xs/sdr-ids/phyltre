#include <stdlib.h>
#include <string.h>
#include <phydet/rolavg.h>
#include <phydet/mem.h>

void rolavg_create(rolavg_p *rp, unsigned n)
{
	rolavg_p r = MEM_ALLOC(sizeof(*r));
	r->n = n;
	r->hist = (float*)MEM_ALLOC(sizeof(float)*n);
	memset(r->hist, 0, sizeof(float)*n);
	r->idx = 0;
	r->sum = 0;

	*rp = r;
}

void rolavg_destroy(rolavg_p r)
{
	MEM_FREE(r->hist);
	MEM_FREE(r);
}

float rolavg_update(rolavg_p r, float v)
{
	r->sum -= r->hist[r->idx];
	r->hist[r->idx] = v;
	r->sum += v;
	/* FIXE: remove the division */
	r->value = r->sum / r->n;
	r->idx++;
	if (r->idx == r->n)
		r->idx = 0;
	return r->value;
}
