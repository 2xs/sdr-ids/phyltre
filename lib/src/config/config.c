#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <sys/types.h>
#include <dirent.h>
#include <errno.h>
#include <phydet/config.h>
#include <phydet/log.h>
#include <phydet/mem.h>
#include "ini.h"


static int scmp(const char *v, unsigned vlen, const char *k, unsigned klen)
{
	return (vlen!=klen) || strncmp(v, k, klen);
}

#define KCMP(v,vlen,k) scmp(v, vlen, k,strlen(k))

#define ISPOW2(n) (((n)!=0) && ((n)&((n)-1))==0)

static int parse_section(struct INI *ini, char *sec_name)
{		      
	const char *section;
	size_t slen;

	if(ini_next_section(ini, &section, &slen) <= 0 || KCMP(section, slen, sec_name))
	{
		LOG_ERROR("Missing section %s", sec_name);
		return -1;
	}
	return 0;
}

static int config_load_phydefinition(struct INI *ini, phydef_t *def)
{
	const char *key, *val;
	size_t klen, vlen;

	if (parse_section(ini, "PhyDefinition"))
		return -1;

	while(ini_read_pair(ini, &key, &klen, &val, &vlen))
	{
		if (!KCMP(key, klen, "name"))
			def->name = strndup(val, vlen);
		else
			LOG_ERROR("Unexpected key '%.*s'", (unsigned)klen, key);
	}
	if (def->name == NULL)
	{
		LOG_ERROR("Incomplete phy definition");
		return -1;
	}
	return 0;
}

static int config_parse_band_bounds(char *s, phydef_bin_spec_t *spec)
{
	unsigned n = 0;
	char *s1, *sp1=NULL, *sp2=NULL, *tok, *slo, *shi;

	for (s1 = s; (tok = strtok_r(s1, ";", &sp1)); s1 = NULL)
	{
		if(n >= PHYDEF_MAX_BINS)
			return -1;
		slo = strtok_r(tok, ",", &sp2);
		shi = strtok_r(NULL, ",", &sp2);
		if(!slo || !shi)
			return -1;
		spec->bins[n].max_freq = strtod(shi, NULL);
		spec->bins[n].min_freq = strtod(slo, NULL);
		n++;
	}
	if (spec->bin_count && spec->bin_count != n)
		return -1;
	spec->bin_count = n;
	return 0;
}

static int config_parse_band_range(char *s, phydef_bin_spec_t *spec, int max_or_min)
{
	unsigned n = 0;
	char *s1, *tok, *sp1=NULL;

	for (s1 = s; (tok = strtok_r(s1, ";", &sp1)); s1 = NULL)
	{
		if(n >= PHYDEF_MAX_BINS)
			return -1;
		if (max_or_min)	spec->bins[n].max_snr = strtod(tok, NULL);
		else		spec->bins[n].min_snr = strtod(tok, NULL);
		n++;
	}
	if (spec->bin_count && spec->bin_count != n)
		return -1;
	spec->bin_count = n;

	return 0;
}

static int config_load_bandselector(struct INI *ini, band_sel_conf_t *bsel)
{
	const char *key, *val;
	size_t klen, vlen;
	char *tmp;

	if(parse_section(ini, "BandSelector"))
		return -1;

	while(ini_read_pair(ini, &key, &klen, &val, &vlen))
	{
		tmp = strndup(val, vlen);
		if (!KCMP(key, klen, "bins"))
			config_parse_band_bounds(tmp, &bsel->bin_spec);
		else if (!KCMP(key, klen, "maxSNR"))
			config_parse_band_range(tmp, &bsel->bin_spec,1);
		else if (!KCMP(key, klen, "minSNR"))
			config_parse_band_range(tmp, &bsel->bin_spec,0);
		else if (!KCMP(key, klen, "minDuration"))
			bsel->min_duration = strtod(val, NULL);
		else
			LOG_ERROR("Unexpected key '%.*s'", (unsigned)klen, key);
		free(tmp);
	}
	return 0;
}

static int config_load_adapt_conf(struct INI *ini, adapt_conf_t *aconf)
{
	const char *key, *val;
	size_t klen, vlen;

	if (parse_section(ini, "Adaptation"))
		return -1;
	while(ini_read_pair(ini, &key, &klen, &val, &vlen))
	{
		if (!KCMP(key, klen, "filterBW"))
			aconf->filter_bw = strtod(val, NULL);
		else if (!KCMP(key, klen, "filterTW"))
			aconf->transition_width = strtod(val, NULL);
		else if (!KCMP(key, klen, "bitrate"))
			aconf->bitrate = strtod(val, NULL);
		else if (!KCMP(key, klen, "osr"))
			aconf->osr = strtol(val, NULL, 10);
		else
			LOG_ERROR("Unexpected key '%.*s'", (unsigned)klen, key);
	}
	return 0;
}

static int config_load_peak_det(struct INI *ini, peak_conf_t *pdet)
{
	const char *key, *val;
	size_t klen, vlen;

	if(parse_section(ini, "PeakDetection"))
		return -1;
	while(ini_read_pair(ini, &key, &klen, &val, &vlen))
	{
		if (!KCMP(key, klen, "estDuration"))
			pdet->est_duration = strtod(val, NULL);
		else if (!KCMP(key, klen, "minPeak"))
			pdet->min_peak = strtod(val, NULL);
		else
			LOG_ERROR("Unexpected key '%.*s'", (unsigned)klen, key);
	}
	if(pdet->min_peak < 0 || pdet->est_duration <= 0)
	{
		LOG_ERROR("Invalid PeakDetection");
		return -1;
	}
	return 0;
}

static int config_load_gen_conf(struct INI *ini, char *sec, gen_conf_t *conf)
{
	const char *key, *val;
	size_t klen, vlen;

	if (parse_section(ini, sec))
		return -1;

	if(!ini_read_pair(ini, &key, &klen, &val, &vlen) || KCMP(key, klen, "type"))
	{
		LOG_ERROR("config_load_gen_conf: 'type' excepted");
		return -1;
	}
	conf->type = strndup(val, vlen);
	conf->num_args = 0;
	while(ini_read_pair(ini, &key, &klen, &val, &vlen))
	{
		assert(conf->num_args < MAX_GEN_ARGS);
		conf->args[conf->num_args].name = strndup(key, klen);
		conf->args[conf->num_args].value = strndup(val, vlen);
		conf->num_args++;
	}
	if (conf->type == NULL)
	{
		LOG_ERROR("Incomplete gen conf");
		return -1;
	}
	return 0;
}

static int config_load_channel_list(struct INI *ini, phydef_t *def)
{
	const char *key, *val;
	size_t klen, vlen;
	phydef_chan_p chan, *next_chan;

	next_chan = &def->channels;

	if(parse_section(ini, "PhyChannels"))
		return -1;
	while(ini_read_pair(ini, &key, &klen, &val, &vlen))
	{
		chan = calloc(sizeof(*chan), 1);
		chan->name = strndup(key, klen);
		chan->phy_def = def;
		chan->center_freq = strtod(val, NULL);
		*next_chan = chan;
		next_chan = &chan->next;
	}
	return 0;
}

static int config_load_logging(struct INI *ini, logging_conf_t *log)
{
	const char *key, *val;
	size_t klen, vlen;

	log->path = NULL;

	if(parse_section(ini, "Logging"))
		return -1;
	while(ini_read_pair(ini, &key, &klen, &val, &vlen))
	{
		if (!KCMP(key, klen, "path"))
			log->path = strndup(val, vlen);
		else
			LOG_ERROR("Unexpected key '%.*s'", (unsigned)klen, key);
	}
	if (!log->path)
		return -1;
	return 0;
}

static phydef_t *config_load_one_phydet(const char *path_or_buf, int fromstring)
{
	struct INI *ini;
	phydef_t *def = NULL;

	if (fromstring)
		ini = ini_open_mem(path_or_buf, strlen(path_or_buf));
	else
		ini = ini_open(path_or_buf);
	if(ini==NULL)
	{
		LOG_ERROR("Invalid ini given");
		return NULL;
	}

	/* Allocate phy definition */
	if (!(def = (phydef_t*) MEM_ALLOC(sizeof(*def))))
		goto err;

	memset(def, 0, sizeof(*def));

	/* Parse layers configuration */
	if(config_load_phydefinition(ini, def))
		goto err;
	if(config_load_bandselector(ini, &def->bandsel))
		goto err;
	if(config_load_adapt_conf(ini, &def->adapt))
		goto err;
	if(config_load_peak_det(ini, &def->peakdet))
		goto err;
	if(config_load_gen_conf(ini, "Modulation", &def->demod))
		goto err;
	if(config_load_gen_conf(ini, "Decoder", &def->decode))
		goto err;
	if(config_load_logging(ini, &def->logging))
		goto err;
	if(config_load_channel_list(ini, def))
		goto err;

	/* Parse channel list */
	ini_close(ini);

	return def;
err:
	free(def);
	ini_close(ini);
	return NULL;
}

/* \fn config_load_phy_det
 * \brief Parse all physical channel definitions from given directory
 * \param path 	Directory containing ini definitions
 * \return a list of phy definitions */
phydef_t *config_load_phydef(const char *path)
{
	phydef_t *tmp, *defs = NULL;
	DIR *dir;
	struct dirent *dirent;
	char pbuf[1024];
	int plen;

	dir = opendir(path);

	while(1)
	{
		if (!(dirent = readdir(dir)))
			break;
		plen = strlen(dirent->d_name);
		if (plen<4 || strcmp(dirent->d_name+plen-4,".ini"))
			continue;
		snprintf(pbuf, sizeof(pbuf), "%s/%s", path, dirent->d_name);
		tmp = config_load_one_phydet(pbuf, 0);
		if (tmp != NULL)
		{
			tmp->next = defs;
			defs = tmp;
		}
	}
	closedir(dir);
	return defs;
}

int parse_nf(const char *nf_in, nf_type_t *nf_type, double *nf_val)
{
	char *endp;
	if (!strncmp(nf_in, "auto", 4))
	{
		*nf_type = NF_AUTO;
		*nf_val = 0;
		return 0;
	}
	else{
		*nf_type = NF_FIXED;
		*nf_val = strtod(nf_in, &endp);
		if (errno || nf_in==endp)
			return 1;
		return 0;
	}
}

acq_src_parm_t *config_load_acq_src(const char *path)
{
	struct INI *ini;
	const char *section, *key, *val;
	size_t slen, klen, vlen;
	int rc;
	acq_src_parm_t *src;

	if (!(ini = ini_open(path)))
		return NULL;

	if (ini_next_section(ini, &section, &slen)<=0 || KCMP(section, slen, "ACQSource"))
	{
		LOG_ERROR("Missing section ACQSource");
		return NULL;
	}

	src = (acq_src_parm_t*) calloc(sizeof(*src), 1);

	while((rc = ini_read_pair(ini, &key, &klen, &val, &vlen)))
	{
		if (!KCMP(key, klen, "name"))
		{
			src->name = strndup(val, vlen);
		}
		else if (!KCMP(key, klen, "driver"))
		{
			src->driver = strndup(val, vlen);
		}
		else if (!KCMP(key, klen, "driverArgs"))
		{
			src->driver_args = strndup(val, vlen);
		}
		else if (!KCMP(key, klen, "startFrequency"))
		{
			src->start_freq = strtod(val, NULL);
		}
		else if (!KCMP(key, klen, "startBandwidth"))
		{
			src->start_bw = strtod(val, NULL);
		}
		else if (!KCMP(key, klen, "startGain"))
		{
			src->start_gain = strtod(val, NULL);
		}
		else if (!KCMP(key, klen, "noiseFloor"))
		{
			if (parse_nf(val, &src->nf_type, &src->noise_floor))
			{
				LOG_ERROR("Invalid noiseFloor");
				return NULL;
			}
		}
		else if (!KCMP(key, klen, "DC"))
		{
			src->block_dc = !strncasecmp(val, "true", vlen);
		}
	
		else
		{
			LOG_ERROR("Unexpected key '%.*s'", (unsigned)klen, key);
			return NULL;
		}
	}
	ini_close(ini);
	return src;
}

void config_del_acq_src(acq_src_parm_t *conf)
{
	MEM_FREE(conf->name);
	MEM_FREE(conf->driver);
	MEM_FREE(conf->driver_args);
	MEM_FREE(conf);
}

static struct str2wintype_s {char *k; win_type_t t;} str2wintype_[] = {
	{"rectangular", WIN_RECT},
	{"hamming", WIN_HAMMING},
	{"hann", WIN_HANN},
	{NULL, 0},
};

static win_type_t str2wintype(const char *s, unsigned slen)
{
	struct str2wintype_s *e;
	for (e = str2wintype_; e->k; e++)
		if (!KCMP(s, slen, e->k))
			return e->t;
	return WIN_INVALID;
}

pwr_det_parm_t *config_load_pwr_det(const char *path)
{
	struct INI *ini;
	const char *section, *key, *val;
	size_t slen, klen, vlen;
	int rc;
	pwr_det_parm_t *parm;

	if (!(ini = ini_open(path)))
		return NULL;

	parm = (pwr_det_parm_t*) calloc(1, sizeof(*parm));

	if (ini_next_section(ini, &section, &slen)<=0)
		return parm;

	if (KCMP(section, slen, "FreqDomainDetector"))
	{
		LOG_ERROR("Invalid section %.*s", (unsigned)slen, section);
		return NULL;
	}

	while((rc = ini_read_pair(ini, &key, &klen, &val, &vlen)))
	{
		if (!KCMP(key, klen, "fftBins"))
		{
			parm->fft_bin_count = strtoul(val, NULL, 10);
		}
		else if (!KCMP(key, klen, "windowType"))
		{
			parm->win_type = str2wintype(val, vlen);
		}
		else if (!KCMP(key, klen, "ignoreDC"))
		{
			parm->ignore_dc = !strncasecmp(val, "true", vlen);
		}
		else if (!KCMP(key, klen, "logFile"))
		{
			parm->log_path = strndup(val, vlen);
		}
		else
		{
			LOG_ERROR("Unexpected key '%.*s'", (unsigned)klen, key);
			return NULL;
		}
	}
	if (!ISPOW2(parm->fft_bin_count) || parm->win_type == WIN_INVALID)
	{
		LOG_ERROR("Incomplete snr det definition");
		return NULL;
	}
	ini_close(ini);

	return parm;
}

void config_del_pwr_det(pwr_det_parm_t *parm)
{
	MEM_FREE(parm->log_path);
	MEM_FREE(parm);
}
