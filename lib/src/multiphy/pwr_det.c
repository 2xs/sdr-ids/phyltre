#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <math.h>
#include <unistd.h>
#include <assert.h>
#include <phydet/types.h>
#include <phydet/phydef.h>
#include <phydet/det.h>
#include <phydet/dsp_util.h>
#include <phydet/log.h>
#include <phydet/tlv_file.h>
#include <phydet/mem.h>
#include <phydet/fft.h>
#include <phydet/pool.h>
#include <phydet/config.h>

typedef enum pwr_det_state_e { 
	DET_STATE_IDLE,
	DET_STATE_MATCHED,
} pwr_det_state_t;

struct pwr_det_tech_s;

#define PWR_DET_HAS_MIN_PWR	(1<<0)
#define PWR_DET_HAS_MAX_SNR	(1<<1)
#define PWR_DET_IS_REF_PWR	(1<<2)

/* dynamic parameters for bin spec */
typedef struct pwr_det_bin_t {
	urange_t range;	// fft bin bounds
	unsigned size;	// range size
	u32 flags;
	double min_bin_pwr;
	double max_bin_snr;

	double pwr_sum;	// current bin power
} pwr_det_bin_t;

typedef struct pwr_det_bin_dyn_s {
	u32 flags;
	unsigned bin_count;
	unsigned ref_count;
	pwr_det_bin_t bins[PHYDEF_MAX_BINS];
} pwr_det_bin_dyn_t;

/* SNR detector for one channel */
typedef struct pwr_det_chan_s {
	struct pwr_det_chan_s *next;
	char *name;
	struct pwr_det_tech_s *tech;

	/* Identifier */
	u32 index;

	/* Configuration */
	double freq;

	/* Dynamic parameters */
	pwr_det_bin_dyn_t bin_dyn;

	/* State */
	pwr_det_state_t state;
	double cur_snr;
	double cur_boundsnr;
} pwr_det_chan_t; 

typedef struct pwr_det_tech_s {
	struct pwr_det_tech_s *next;
	char *name;
	pwr_det_chan_t *channels;

	/* Identifier */
	u32 index;

	/* Configuration */
	phydef_bin_spec_t bin_spec;
	double min_duration;

	/* Dynamic parameters */
	u32 bloc_history;

	/* state */
	float *fft_mag;	/* average buffer */
} pwr_det_tech_t, *pwr_det_tech_p;

/* Channel event structure */
typedef struct {
	pwr_det_evt_type_t type;
	pwr_det_chan_t *det;
	float value;
	float value2;
} pwr_det_event_t;

/* Chunk history */
#define HIST_FL_KEEP	1
typedef struct {
	u64 sample_time;		/* Timestamp */
	complex_t *samples;		/* Original data */
	float *fft_mag;			/* fft buffer */
	complex_t*fft_out;
	u32 event_count;		/* Event count for this chunk*/
	pwr_det_event_t *events;	/* Channel events*/
	u32 flags;			/* Chunk flags */
} pwr_det_hist_el_t;

/* History circular buffer manager */
typedef struct {
	int index;
	u32 size;
	u32 elem_max;
	u32 used_count;
	pwr_det_hist_el_t *elements;
} pwr_det_hist_t; 

/* SNR detector for a full band */
typedef struct pwr_det_ctx_s {
	/* Band parameters */
	double center_freq;
	double bandwidth;
	double low_freq;
	double high_freq;
	double noise_floor_dbm;
	double noise_floor_mag;

	/* FFT */
	unsigned buf_size;
	phydet_fft_p fft;

	/* Selected channels */
	u32 tech_count;
	pwr_det_tech_p tech;
	u32 chan_count;

	/* history*/
	pwr_det_hist_t history;

	/* Logging */
	tlv_file_logger_t *logger;
	int logger_has_header;
} pwr_det_ctx_t;

char * pwr_det_event_to_str(pwr_det_evt_type_t type){
	return   type==DET_EVENT_MATCH_START?"START"
		:type==DET_EVENT_MATCH_END?"END"
		:type==DET_EVENT_MATCH_CONT?"CONT"
		:"?";

}

static pwr_det_hist_el_t *pwr_det_hist_add_block(pwr_det_ctx_p ctx, complex_t *samples, u64 samp_time)
{
	unsigned i;
	pwr_det_hist_el_t *el;

	if (ctx->history.used_count < ctx->history.size)
		ctx->history.used_count += 1;

	el = &ctx->history.elements[ctx->history.index];
	el->sample_time = samp_time;
	memcpy(el->samples, samples, sizeof(complex_t)*ctx->buf_size);
	el->event_count = 0;
	el->flags = 0;

	for (i=0;i<ctx->chan_count;i++)
	{
		el->events[i].type = DET_EVENT_NONE;
	}
	
	return el;
}

static int pwr_det_hist_add_idx(pwr_det_ctx_p ctx, int offset)
{
	int idx;

	idx = ctx->history.index + offset;

	if(idx<0)
		idx += ctx->history.size;

	return idx;
}

static pwr_det_hist_el_t *pwr_det_hist_get(pwr_det_ctx_p ctx, int offset)
{
	pwr_det_hist_el_t *el;
	int idx = pwr_det_hist_add_idx(ctx, offset);

	if (ctx->history.used_count < idx)
		return NULL;

	el = &ctx->history.elements[idx];

	return el;
}

static void pwr_det_hist_free(pwr_det_ctx_p ctx)
{
	u32 i;

	if (ctx->history.size == 0)
		return;

	for(i=0;i<ctx->history.size;i++)
	{
		MEM_FREE(ctx->history.elements[i].samples);
		MEM_FREE(ctx->history.elements[i].fft_mag);
		MEM_FREE(ctx->history.elements[i].fft_out);
		MEM_FREE(ctx->history.elements[i].events);
	}
	ctx->history.size = 0;
	MEM_FREE(ctx->history.elements);
}

/* Reset the history */
static void pwr_det_hist_reset(pwr_det_ctx_p ctx)
{
	int i;
	pwr_det_tech_t *tech;
	u32 hist_max = 0;

	/* Assign index to channels*/
	for (tech = ctx->tech; tech; tech=tech->next)
	{
		if (tech->bloc_history > hist_max )
			hist_max = tech->bloc_history;
	}
	/* Keep one more for myself */
	hist_max += 1;

	/* Reset the history anyway */
	if (ctx->history.size != 0)
		pwr_det_hist_free(ctx);

	ctx->history.index = 0;
	ctx->history.used_count = 0;
	ctx->history.size = hist_max;
	ctx->history.elements = (pwr_det_hist_el_t *)MEM_ALLOC(sizeof(pwr_det_hist_el_t)*ctx->history.size);
	for(i=0;i<ctx->history.size;i++)
	{
		ctx->history.elements[i].sample_time = 0;
		ctx->history.elements[i].samples = (complex_t*) MEM_ALLOC_ALIGN(sizeof(complex_t)*ctx->buf_size);
		ctx->history.elements[i].fft_mag = (float*)calloc(1,sizeof(float)*ctx->buf_size);
		ctx->history.elements[i].fft_out = (complex_t*)MEM_ALLOC_ALIGN(sizeof(complex_t)*ctx->buf_size);
		ctx->history.elements[i].event_count = 0;
		ctx->history.elements[i].events = (pwr_det_event_t*)calloc(1,sizeof(pwr_det_event_t)*ctx->chan_count);
		ctx->history.elements[i].flags = 0;
	}
}

/* Add event with given type & detector at history hist_idx
 * We need to track all events types (start/cond/end) even though
 * we dont output cont events, in order to properly replace
 * existing events. */
static int pwr_det_add_event_helper(pwr_det_ctx_p ctx, int hist_idx, pwr_det_evt_type_t type, pwr_det_chan_t *chan)
{
	pwr_det_hist_el_t *el;
	pwr_det_event_t *event;

	el = &ctx->history.elements[hist_idx];

#ifdef LOG_DET
	printf("%2d | Add event %s in %30s\n", hist_idx, pwr_det_event_to_str(type),
				chan->name);
#endif
	/* Output all samples chunks with valid event */
	if(type != DET_EVENT_NONE)
		el->flags |= HIST_FL_KEEP;

	if (type != DET_EVENT_NONE)
	{
		event = &el->events[chan->index];

		if (event->type == DET_EVENT_NONE)
		{
			/* Write new event */
			el->event_count++;
			event->value = chan->cur_snr;
			event->type = type;
			event->det = chan;
		}
		else
		{
			/* There is already an event */
			if(event->type==type){
				/* Trying to replace cont with cont */
				assert(type==DET_EVENT_MATCH_CONT);
				return 0;
			}

			/* Overwrite existing event */
			switch(event->type){
			case DET_EVENT_MATCH_CONT:
				switch(type){
				case DET_EVENT_MATCH_START:
					/* keep (cont,cont,...) instead of writing (cont,start,...) */
					type = DET_EVENT_MATCH_CONT;
					break;
				default:
					assert(0);
					break;
				}	
				break;
			case DET_EVENT_MATCH_END:
				/* replace (cont,end,...) with cont,cont,...) */
				type = DET_EVENT_MATCH_CONT;
				break;
			default:
				/* unreachable*/
				assert(0);
				break;
			}
			assert(type==DET_EVENT_MATCH_CONT);
			/* Treat it as a continuation event */
			event->type = type; 
		}
	}

	return 0;
}

static int pwr_det_hist_add_event(pwr_det_ctx_p ctx, pwr_det_evt_type_t evt, pwr_det_chan_t *chan, u64 samp_time)
{
	int idx, i;
	int size = ctx->history.size;

	switch(evt){
	case DET_EVENT_NONE:
		break;
	case DET_EVENT_MATCH_START:
		idx = pwr_det_hist_add_idx(ctx, 1 - chan->tech->bloc_history);
		/* Add all history in match */
		pwr_det_add_event_helper(ctx, idx, DET_EVENT_MATCH_START, chan);
		for (i=1;i<chan->tech->bloc_history;i++)
		{
			if(++idx >= size)
				idx = 0;
			pwr_det_add_event_helper(ctx, idx, DET_EVENT_MATCH_CONT, chan);
		}
		break;
	case DET_EVENT_MATCH_END:
	case DET_EVENT_MATCH_CONT:
		idx = ctx->history.index;
		pwr_det_add_event_helper(ctx, idx, evt, chan);
		break;
	}
	return 0;
}

static pwr_det_hist_el_t *pwr_det_get_out_block(pwr_det_ctx_p ctx)
{
	pwr_det_hist_el_t *el;

	if (ctx->history.used_count != ctx->history.size)
		return NULL;

	el = &ctx->history.elements[ctx->history.index];

	if (!(el->flags & HIST_FL_KEEP))
		return NULL;

	return el;
}

static int pwr_det_log_header(pwr_det_ctx_p ctx)
{
	int rc;
	pwr_det_tech_t *tech;
	pwr_det_chan_t *chan;
	tlv_field_t *field;
	cfil_hdr_t hdr = {
		ctx->center_freq, ctx->bandwidth,
		ctx->noise_floor_dbm, ctx->buf_size
	};

	field = tlv_field_alloc_init('LIFC', &hdr, sizeof(hdr));

	/* Header */
	if((rc=tlv_file_logger_write_metas(ctx->logger, &field, 1)))
		return rc;
	MEM_FREE(field);

	/* Channel definitions */
	for(tech = ctx->tech; tech; tech=tech->next)
	{
		for (chan = tech->channels; chan; chan=chan->next)
		{
			field = tlv_field_alloc_init('FEDC', chan->name, strlen(chan->name));
			if((rc=tlv_file_logger_write_metas(ctx->logger, &field, 1)))
				return rc;
			MEM_FREE(field);
		}
	}
	field = tlv_field_alloc_init('FEDC', "", 0);
	if((rc=tlv_file_logger_write_metas(ctx->logger, &field, 1)))
		return rc;
	MEM_FREE(field);

#if 0
	/* (fake) GMT Time */
	if((rc=tlv_write_GTIM(fd, 0)))
		return rc;
#endif

	return SUCCESS;
}

/* Write all start/end events for current block */
static int pwr_det_log_EVNTS(pwr_det_ctx_p ctx)
{
	#define MAX_EVENT 512
	static struct PACKED {
		tlv_hdr_t hdr;
		cfil_evt_t evt[MAX_EVENT];
	} events;
	int i, count, eid;
	pwr_det_hist_el_t *el = &ctx->history.elements[ctx->history.index];

	assert(ctx->chan_count <= MAX_EVENT);

	/* Write events */
	for (i=0, eid=0, count=0; count<el->event_count; i++)
	{
		switch(el->events[i].type){
		case DET_EVENT_MATCH_START:
			events.evt[eid].chan_index = el->events[i].det->index;
			events.evt[eid].event = 1;
			events.evt[eid].value = el->events[i].value;
			events.evt[eid].value2 = el->events[i].value2;
			count++;
			eid++;
			break;
		case DET_EVENT_MATCH_END:
			events.evt[eid].chan_index = el->events[i].det->index;
			events.evt[eid].event = 0;
			events.evt[eid].value = el->events[i].value;
			events.evt[eid].value2 = el->events[i].value2;
			count++;
			eid++;
			break;
		case DET_EVENT_MATCH_CONT:
			count++;
			break;
		default:
			break;
		}
	}
	events.hdr.tag = 'TNVE';
	events.hdr.size = sizeof(cfil_evt_t)*eid;

	if(eid)
		return tlv_file_logger_write(ctx->logger, &events, (size_t)((void*)&events.evt[eid]-(void*)&events));

	return 0;
}

static int pwr_det_log_block(pwr_det_ctx_p ctx)
{
	int rc;
	pwr_det_hist_el_t *el;

	if (!ctx->logger_has_header)
	{
		if((rc=pwr_det_log_header(ctx)))
			return rc;
		ctx->logger_has_header = 1;
	}

	if (!(el = pwr_det_get_out_block(ctx)))
		return 0;

	if (!el)
		return 0;

	struct PACKED {
		tlv_hdr_t hdr;
		double time_ns;
	} stim = {{'MITS', sizeof(double)}, (double)el->sample_time};

	if((rc=tlv_file_logger_write(ctx->logger, &stim, sizeof(stim))))
		return rc;

	assert(el->event_count <= ctx->chan_count);

	pwr_det_log_EVNTS(ctx);

#ifdef USE_OUTPUT_FFT
#error "nyi"
	if((rc=tlv_write_DFFT(fd, ctx->buf_size, el->fft_out)))
		return rc;
#endif

#ifdef USE_OUTPUT_SAMPLES
	/* Write data */
	tlv_hdr_t data_hdr = {'ATAD', ctx->buf_size*sizeof(complex_t)};
	if((rc=tlv_file_logger_write(ctx->logger, &data_hdr, sizeof(data_hdr)))
	  ||(rc=tlv_file_logger_write(ctx->logger, el->samples, ctx->buf_size*sizeof(complex_t))))
		return rc;
#endif

	return 1;
}

int pwr_det_create(pwr_det_ctx_p *ctxp, pwr_det_parm_t *parm, double center_freq, double bw, double noise_floor_dbm)
{
	int rc;
	pwr_det_ctx_p ctx;

	if (!(ctx = MEM_ALLOC(sizeof(*ctx))))
		return NO_MEM;

	ctx->center_freq = center_freq;
	ctx->bandwidth = bw;
	ctx->low_freq = center_freq - bw/2;
	ctx->high_freq = center_freq + bw/2;
	ctx->noise_floor_dbm = noise_floor_dbm;
	ctx->noise_floor_mag = dbm2mag(noise_floor_dbm);
	ctx->chan_count = 0;
	ctx->tech_count = 0;
	ctx->tech = NULL;
	ctx->history.size = 0;
	ctx->buf_size = parm->fft_bin_count;
	phydet_fft_create(&ctx->fft, parm->fft_bin_count, parm->win_type, parm->ignore_dc);
	if (parm->log_path)
	{
		if((rc=tlv_file_logger_create(&ctx->logger, parm->log_path,0x40000)))
			return rc;
		ctx->logger_has_header = 0;
	}
	else
	{
		ctx->logger = NULL;
	}
	*ctxp = ctx;

	return SUCCESS;
}

static void pwr_det_chan_calc_threshold(pwr_det_ctx_p ctx, pwr_det_chan_t *s)
{
	unsigned i;
	pwr_det_bin_t *bin;

	for (i=0;i<s->bin_dyn.bin_count;i++)
	{
		bin = &s->bin_dyn.bins[i];
		if (bin->flags & PWR_DET_HAS_MIN_PWR)
			bin->min_bin_pwr = bin->size * dbm2mag(ctx->noise_floor_dbm + s->tech->bin_spec.bins[i].min_snr);
	}
}

static void pwr_det_tech_calc_threshold(pwr_det_ctx_p ctx, pwr_det_tech_t *t)
{
	pwr_det_chan_t *s;

	for (s = t->channels; s; s=s->next)
		pwr_det_chan_calc_threshold(ctx, s);
}

int pwr_det_set_noise_floor(pwr_det_ctx_p ctx, float noise_floor_dbm)
{
	pwr_det_tech_t *t;

	ctx->noise_floor_dbm = noise_floor_dbm;
	ctx->noise_floor_mag = dbm2mag(noise_floor_dbm);

	for (t=ctx->tech; t; t=t->next)
		pwr_det_tech_calc_threshold(ctx, t);

	return 0;
}

void pwr_det_destroy(pwr_det_ctx_p ctx)
{
	pwr_det_chan_t *ch, *chn;
	pwr_det_tech_t *t, *tn;
	
	for(t = ctx->tech; t; t=tn)
	{
		tn = t->next;
		/* remove all chans */
		for(ch = t->channels; ch; ch=chn)
		{
			chn = ch->next;
			MEM_FREE(ch->name);
			MEM_FREE(ch);
		}
		MEM_FREE(t->fft_mag);
		MEM_FREE(t->name);
		MEM_FREE(t);
	}
	pwr_det_hist_free(ctx);
	phydet_fft_destroy(ctx->fft);
	MEM_FREE(ctx);
}

/* Compute internal parameters for the given channel */
static int pwr_det_chan_init(pwr_det_ctx_p ctx, pwr_det_chan_t *s)
{
	unsigned i;
	pwr_det_bin_t *bin;
	phydef_bin_t *bin_spec;

	s->state = DET_STATE_IDLE;
	s->bin_dyn.flags = 0;
	s->bin_dyn.ref_count = 0;
	s->bin_dyn.bin_count = s->tech->bin_spec.bin_count;

	/* Compute parameters for each bin */
	for (i=0; i<s->tech->bin_spec.bin_count; i++)
	{
		bin_spec = &s->tech->bin_spec.bins[i]; 
		bin = &s->bin_dyn.bins[i];
		/* Map bin bounds to fft bins */
		if(find_bin_indexes(ctx->center_freq, ctx->bandwidth, ctx->buf_size,
			s->freq+bin_spec->min_freq,
			s->freq+bin_spec->max_freq, &bin->range))
		{
			return -1;
		}
		bin->size = bin->range.last-bin->range.first+1;

		/* Compute noise-floor relative threshold for this bin */
		if (bin_spec->min_snr != 0)
		{
			bin->min_bin_pwr = bin->size * dbm2mag(ctx->noise_floor_dbm + bin_spec->min_snr);
			bin->flags |= PWR_DET_HAS_MIN_PWR;
		}
		if (bin_spec->max_snr != 0)
		{
			/* Avoid log-scale at runtime */
			bin->max_bin_snr = pow(10.f, bin_spec->max_snr/10.f);
			bin->flags |= PWR_DET_HAS_MAX_SNR;
			s->bin_dyn.flags |= PWR_DET_HAS_MAX_SNR;
		}
		else
		{
			bin->flags |= PWR_DET_IS_REF_PWR;
			s->bin_dyn.ref_count += bin->size;
		}
	}
	/* Adjust relative levels according to number of reference bins */
	for (i=0; i<s->tech->bin_spec.bin_count; i++)
	{
		bin = &s->bin_dyn.bins[i];
		/* Save one runtime division by scaling max_snr there */
		if (bin->flags & PWR_DET_HAS_MAX_SNR)
			bin->max_bin_snr *= (double)bin->size / (double)s->bin_dyn.ref_count;
	}	

	return 0;
}

/* Returns 1 if bin_spec is valid */
static int bin_spec_valid(phydef_bin_spec_t *bin_spec) 
{
	unsigned i;
	unsigned has_ref_lvl = 0, has_max_snr = 0, has_min_snr = 0;

	if (bin_spec->bin_count > PHYDEF_MAX_BINS)
		return 0;

	/* Check validity of bin specification */
	for (i=0;i<bin_spec->bin_count;i++)
	{
		if (bin_spec->bins[i].min_snr != 0)
		{
			if (bin_spec->bins[i].min_snr < 0)
			{
				LOG_ERROR("minSNR must be >= 0");
				return -1;
			}
			has_min_snr = 1;
		}
		if (bin_spec->bins[i].max_snr != 0)
		{
			if (bin_spec->bins[i].max_snr > 0)
			{
				LOG_ERROR("maxSNR must be <= 0");
				return -1;
			}
			has_max_snr = 1;
		}
		else
			has_ref_lvl = 1;
	}
	if (!has_min_snr && !has_max_snr) // nothing to do
	{
		LOG_ERROR("No band selector parameters");
		return 0;
	}
	if (has_max_snr && !has_ref_lvl)
	{
		LOG_ERROR("Missing ref level in maxSNR");
		return 0;
	}
	return 1;
}

int pwr_det_tech_add(pwr_det_ctx_p ctx, char *name, band_sel_conf_t *bsel)
{
	unsigned sample_history;
	pwr_det_tech_t *t, *tmp;

	if (!bin_spec_valid(&bsel->bin_spec))
		return -1;

	t = calloc(sizeof(*t), 1);
	t->name = strdup(name);
	t->min_duration = bsel->min_duration;
	t->bin_spec = bsel->bin_spec;

	t->fft_mag = MEM_ALLOC(sizeof(complex_t)*ctx->buf_size);
	memset(t->fft_mag, 0, sizeof(complex_t)*ctx->buf_size);
	
	sample_history = t->min_duration*ctx->bandwidth;
	t->bloc_history = (sample_history + ctx->buf_size-1) / ctx->buf_size;
	LOG_INFO("Tech %s: bloc history = %d", t->name, t->bloc_history);

	/* We need to reset history to resize it as required */
	pwr_det_hist_reset(ctx);

	/* Tech index */
	tmp = (pwr_det_tech_t*)&ctx->tech;
	while(tmp->next) tmp = tmp->next;
	tmp->next = t;
	t->index = ctx->tech_count++;

	return t->index;
}

static pwr_det_tech_t *pwr_det_tech_by_idx(pwr_det_ctx_p ctx, unsigned tech_idx)
{
	unsigned i;
	pwr_det_tech_t *t = ctx->tech;

	if (tech_idx >= ctx->tech_count)
		return NULL;
	for (i=0; i<tech_idx; i++, t=t->next);
	return t;
}

int pwr_det_tech_chan_add(pwr_det_ctx_p ctx, unsigned tech_idx, char *name, double center_freq)
{
	pwr_det_tech_t *tech;
	pwr_det_chan_t *s, *tmp;

	if (!(tech = pwr_det_tech_by_idx(ctx, tech_idx)))
		return -1;

	/* Initialize parameters */
	s = calloc(sizeof(*s), 1);
	s->name = strdup(name);
	s->freq = center_freq;
	s->tech = tech;

	/* Initialize internals */
	if(pwr_det_chan_init(ctx, s))
	{
		free(s->name);
		free(s);
		return -1;
	}

	/* FIXME: channel ordering must match index*/
	tmp = (pwr_det_chan_t*)&tech->channels;
	while(tmp->next) tmp = tmp->next;
	tmp->next = s;
	s->index = ctx->chan_count++;

	/* Resize history */
	pwr_det_hist_reset(ctx);

	return 0;
}

/* Update rolling average of psd for given tech */
static void pwr_det_tech_update_psd(pwr_det_ctx_p ctx, pwr_det_tech_p tech)
{
	unsigned i;
	pwr_det_hist_el_t *old, *new;

	new = pwr_det_hist_get(ctx, 0); 
	old = pwr_det_hist_get(ctx, -tech->bloc_history); 

	if (!new || !old)
		return;

	for (i=0; i<ctx->buf_size; i++)
	{
		tech->fft_mag[i] += (new->fft_mag[i] - old->fft_mag[i]) / tech->bloc_history;
	}
}

static pwr_det_evt_type_t pwr_det_chan_execute(pwr_det_ctx_p ctx, pwr_det_chan_t *chan)
{
	pwr_det_bin_t *bin;
	unsigned i,j;
	float *magbuf = chan->tech->fft_mag;
	double ref_sum = 0, snr_dif;
	int matching;

	matching = 1;

	/* Compute sums for all bins and check power thresholds */
	for (i=0; i<chan->bin_dyn.bin_count; i++)
	{
		bin = &chan->bin_dyn.bins[i];
		bin->pwr_sum = 0.f;
		for (j=bin->range.first; j<=bin->range.last;j++)
			bin->pwr_sum += magbuf[j];
		if (bin->flags & PWR_DET_HAS_MIN_PWR)
		{
			/* Exit now if minSNR is not satisfied*/
			if (bin->pwr_sum < bin->min_bin_pwr)
			{
				matching = 0;
				break;
			}
		}
		if (bin->flags & PWR_DET_IS_REF_PWR)
		{
			ref_sum += bin->pwr_sum;
		}
	}
	/* Check for relative levels */
	if (matching && chan->bin_dyn.flags & PWR_DET_HAS_MAX_SNR)
	{
		/* Compute reference level  */
		for (i=0; i<chan->bin_dyn.bin_count; i++)
		{
			bin = &chan->bin_dyn.bins[i];
			if (bin->flags & PWR_DET_HAS_MAX_SNR)
			{
				snr_dif = bin->pwr_sum / ref_sum;
				if (snr_dif > bin->max_bin_snr)
				{
					matching = 0;
					break;
				}
			}
		}
	}
	if (chan->state == DET_STATE_IDLE)
	{
		/* cur_snr is estimated with ratio of inner/outer bands power.
		 * Negative values typically occurs when a signal with a way larger
		 * bandwidth than the chan detectors's occured. */
		if  (matching)
		{
#ifdef LOG_DET
			printf("[+++ %30s]\n", chan->name);
#endif
			return DET_EVENT_MATCH_START;
		}
		else return DET_EVENT_NONE;
	}
	else
	{
		if (!matching)
		{
#ifdef LOG_DET
			printf("[--- %30s]\n", chan->name);
#endif
			return DET_EVENT_MATCH_END;
		}
		else return DET_EVENT_MATCH_CONT;
	}
}

int pwr_det_out_write_header(pwr_det_ctx_p ctx, int fd)
{
	int rc;
	pwr_det_tech_t *tech;
	pwr_det_chan_t *chan;

	/* Header */
	if((rc=tlv_write_CFIL(fd, ctx->center_freq, ctx->bandwidth, ctx->noise_floor_dbm, ctx->buf_size)))
		return rc;

	/* Channel definitions */
	for(tech = ctx->tech; tech; tech=tech->next)
	{
		for (chan = tech->channels; chan; chan=chan->next)
		{
			if((rc=tlv_write_CDEF(fd, chan->name)))
				return rc;
		}
	}
	if((rc=tlv_write_CDEF(fd, "")))
		return rc;

	/* (fake) GMT Time */
	if((rc=tlv_write_GTIM(fd, 0)))
		return rc;
	return 0;
}

int pwr_det_out_block(pwr_det_ctx_p ctx, sbuf_p *sb, pwr_det_out_evt_t *evts, size_t evt_max)
{
	int i,count;
	pwr_det_hist_el_t *el = pwr_det_get_out_block(ctx);

	if (!el)
		return 0;

	assert(evt_max >= el->event_count);

	/* TODO: wrap those */
	if(!(*sb = sbuf_alloc()))
		return -1;
	
	sbuf_fill(*sb, el->samples, el->fft_out, el->fft_mag, el->sample_time, 0);

	/* Write events */
	for (i=0, count=0; count<el->event_count; i++)
	{
		if (el->events[i].type != DET_EVENT_NONE)
		{
			evts[count].chan_index = el->events[i].det->index;
			evts[count].event = el->events[i].type;
			evts[count].value = el->events[i].value;
			evts[count].value2 = el->events[i].value2;
			count++;
		}
	}

	return count;
}

int pwr_det_execute(pwr_det_ctx_p ctx, complex_t *samples, unsigned len, u64 samp_time)
{
	pwr_det_tech_p tech;
	pwr_det_chan_t *chan;
	pwr_det_evt_type_t evt;
	bool_t ready;
	pwr_det_hist_el_t *el;
	int rv = 0;

	if (len != ctx->buf_size)
	{
		return -1;
	}

	/* 1 - Update rx history buffers */
	el = pwr_det_hist_add_block(ctx, samples, samp_time);

	/* 2 - Execute detection */
	phydet_fft_execute(ctx->fft, el->samples, el->fft_mag);

	memcpy(el->fft_out, ctx->fft->plan_out, sizeof(complex_t)*ctx->buf_size);

	/* Wait for all history to be filled before considering ready.
	 * we could be doing it per phy, but whatever */
	ready = ctx->history.used_count == ctx->history.size;

	for (tech = ctx->tech; tech; tech=tech->next)
	{
		pwr_det_tech_update_psd(ctx, tech);
		for (chan = tech->channels; chan; chan=chan->next)
		{
			evt = pwr_det_chan_execute(ctx, chan);
			if(evt && ready)
			{
				if(evt==DET_EVENT_MATCH_START){
					assert(chan->state==DET_STATE_IDLE);
					chan->state = DET_STATE_MATCHED;
					rv ++;
				}
				else if (evt == DET_EVENT_MATCH_END){
					assert(chan->state==DET_STATE_MATCHED);
					chan->state = DET_STATE_IDLE;
				}
				pwr_det_hist_add_event(ctx, evt, chan, samp_time);
			}
		}
	}
	/* 3 - Update history index */
	ctx->history.index = (ctx->history.index + 1)%ctx->history.size;

	if (ctx->logger)
	{
		pwr_det_log_block(ctx);
	}

	return rv;
}

