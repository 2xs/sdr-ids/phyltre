#include <stdio.h>
#include <string.h>
#include <volk/volk.h>
#include <phydet/acq_src.h>
#include <phydet/log.h>
#include <phydet/dsp_util.h>
#include <phydet/fft.h>
#include <phydet/mem.h>
#include <phydet/nf_est.h>
#include "ini.h"

extern acq_src_drv_intf_t acq_src_file_intf;
extern acq_src_drv_intf_t acq_src_soapy_intf;

static struct drv_lut_s {
	char *name;
	acq_src_drv_intf_t *intf;
} drv_lut[] = {
	{"file", &acq_src_file_intf},
	{"soapy", &acq_src_soapy_intf},
	{NULL},
};

int acq_src_determine_noise_floor(acq_src_drv_t *drv,  unsigned fft_size, win_type_t wtype, unsigned fft_count, float *floor)
{
	int rc;
	size_t i;
	nfe_p nfe;
	complex_t *rxbuf;
	u64 time_ns;
	float floor_med, floor_avg;
	phydet_fft_p fft;
	float *magbuf;

	if (!(magbuf=MEM_ALLOC(sizeof(float)*fft_count)))
		return NO_MEM;

	if ((rc=phydet_fft_create(&fft, fft_size, wtype, 0)))
		return rc;

	if((rc=nfe_create(&nfe, fft_size, fft_count)))
		return rc;

	rxbuf = MEM_ALLOC_ALIGN(sizeof(complex_t)*fft_size);

	acq_src_start_stream(drv);
	for(i=0;i<fft_count;i++)
	{
		if(acq_src_read_stream(drv, rxbuf, fft_size, &time_ns) != fft_size)
			return -1;
		phydet_fft_execute(fft, rxbuf, magbuf);
		nfe_execute(nfe, magbuf, fft_size);
	}
	acq_src_stop_stream(drv);

	floor_avg = nfe_average(nfe);
	floor_med = nfe_median(nfe);
	printf("Noise floor: med=%.2f, avg=%.2f\n", floor_med, floor_avg);

	MEM_FREE(rxbuf);
	MEM_FREE(magbuf);
	nfe_destroy(nfe);
	phydet_fft_destroy(fft);

	*floor = floor_med;

	return 0;
}

typedef struct {
	size_t count;
	size_t idx;
	complex_t result;
} dc_cb_data_t ;

static int acq_src_dc_cal_cb(acq_src_drv_t *drv, complex_t *samples, u32 count, u64 timestamp, void *data_)
{
	size_t i;
	complex_t c = 0;
	dc_cb_data_t *data = (dc_cb_data_t*)data_;

	for (i=0; i<count; i++){
		c += samples[i];
	}
	data->result += c;
	if((data->idx += count) >= data->count){
		data->result /= data->idx;
		return 1;
	}
	return 0;
}

#define DC_CAL_SAMPLES 0x20000
static int acq_src_dc_calibration(acq_src_drv_t *drv)
{
	int rc;
	dc_cb_data_t data;
	data.idx = 0;
	data.count = DC_CAL_SAMPLES;
	data.result = 0;

	fprintf(stderr, "DC calibration ... ");

	if (acq_src_stream_rx(drv, acq_src_dc_cal_cb, &rc, &data) || rc != 1)
	{
		LOG_ERROR("stream_rx error");
		return 1;
	}
	fprintf(stderr, "I=%.6f, Q=%.6f\n", creal(data.result), cimag(data.result));
	drv->dc_val = data.result;
	return 0;
}


/* Open driver & setup initial capture configuration */
int acq_src_init_parm(acq_src_drv_t* drv, acq_src_parm_t *parm)
{
	drv->args = strdup(parm->driver_args);
	drv->block_dc = parm->block_dc;
	if (drv->intf->open(drv)
	||	acq_src_set_bw(drv, parm->start_bw)
	||	acq_src_set_freq(drv, parm->start_freq)
	||	acq_src_set_gain(drv, parm->start_gain)
	){
		return 1;
	}
	if(drv->block_dc)
		acq_src_dc_calibration(drv);

	return 0;
}

/* Find & init driver */
acq_src_drv_t* acq_src_create(acq_src_parm_t *parm)
{
	acq_src_drv_t *drv;
	struct drv_lut_s *lut;

	drv = calloc(1, sizeof(*drv));

	for(lut=drv_lut; lut->name; lut++)
	{
		if (!strcmp(lut->name, parm->driver))
		{
			drv->intf = lut->intf;
			if(acq_src_init_parm(drv, parm))
				goto err;
			return drv;
		}
	}
	LOG_ERROR("Driver '%s' not supported", parm->driver);
err:
	free(drv);
	return NULL;
}

void acq_src_destroy(struct acq_src_drv_s *drv)
{
	drv->intf->close(drv);
	MEM_FREE(drv->args);
	MEM_FREE(drv);
}

int acq_src_set_freq(struct acq_src_drv_s *drv, double freq)
{
	int rc = 0;

	if (freq != drv->cur_freq)
	{
		if (!(rc = drv->intf->set_freq(drv, freq)))
			drv->cur_freq = freq;
	}
	return rc;
} 
int acq_src_set_bw(struct acq_src_drv_s *drv, double bw)
{
	int rc = 0;
	if (bw != drv->cur_bw)
	{
		if (!(rc = drv->intf->set_bw(drv, bw)))
			drv->cur_bw = bw;
	}
	return rc;
}
int acq_src_set_gain(struct acq_src_drv_s *drv, double gain)
{
	int rc = 0;
	if (drv->cur_gain != gain)
	{
		if (!(rc = drv->intf->set_gain(drv, gain)))
			drv->cur_gain = gain;
	}
	return rc;
}
int acq_src_get_freq(struct acq_src_drv_s *drv, double *freq)
{
	*freq = drv->cur_freq;
	return 0;
}
int acq_src_get_bw(struct acq_src_drv_s *drv, double *bw)
{
	*bw = drv->cur_bw;
	return 0;
}
int acq_src_get_gain(struct acq_src_drv_s *drv, double *gain)
{
	*gain = drv->cur_gain;
	return 0;
}
int acq_src_start_stream(struct acq_src_drv_s *drv)
{
	int rc = 0;

	if (!drv->streaming)
	{
		if(!(rc = drv->intf->start_stream(drv)))
			drv->streaming = 1;
	}
	return rc;
}
int acq_src_stop_stream(struct acq_src_drv_s *drv)
{
	int rc = 0;

	if (drv->streaming)
	{
		if(!(rc = drv->intf->stop_stream(drv)))
			drv->streaming = 0;
	}
	return rc;
}

int acq_src_read_stream(struct acq_src_drv_s *drv, complex_t *buf, size_t count, u64 *time_ns)
{
	int cnt, i;

	if ((cnt = drv->intf->read_stream(drv, buf, count, time_ns)) > 0)
	{
		/* Apply DC correction there */
		if (drv->block_dc)
		{
			for(i=0;i<count;i++)
				buf[i] -= drv->dc_val;
		}
	}
	return cnt;
}

int acq_src_stream_rx(struct acq_src_drv_s *drv, acq_stream_rx_cb_t cb, int *cb_ret, void *cb_data)
{
	#define ACQ_BUF_COUNT 4096
	complex_t *buf;
	u64 time_ns;
	int cnt,rc;
	int rv = 0;

	buf = MEM_ALLOC_ALIGN(ACQ_BUF_COUNT*sizeof(complex_t));

	if (acq_src_start_stream(drv))
		return -1;

	while (1)
	{
		if ((cnt = acq_src_read_stream(drv, buf, ACQ_BUF_COUNT, &time_ns)) != ACQ_BUF_COUNT)
		{
			printf("invalid read %d\n", cnt);
			rv = -1;
			break;
		}

		if ((rc=cb(drv, buf, cnt, time_ns, cb_data)))
		{
			*cb_ret = rc;
			break;
		}
	}
	acq_src_stop_stream(drv);
	MEM_FREE(buf);

	return rv;
}

void acq_src_print(acq_src_parm_t *src)
{
	printf("ACQ Src '%s': %s:%s\n", src->name, src->driver, src->driver_args);
}
