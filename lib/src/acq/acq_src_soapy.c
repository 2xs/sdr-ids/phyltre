#include <stdlib.h>
#include <string.h>
#include <phydet/types.h>
#include <phydet/acq_src.h>
#include <phydet/log.h>
#include <phydet/mem.h>
#include <unistd.h>
#include <SoapySDR/Device.h>
#include <SoapySDR/Formats.h>

#define SOAPY_BUF_SIZE 8192

static void soapy_get_infos(SoapySDRDevice *sdr);

/* parsed driverArgs for soapy rx device */
typedef struct {
	char *driver;
	char *antenna;
	size_t channel;
} soapy_opt_t;

typedef struct {
	SoapySDRDevice *sdr;
	SoapySDRStream *rx_stream;
	soapy_opt_t opt;
	long long total_samp;
} soapy_src_t;

static void soapy_parse_driverArgs(char *str, soapy_opt_t *opt)
{
#define SEP1 ","
#define SEP2 "="
        char *str1, *str2, *saveptr1, *saveptr2;
        char *token, *arg, *val;

	opt->driver="";
	opt->antenna="";
	opt->channel=0;

        memset(opt, 0, sizeof(*opt));
        for (str1 = str; ; str1 = NULL)
        {
                if(!(token = strtok_r(str1, SEP1, &saveptr1)))
                        break;
                str2 = token;
                arg = strtok_r(str2, SEP2, &saveptr2);
                val = strtok_r(NULL, SEP2, &saveptr2);
                if(!(arg&&val))
                        break;
                if(!strcmp(arg,"driver")){
			opt->driver = val;
		}
                else if(!strcmp(arg,"antenna")){
			opt->antenna = val;
		}
                else if(!strcmp(arg,"channel")){
			opt->channel = strtoul(val, NULL, 10);
		}
	}
}

SoapySDRDevice *soapy_find_any(void)
{
	size_t length, i, j;
	SoapySDRKwargs *results;
	SoapySDRDevice *dev = NULL;

	results = SoapySDRDevice_enumerate(NULL, &length);
	for (i=0; i<length; i++)
	{
		printf("Found device #%d:\n", (int) i);
		for (j=0; j<results[i].size; j++)
		{
			printf("  %s=%s\n", results[i].keys[j], results[i].vals[j]);
		}
		puts("");
	}
	dev = SoapySDRDevice_make(&results[0]);
	SoapySDRKwargsList_clear(results, length);
	return dev;
}

static int soapy_read_lo_locked_(soapy_src_t*src, int dir)
{
	int rc;
	char *v = SoapySDRDevice_readChannelSensor(src->sdr, dir, src->opt.channel, "lo_locked");

	if (v){
		rc = !*v||!strcmp(v, "true");
		free(v);
		return rc;
	}
	return 1;
}
static int soapy_read_clock_locked_(soapy_src_t*src)
{
	int rc;
	char *v = SoapySDRDevice_readSensor(src->sdr, "clock_locked");

	if (v)
	{
		rc = !*v||!strcmp(v, "true");
		free(v);
		return rc;
	}
	return 1;
}

static int soapy_set_freq(acq_src_drv_t *drv, double freq)
{
	soapy_src_t *src = (soapy_src_t*) drv->data;

	LOG("Setting rx freq %f", freq);
	if (SoapySDRDevice_setFrequency(src->sdr, SOAPY_SDR_RX,
				src->opt.channel, freq, NULL) != 0)
	{
		printf("setFrequency fail: %s\n", SoapySDRDevice_lastError());
		return -1;
	}
	while(!soapy_read_lo_locked_(src, SOAPY_SDR_RX)){
		usleep(1000);
	}
	return 0;
}

static int soapy_set_gain(acq_src_drv_t *drv, double gain)
{
	soapy_src_t *src = (soapy_src_t*) drv->data;

	/* Disable AGC */
	if(SoapySDRDevice_setGainMode(src->sdr, SOAPY_SDR_RX,
					src->opt.channel, 0))
	{
		LOG( "SoapySDRDevice_setGainMode fail: %s",
			SoapySDRDevice_lastError());
		return -1;
	}
	LOG("Setting gain %f", gain);
	if (!strcmp(src->opt.driver,"lime"))
	{
		if(SoapySDRDevice_setGainElement(src->sdr, SOAPY_SDR_RX,
			src->opt.channel, "TIA", 12))
		{
			LOG( "setGain TIA fail: %s",
				SoapySDRDevice_lastError());
			return -1;
		}
		if(SoapySDRDevice_setGainElement(src->sdr, SOAPY_SDR_RX,
			src->opt.channel, "PGA", 3))
		{
			LOG( "setGain PGA fail: %s",
				SoapySDRDevice_lastError());
			return -1;
		}
		if(SoapySDRDevice_setGainElement(src->sdr, SOAPY_SDR_RX,
			src->opt.channel, "LNA", gain))
		{
			LOG( "setGain LNA fail: %s",
				SoapySDRDevice_lastError());
			return -1;
		}
	}
	else
	{
		if(SoapySDRDevice_setGain(src->sdr, SOAPY_SDR_RX,
			src->opt.channel, gain))
		{
			LOG( "setGain fail: %s",
				SoapySDRDevice_lastError());
			return -1;
		}
	}
	return 0;
}

static int soapy_set_bw(acq_src_drv_t *drv, double bw)
{
	soapy_src_t *src = (soapy_src_t*) drv->data;
	char bwbuf[24];

	LOG("Setting sampleRate first %e", bw);
	if (SoapySDRDevice_setSampleRate(src->sdr,
			SOAPY_SDR_RX, src->opt.channel, bw) != 0)
	{
		printf("setSampleRate fail: %s\n",
			SoapySDRDevice_lastError());
		return -1;
	}
	while (!soapy_read_clock_locked_(src))
	{
		usleep(1000);
	}
	LOG("Setting bandwidth %e", bw);
	if (SoapySDRDevice_setBandwidth(src->sdr,
			SOAPY_SDR_RX, src->opt.channel, bw) != 0)
	{
		printf("setBandwidth fail: %s\n",
			SoapySDRDevice_lastError());
		return -1;
	}

	/* FIXME: lime calibration */
	if (!strcmp(src->opt.driver,"lime"))
	{
		LOG("Lime RX calibration");
		soapy_set_gain(drv, 40);
		snprintf(bwbuf, 24, "%f", bw);
		SoapySDRDevice_writeSetting(src->sdr, "CALIBRATE_RX", bwbuf);
		soapy_set_gain(drv, drv->cur_gain);
	}

	return 0;
}


static int soapy_open(acq_src_drv_t *drv)
{
	SoapySDRKwargs args = {};
	soapy_src_t *src;

	drv->data = src = (soapy_src_t*)calloc(1,sizeof(soapy_src_t));
	soapy_parse_driverArgs(drv->args, &src->opt);

	if (!src->opt.driver)
	{
		src->sdr = soapy_find_any();
	}
	else
	{
		LOG("Making soapy '%s'", src->opt.driver);
		SoapySDRKwargs_set(&args, "driver", src->opt.driver);
		src->sdr = SoapySDRDevice_make(&args);
		SoapySDRKwargs_clear(&args);
	}

	if (!src->sdr){
		LOG( "SoapySDRDevice_make fail: %s",
			SoapySDRDevice_lastError());
		return -1;
	}
	LOG( "Found SoapySDR device:");

	if (!(src->rx_stream = SoapySDRDevice_setupStream(src->sdr,
		SOAPY_SDR_RX, SOAPY_SDR_CF32, &src->opt.channel, 1, NULL)))
	{
		LOG( "RX SoapySDRDevice_setupStream fail: %s",
			SoapySDRDevice_lastError());
		return -1;
	}

	soapy_get_infos(src->sdr);
	
	if (src->opt.antenna)
	{
		LOG( "Setting antenna %s", src->opt.antenna);
		if(SoapySDRDevice_setAntenna(src->sdr, SOAPY_SDR_RX,
						src->opt.channel, src->opt.antenna))
		{
			LOG( "SoapySDRDevice_setAntenna fail: %s",
				SoapySDRDevice_lastError());
			return -1;
		}
	}

	

	return 0;
}

static int soapy_start_stream(acq_src_drv_t *drv)
{
	soapy_src_t *src = (soapy_src_t*) drv->data;

	if (SoapySDRDevice_activateStream(src->sdr, src->rx_stream, 0, 0, 0))
	{
		LOG( "SoapySDRDevice_activateStream fail: %s",
			SoapySDRDevice_lastError());
		return -1;
	}
	return 0;
}

static int soapy_stop_stream(acq_src_drv_t *drv)
{
	soapy_src_t *src = (soapy_src_t*) drv->data;

	if(SoapySDRDevice_deactivateStream(src->sdr,
			src->rx_stream, 0, 0))
	{
		LOG( "SoapySDRDevice_deactivateStream fail: %s",
			SoapySDRDevice_lastError());
		return -1;
	}
	return 0;
}

static int soapy_close(acq_src_drv_t *drv)
{
	int rc;
	soapy_src_t *src = (soapy_src_t*) drv->data;

	SoapySDRDevice_closeStream(src->sdr, src->rx_stream);
	rc = SoapySDRDevice_unmake(src->sdr);
	
	MEM_FREE(src);
	return rc;
}

static int soapy_read_stream(acq_src_drv_t *drv, complex_t *buf, size_t num, u64 *time_ns)
{
	soapy_src_t *src = (soapy_src_t*) drv->data;
	int cnt, total = 0;
	int flags = 0;
	long long *t = (long long*)time_ns, x_;

	while(total != num)
	{
		cnt = SoapySDRDevice_readStream(src->sdr, src->rx_stream,
			(void**)&buf, num-total, &flags, total > 0 ? &x_ : t, 500000);
		/* XXX: DEBUG*/
		if (cnt < 0)
		{
			if (cnt == SOAPY_SDR_TIMEOUT){
				putchar('U');
			}
			else if (cnt == SOAPY_SDR_OVERFLOW)
			{
				/* FIXME: what happens here with bladerf 2.0 ? */
				putchar('W'); 
				LOG("WTF");
			}
			else
			{
				LOG( "SoapySDRDevice_readStream fail (%d): %s",
					cnt, SoapySDRDevice_lastError());
				return -1;
			}
		}
		else {
			//printf("readen %d\n", cnt);
			total += cnt;
		}
	}
	src->total_samp += total;
	/* FIXME: hack */
	if (!strcmp(src->opt.driver, "hackrf"))
	{
		double bw;
		bw = SoapySDRDevice_getBandwidth(src->sdr,
				SOAPY_SDR_RX, src->opt.channel);
		*time_ns = src->total_samp*1e9/bw;
	}
	return total;
}

static void soapy_get_infos(SoapySDRDevice *sdr)
{
	char **names;
	size_t l, i, chan, num_chan, dir;
	SoapySDRRange range, *ranges;

	num_chan = SoapySDRDevice_getNumChannels(sdr, SOAPY_SDR_RX);

	for (chan=0;chan<num_chan; chan++){
		fprintf(stderr, "RX channel %lu:\n", chan);

		ranges = SoapySDRDevice_getBandwidthRange(sdr,
			SOAPY_SDR_RX, chan, &l);
		for(i=0;i<l;i++){
			printf("BW range %lu: %e - %e (step %e)\n",
				i, ranges[i].minimum,ranges[i].maximum,ranges[i].step);
		}
		free(ranges);
		ranges = SoapySDRDevice_getSampleRateRange(sdr,
			SOAPY_SDR_RX, chan, &l);
		for(i=0;i<l;i++){
			printf("SR range %lu: %e - %e (step %e)\n",
				i, ranges[i].minimum,ranges[i].maximum,ranges[i].step);
		}
		free(ranges);

		names = SoapySDRDevice_listAntennas(sdr, SOAPY_SDR_RX, chan, &l);
		fprintf(stderr, "\tAntennas: ");
		for (i=0;i<l;i++)
			fprintf(stderr, "%s, ", names[i]);
		SoapySDRStrings_clear(&names, l);
		fprintf(stderr, "\n");
#if 0
		names = SoapySDRDevice_listGains(sdr, SOAPY_SDR_RX, chan, &l);
		LOG( "\tGains: ");
		for (i = 0; i < l; i++)
			LOG( "'%s', ", names[i]);
		LOG( "\n");
		SoapySDRStrings_clear(&names, l);
#endif
	}


	names = SoapySDRDevice_listSensors(sdr, &l);
	fprintf(stderr, "Sensors: ");
	for (i=0;i<l;i++){
		fprintf(stderr, "  %s", names[i]);
	}
	SoapySDRStrings_clear(&names, l);
	fprintf(stderr, "\n");
	for (dir=0;dir<=1;dir++){
		names = SoapySDRDevice_listChannelSensors(sdr, dir, 0, &l);
		fprintf(stderr, "Channel sensors %s: ", dir==SOAPY_SDR_RX?"RX":"TX");
		for (i=0;i<l;i++){
			fprintf(stderr, "  %s", names[i]);
		}
		fprintf(stderr, "\n");
		SoapySDRStrings_clear(&names, l);
	}
	for(dir=0;dir<=1;dir++){
		names = SoapySDRDevice_listGains(sdr, dir, 0, &l);
		fprintf(stderr, "Gain elements for %s:\n", dir==SOAPY_SDR_RX?"RX":"TX");
		for (i=0;i<l;i++){
			range = SoapySDRDevice_getGainElementRange(sdr, dir, 0, names[i]);
			fprintf(stderr, "  %s: %.1f - %.1f (step %.1f)\n", names[i],
				range.minimum, range.maximum, range.step);
			
		}
		SoapySDRStrings_clear(&names, l);
	}
}

acq_src_drv_intf_t acq_src_soapy_intf = {
	.open		= soapy_open,
	.close		= soapy_close,
	.set_freq	= soapy_set_freq,
	.set_bw		= soapy_set_bw,
	.set_gain	= soapy_set_gain,
	.start_stream	= soapy_start_stream,
	.stop_stream	= soapy_stop_stream,
	.read_stream	= soapy_read_stream,
};
