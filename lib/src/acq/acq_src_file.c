#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <phydet/acq_src.h>
#include <phydet/log.h>
#include <phydet/mem.h>

typedef struct {
	int fd;
	u64 file_pos;
} file_drv_t;

static inline u64 pos_to_ns(acq_src_drv_t *drv, u64 samp_cnt)
{
	return (u64)((samp_cnt*1e9) / drv->cur_bw);
}

static int file_open(acq_src_drv_t *drv)
{
	int fd;
	file_drv_t *d;

	if (-1==(fd=open(drv->args, O_RDONLY)))
	{
		LOG_ERROR("Unable to open file '%s'", drv->args);
		return -1;
	}
	/* I like this malloc */
	d = malloc(sizeof (*d));
	d->fd = fd;
	d->file_pos = 0;
	drv->data = d;

	return 0;
}

static int file_close(acq_src_drv_t *drv)
{
	file_drv_t *d = drv->data;

	close(d->fd);
	free(d);
	drv->data = NULL;

	return 0;
}

static int file_set_freq(acq_src_drv_t *drv, double freq)
{
	return 0;
}

static int file_set_bw(acq_src_drv_t *drv, double bw)
{
	return 0;
}

static int file_set_gain(acq_src_drv_t *drv, double gain)
{
	return 0;
}

static int file_read_stream(acq_src_drv_t *drv, complex_t *buf, size_t count, u64 *time_ns)
{
	int len;
	file_drv_t *d = drv->data;

	len = read(d->fd, buf, sizeof(complex_t)*count);
	if (len < 0)
		return len;
	d->file_pos += count;
	len /= sizeof(complex_t);
	*time_ns = pos_to_ns(drv, d->file_pos);

	return len;
}

static int file_start_stream(acq_src_drv_t *drv)
{
	return 0;
}
static int file_stop_stream(acq_src_drv_t *drv)
{
	return 0;
}

acq_src_drv_intf_t acq_src_file_intf = {
	.open		= file_open,
	.close		= file_close,
	.set_freq	= file_set_freq,
	.set_bw		= file_set_bw,
	.set_gain	= file_set_gain,
	.start_stream	= file_start_stream,
	.stop_stream	= file_stop_stream,
	.read_stream	= file_read_stream,
};
