#include <math.h>
#include <stdio.h>
#include <assert.h>
#include <assert.h>
#include <unistd.h>
#include <phydet/log.h>
#include <phydet/mem.h>
#include <phydet/pool.h>
#include <phydet/chan_phy.h>
#include <phydet/dsp_util.h>
#include <phydet/circbuf.h>

//#define BIGDEBUG
#define BURST_TAIL 8

int chan_phy_read_buf(chan_phy_p chan, complex_t *buf, size_t *cnt)
{
/* FIXME */
	return 0;
}
int chan_phy_write_buf(chan_phy_p chan, int fd)
{
/* FIXME */
	return 0;
}

/* Must be called whenever a new window is starting */
static void chan_phy_reset(chan_phy_p chan)
{
	chan->state = CHAN_PHY_IDLE;
	if(chan->cb)
		cbuf_reset(chan->cb);
	chan_phy_adapt_reset(&chan->adapt);
	chan_demod_reset(chan->demod);
	peak_det_reset(chan->peak_det);
}

/* Convert offset in adapted buffer to absolute time in nanosec */
static inline double chan_phy_off_to_time_ns(chan_phy_p chan, u64 sample_offset)
{
	double offset_ns = sample_offset*1000000000 / chan->adapt.out_sr; 

	return chan->win_time + offset_ns;
}

/* Consume window buffer and update start timestamp */
static void chan_phy_advance_win(chan_phy_p chan, u32 count)
{
	cbuf_get(chan->cb, sizeof(complex_t)*count);
	chan->win_time = chan_phy_off_to_time_ns(chan, count);
}

/* Perform analysis on one burst of data */
static int chan_phy_analyse_burst(chan_phy_p chan, float rssi, size_t start, size_t end)
{
	size_t i;
	complex_t *samples;
	tlv_field_t *metas[8];
	size_t meta_count, sym_count;
	unsigned char *syms;
	int rc;
	double time_ns;

	samples = (complex_t*)cbuf_head(chan->cb);

	/* Feed required history */
	if (start < chan->demod->req_hist)
		start = 0;
	else
		start -= chan->demod->req_hist;

	//time_ns = chan_phy_off_to_time_ns(chan, start+chan->demod->req_hist);
	time_ns = chan_phy_off_to_time_ns(chan, start);
	LOG_DEBUG("%-16s/%2s | %2.4fms | burst (%lu)", chan->cdef->phy_def->name,chan->cdef->name, time_ns/1e6, end-start);

	meta_count = ARRAY_SIZE(metas);
	if((rc=chan_demod_execute(chan->demod, time_ns, &samples[start], end-start,
		&syms, &sym_count, metas, &meta_count)))
	{
		return rc;
	}

	/* Write metas */
	if (meta_count)
	{
		if((rc=chan_logger_write_metas(chan->logger, metas, meta_count)))
			return rc;
		for (i=0; i<meta_count; i++)
			free(metas[i]);
	}
	meta_count = ARRAY_SIZE(metas);
	if ((rc=chan_decode_execute(chan->decode, time_ns, syms, sym_count,
			metas, &meta_count)))
	{
		return rc;
	}
	/* Write metas */
	if (meta_count)
	{
		if((rc=chan_logger_write_metas(chan->logger, metas, meta_count)))
			return rc;
		for (i=0; i<meta_count; i++)
			free(metas[i]);
	}

	return SUCCESS;
}

static int chan_phy_log_peaks(chan_phy_p chan, peak_t *peaks, size_t peak_count)
{
	size_t i;
	int rc;
	tlv_field_t *f;
	tlv_peak_t *pp;

	if (peak_count == 0)
		return SUCCESS;

	if (!(f = tlv_field_alloc(sizeof(tlv_peak_t)*peak_count)))
		return NO_MEM;

	f->hdr.tag = 'KAEP';
	pp = (tlv_peak_t*)&f->data;

	for (i=0; i<peak_count; i++)
	{
		pp[i].time_ns = chan_phy_off_to_time_ns(chan, peaks[i].max_idx);
		pp[i].jump = peaks[i].max_val;
		pp[i].dBm_after = peaks[i].abs_val;
	}
	if((rc=chan_logger_write_metas(chan->logger, &f, 1)))
		return rc;
	return SUCCESS;
}

/* Window analysis: 
 * Find snr jumps above threshold.
 * Call modulation analysis for each positive jump.
 */
static size_t chan_phy_analyse_win(chan_phy_p chan)
{
	size_t count, i, peak_count;
	size_t start, end;
	complex_t *samples;
	float power;
#define MAX_PEAKS 256
	peak_t peaks[MAX_PEAKS], *peak;
	int pos;
	enum {
		WIN_STATE_LOW,
		WIN_STATE_HIGH,
	} state;

	/* Input data */
	count = cbuf_avail(chan->cb)/sizeof(complex_t);
	samples = (complex_t*)cbuf_head(chan->cb);
	
	/* Find all peaks in window */
	for(peak=peaks,peak_count=0, i=0; i<count && peak_count < MAX_PEAKS; i++)
	{
		power = mag_squared(samples[i]);
		if(peak_det_update(chan->peak_det, power, peak, NULL))
		{
			pos = i - chan->peak_det->n - peak->length + peak->max_idx;
			if (pos >= 0)
			{
				peak->max_idx = pos; // convert max_idx to window offset
				peak_count++;
				peak++;
			}
		}
	}
	chan_phy_log_peaks(chan, peaks, peak_count);

	/* Packetize window based on positive SNR peaks */
	float packet_rssi = 0;
	start = 0;
	state=WIN_STATE_LOW;
	for (peak=peaks, i=0; i<peak_count; i++, peak++)
	{
		LOG_DEBUG("%-16s/%2s | st=%d, %5d (%2.4fms)| peak(%3d) SNR: % -2.2f, dBm: % -2.3f",
			chan->cdef->phy_def->name,chan->cdef->name,
			state, peak->max_idx,
			chan_phy_off_to_time_ns(chan, peak->max_idx) / 1e6,
			peak->length, peak->max_val, peak->abs_val);
		if (state == WIN_STATE_HIGH)
		{
			end = peak->max_idx;

			/* FIXME: this BURST_TAIL */
			end += BURST_TAIL;
			if  (end > count)
				end = count;

			/* process previous window */
			chan_phy_analyse_burst(chan, packet_rssi, start, end);
		}
		if (peak->max_val > 0)
		{
			state = WIN_STATE_HIGH;
			start = peak->max_idx;
			packet_rssi = peak->abs_val;
		}
		else
		{
			state = WIN_STATE_LOW;
		}
	}
	/* consume up to last peak */
	if (peak_count == 0)
		return count;
	return peaks[peak_count-1].max_idx;
}

void chan_phy_handle_buf(chan_phy_p chan)
{
	int num_handled;
	int hist, consume;

	//LOG_DEBUG("handle_buf");

	/* Call upper layers */
	num_handled = chan_phy_analyse_win(chan);

	/* Keep some history */
	hist = chan->peak_det->n + chan->demod->req_hist;
	if (num_handled < hist)
		consume = 0;
	else
		consume = num_handled - hist;

	if (consume)
	{
		chan_phy_advance_win(chan, consume);
	}
}

/* Receive one chunk from lower layer. Adapt samples, reassemble window, then call upper layer */
int chan_phy_rcv(chan_phy_p chan, u64 chunk_id, pwr_det_evt_type_t event, sbuf_p sb)
{
	int win_end, win_ov;
	int status = 0;
	u32 buf_req;
	int n;

	/* Detect end of window / dropped packet */
	win_end = (chunk_id != chan->next_bid 
		|| event == DET_EVENT_MATCH_START );
	chan->next_bid = chunk_id+1;

	/* Required items for adaptation */
	buf_req = sizeof(complex_t)*((unsigned)ceil(sb->size * chan->adapt.out_sr / chan->adapt.in_sr)+1);
	win_ov = cbuf_free(chan->cb) < buf_req;

	if (win_end)
	{
		chan_phy_handle_buf(chan);
		/* Start new window */
		chan_phy_reset(chan);
		chan->win_id++;
		chan->win_time = sb->time_ns;
#ifdef BIGDEBUG
		LOG_DEBUG("Window %lld starts: %f - %f ms", chan->win_id, chan->win_time,
			chan_phy_off_to_time_ns(chan, chan->adapt.delay)/1e6);
#endif
	}
	else if (win_ov)
	{
		LOG_DEBUG("win_ov");
		/* Call upper layers */
		chan_phy_handle_buf(chan);
		/* Ensure room */
		int dif = (int)buf_req - cbuf_free(chan->cb);
		if (dif > 0)
		{
			/* FIXME: Dropping samples, this condition must be logged. */
			win_ov = cbuf_free(chan->cb) < buf_req;
			chan_phy_advance_win(chan, dif / sizeof(complex_t));
		}
		assert(cbuf_free(chan->cb) >= buf_req);
	}

	/* Adapt incoming data */
	n = chan_phy_adapt_samples(&chan->adapt, cbuf_tail(chan->cb), sb->samples, sb->size, cbuf_free(chan->cb)/sizeof(complex_t));
	cbuf_put(chan->cb, n*sizeof(complex_t));

	return status;
}

/* Handle timeout for current window */
int chan_phy_rcv_timeout(chan_phy_p chan)
{
	LOG_DEBUG("rcv_timeout");
	chan_phy_handle_buf(chan);
	chan_logger_flush(chan->logger);

	return SUCCESS;
}

/* \fn chan_phy_create
 * \ brief Create and initialize physical channel object.
 * 
 * \param cphyp		Return pointer
 * \param cdef		Channel definition
 * \param in_freq	Input samples center frequency
 * \param in_sr		Input sample rate
 * \param in_buf_size
 */
int chan_phy_create(chan_phy_p *cphyp, const phydef_chan_t *cdef, double in_freq, double in_sr, size_t in_buf_size)
{
	int rc = ERROR;
	chan_phy_p chan; 

	if(!(chan=MEM_ALLOC(sizeof(*chan))))
		return NO_MEM;
	memset(chan, 0, sizeof(*chan));
	*cphyp = chan;

	if((cbuf_create(&chan->cb, in_buf_size*sizeof(complex_t))))
		goto err;
	chan->cdef = cdef;
	chan->state = CHAN_PHY_IDLE;
	chan->win_id = 0;

	if((rc=chan_phy_adapt_init(&chan->adapt, cdef, in_freq, in_sr)))
		goto err;

	unsigned est_len = (unsigned)round(chan->adapt.out_sr*cdef->phy_def->peakdet.est_duration);
	if((rc=peak_det_create(&chan->peak_det, est_len, 2*est_len, cdef->phy_def->peakdet.min_peak)))
		goto err;

	if ((rc=chan_demod_create(&chan->demod, cdef)))
		return rc;

	if ((rc=chan_decode_create(&chan->decode, cdef)))
		return rc;

	if ((rc=chan_logger_create(&chan->logger, cdef)))
		return rc;

	return SUCCESS;
err:
	chan_phy_destroy(chan);
	return rc;
}

/* \fn chan_phy_destroy
 * \ brief Destroy chan_phy object
 */
void chan_phy_destroy(chan_phy_p chan)
{
	chan_phy_adapt_free(&chan->adapt);
	if(chan->cb)
		cbuf_destroy(chan->cb);
	if(chan->peak_det)
		peak_det_destroy(chan->peak_det);
	if(chan->demod)
		chan_demod_destroy(chan->demod);
	if (chan->decode)
		chan_decode_destroy(chan->decode);
	if (chan->logger)
		chan_logger_destroy(chan->logger);
	MEM_FREE(chan);
}
