#include <math.h>
#include <assert.h>
#include <liquid/liquid.h>
#include <phydet/log.h>
#include <phydet/mem.h>
#include <phydet/phydef.h>
#include <phydet/chan_phy.h>

/* chan_phy_adapt_init:
 * Configure filtering, rotation and resampling as required
 * for given channel definition and capture parameters.
*/
int chan_phy_adapt_init(chan_phy_adapt_p a, const phydef_chan_t *cdef, double in_freq, double in_sr)
{
	const int resamp_delay = 13;
	int n, i;
	float *h;
	complex float *hc;
	float fc, ft, fs, As, cfreq; 
	adapt_conf_t *aconf = &cdef->phy_def->adapt;
	float ratio, decf = in_sr / (aconf->bitrate*aconf->osr);
	int do_rotate = 0;
	int do_filter = 1;

	a->in_idx = 0;
	a->delay = 0;
	a->dec_idx = 0;
	a->decimation = (int)roundf(decf);

	/* Cleanup */
	if (a->resamp)
	{
		resamp_cccf_destroy(a->resamp);
		a->resamp = NULL;
	}
	if (a->filter)
	{
		firfilt_cccf_destroy(a->filter);
		a->filter = NULL;
	}
	if(a->rotator)
	{
		nco_crcf_destroy(a->rotator);
		a->rotator = NULL;
	}

	/* Configure resampler if required */
	if( (float)a->decimation != decf)
	{
		a->decimation = floor(decf/2.f);
		ratio = a->decimation / decf;
		a->resamp = resamp_cccf_create(ratio,
			resamp_delay,  	/* delay */
			0.499, 	/* cutoff */
			60.0, 	/* stop band attenuation */
			32	/* ntaps */);
		LOG_ERROR("Warning: invalid decimation ratio %.2f, using decimation %d, resamp %f",
			decf, a->decimation, ratio);
		a->max_resamp = (unsigned)ceil(ratio);
		a->delay += resamp_delay;
	}

	a->in_freq = in_freq;
	a->out_freq = cdef->center_freq;
	a->in_sr = in_sr;
	a->out_sr = in_sr / a->decimation;

	LOG_INFO("in_freq: %e, out_freq: %e, in_sr: %f, out_sr: %f, bitrate: %f, osr: %d, decimation %d", 
		in_freq,cdef->center_freq,
		in_sr, a->out_sr, aconf->bitrate, aconf->osr, a->decimation);

	/* Init filter parameters */
	cfreq = (cdef->center_freq - in_freq) / in_sr;
	if (cfreq != 0)
		do_rotate = 1;
	fc = (aconf->filter_bw/2) / in_sr;	// cutoff

	ft = aconf->transition_width / in_sr;				// transition width
	As = 65.f;					// attenuation

	/* rotation */
	fs = 2*M_PI*cfreq;

	if (do_filter)
	{
		n = estimate_req_filter_len(ft, As);
		h = (float*) MEM_ALLOC(sizeof(float)*n);
		hc = (complex float*) MEM_ALLOC_ALIGN(sizeof(complex float)*n);
		a->delay += n / a->decimation;
		LOG_INFO("filter len: %d, valid idx: %d", n, a->delay);

		/* Low-pass real taps */
		liquid_firdes_kaiser(n, fc, As, 0.0f, h);

		/* band-pass complex taps */
		for (i=0;i<n; i++)
			hc[i] = (complex float)h[i];

		/* rotate taps */
		if (do_rotate)
		{
			for (i=0;i<n; i++)
				hc[i] *= cexpf(i*fs*I);
		}

		a->filter = firfilt_cccf_create(hc, n);
		firfilt_cccf_set_scale(a->filter, 1.f/n);
		MEM_FREE(h);
		MEM_FREE(hc);
	}

	/* Frequency rotator */
	if (do_rotate)
	{
		a->rotator = nco_crcf_create(LIQUID_NCO);
		nco_crcf_set_phase(a->rotator, 0.0f);
		nco_crcf_set_frequency(a->rotator, -a->decimation*fs);
	}

	return 0;
}

int chan_phy_adapt_samples(chan_phy_adapt_p a, complex_t *out_samples, complex_t *in_samples, size_t input_count, size_t output_max)
{
	int i,n;
	unsigned cnt;
	complex_t samp, phase;
	int has_filter = a->filter != NULL;
	int has_rotator = a->rotator != NULL;

	for(i=n=0;i<input_count;i++)
	{
		if(has_filter)
			firfilt_cccf_push(a->filter, in_samples[i]);
		if (++a->dec_idx == a->decimation)
		{
			a->dec_idx = 0;
			if(has_filter)
			{
				firfilt_cccf_execute(a->filter, &samp);
			}
			else
				samp = in_samples[i];
			if(has_rotator)
			{
				nco_crcf_step(a->rotator);
				nco_crcf_cexpf(a->rotator, &phase);
				samp *= phase;
			}
			if (a->resamp)
			{
				/* FIXME: assuming buffer is big enough */
				resamp_cccf_execute(a->resamp, samp, &out_samples[n], &cnt);
			}
			else
			{
				out_samples[n] = samp;
				cnt = 1;
			}
			assert(cnt+n <= output_max);
			if(a->in_idx >= a->delay)
			{
				n += cnt;
			}
			a->in_idx += cnt;
		}
	}
	return n;
}

void chan_phy_adapt_reset(chan_phy_adapt_p a)
{
	if(a->filter)
		firfilt_cccf_reset(a->filter);
	if(a->resamp)
		resamp_cccf_reset(a->resamp);
	/* No need to reset initial phase in nco */
	a->dec_idx = 0;
	a->in_idx = 0;
}

void chan_phy_adapt_free(chan_phy_adapt_p a)
{
	if(a->filter)
		firfilt_cccf_destroy(a->filter);
	if(a->rotator)
		nco_crcf_destroy(a->rotator);
	if(a->resamp)
		resamp_cccf_destroy(a->resamp);
}

