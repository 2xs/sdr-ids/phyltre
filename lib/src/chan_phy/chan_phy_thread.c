#include <stdio.h>
#include <phydet/mem.h>
#include <phydet/pool.h>
#include <phydet/chan_phy.h>
#include <phydet/log.h>

#define THREAD_BUF_COUNT 0x400000

int chan_phy_chunk_create(chan_phy_chunk_p *pp, u64 id, pwr_det_evt_type_t evt, sbuf_p sb)
{
	chan_phy_chunk_p p;

	if (!(p=MEM_ALLOC(sizeof(*p))))
		return -1;
	p->id = id;
	p->event = evt;
	p->sb = sb;
	sbuf_addref(p->sb);
	*pp = p;
	return 0;
}

void chan_phy_chunk_destroy(chan_phy_chunk_p p)
{
	sbuf_release(p->sb);
	MEM_FREE(p);
}

static void *chan_phy_thr_main(void *arg)
{
	chan_phy_thr_p thr = arg;
	chan_phy_chunk_p p;

	LOG_INFO("thr_main starts in %p: thread = %p", thr, thr->thread);

	while (!thread_stopped(thr->thread))
	{
		/* FIXME?: Assuming chunk interval is always less than
		 * one second. */
		if (!queue_timedget(thr->in_queue, (void**)&p, 1000))
		{
			chan_phy_rcv(thr->chan, p->id, p->event, p->sb);
			chan_phy_chunk_destroy(p);
		}
		else
		{
			/* Notify the chan of timeout */
			chan_phy_rcv_timeout(thr->chan);
		}
	}
	/* Process end of queue */ 
	while (!queue_timedget(thr->in_queue, (void**)&p, 10))
	{
		chan_phy_rcv(thr->chan, p->id, p->event, p->sb);
		chan_phy_chunk_destroy(p);
	}
	chan_phy_rcv_timeout(thr->chan);

	return 0;
}

int chan_phy_thr_create(
	chan_phy_thr_p *thrp, const phydef_chan_t *cdef,
	double in_freq, double in_sr, size_t queue_size)
{
	chan_phy_thr_p thr;

	if(!(thr = MEM_ALLOC(sizeof(*thr))))
		return -1;

	if(queue_create(&thr->in_queue, queue_size))
	{
		free(thr);
		return -1;
	}
	if(chan_phy_create(&thr->chan, cdef, in_freq, in_sr, THREAD_BUF_COUNT))
	{
		queue_destroy(thr->in_queue);
		free(thr);
		return -1;
	}
	if(thread_create(&thr->thread, chan_phy_thr_main, thr))
		return -1;

	*thrp = thr;
	return 0;
}

int chan_phy_thr_send(chan_phy_thr_p chan, chan_phy_chunk_p pc)
{
	if(queue_tryput(chan->in_queue, pc))
		return -1;

	return 0;
}

int chan_phy_thr_send_blocking(chan_phy_thr_p chan, chan_phy_chunk_p pc)
{
	return queue_put(chan->in_queue, pc);
}

int chan_phy_thr_join(chan_phy_thr_p thr, void**retval)
{
	thread_stop(thr->thread);
	return thread_join(thr->thread, retval);
}

void chan_phy_thr_destroy(chan_phy_thr_p thr)
{
	chan_phy_chunk_p chunk;

	chan_phy_destroy(thr->chan);
	while((queue_size(thr->in_queue)) > 0)
	{
		queue_get(thr->in_queue, (void**)&chunk);
		chan_phy_chunk_destroy(chunk);
	}
	queue_destroy(thr->in_queue);
	MEM_FREE(thr);
}
