#include <phydet/mem.h>
#include <phydet/bitset.h>

int bitset_create(bitset_p *bp, size_t capacity)
{
	size_t lsize = (capacity+63) / 64;
	bitset_p b;

	if(!(b = MEM_ALLOC(offsetof(bitset_t,v[lsize]))))
		return -1;

	b->long_size = lsize;
	b->size = capacity;

	*bp = b;

	return 0;
}

void bitset_destroy(bitset_p b)
{
	MEM_FREE(b);
}

