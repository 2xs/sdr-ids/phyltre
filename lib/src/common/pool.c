#include <string.h>
#include <phydet/mem.h>
#include <phydet/pool.h>
#include <phydet/log.h>

typedef struct sbuf_pool_s {
	MUTEX_T mutex;
	size_t elcount;
	size_t elsize;
	struct sbuf_s	*freelist;
	struct sbuf_s	*ebuf;
	complex_t	*sbuf;		/* sample buffer */
	complex_t	*fbuf;	 	/* fft buffer */
	float		*fmbuf; 	/* fft mag buffer */
} sbuf_pool_t, *sbuf_pool_p;

static sbuf_pool_p sbuf_pool_g = NULL;

void sbuf_pool_dump_state(void)
{
	size_t cnt = 0;
	sbuf_p sb;

	for(sb=sbuf_pool_g->freelist;sb;sb=sb->next)
		cnt++;

	LOG_INFO("Sbuf pool: free=%lu/%lu, elsize=%lu\n", cnt,
		sbuf_pool_g->elcount, sbuf_pool_g->elsize);
}

void sbuf_pool_destroy(void)
{
	int i;

	for(i=0;i<sbuf_pool_g->elcount;i++)
	{
		MUTEX_DESTROY(&sbuf_pool_g->ebuf[i].mutex);
	}
	MUTEX_DESTROY(&sbuf_pool_g->mutex);
	MEM_FREE(sbuf_pool_g->sbuf);
	MEM_FREE(sbuf_pool_g->fbuf);
	MEM_FREE(sbuf_pool_g->fmbuf);
	MEM_FREE(sbuf_pool_g->ebuf);
	MEM_FREE(sbuf_pool_g);
}

int sbuf_pool_init(size_t count, size_t size)
{
	size_t i;
	sbuf_pool_p pool;

	LOG_INFO("Allocating ~%lu Mbytes", size*count*(sizeof(complex_t)*2+sizeof(float))/(1024*1024));

	if (sbuf_pool_g)
	{
		sbuf_pool_destroy();
	}
	pool = MEM_ALLOC(sizeof(*pool));
	pool->elcount = count;
	pool->elsize = size;

	MUTEX_INIT(&pool->mutex);
	pool->freelist = pool->ebuf = MEM_ALLOC(sizeof(sbuf_t)*count);
	pool->sbuf = MEM_ALLOC_ALIGN(sizeof(complex_t)*size*count);
	pool->fbuf = MEM_ALLOC_ALIGN(sizeof(complex_t)*size*count);
	pool->fmbuf = MEM_ALLOC_ALIGN(sizeof(float)*size*count);

	for(i=0;i<count;i++)
	{
		MUTEX_INIT(&pool->freelist[i].mutex);
		pool->ebuf[i].next = &pool->ebuf[i+1];
		pool->ebuf[i].size = size;
		pool->ebuf[i].samples = &pool->sbuf[size*i];
		pool->ebuf[i].fft = &pool->fbuf[size*i];
		pool->ebuf[i].fft_mag = &pool->fmbuf[size*i];
	}
	pool->ebuf[count-1].next = NULL;
	sbuf_pool_g = pool;

	return 0;
}

/* Allocate an element from the pool, initialize with given data block and attributes */
sbuf_p sbuf_alloc(void)
{
	sbuf_p sb = NULL;

	MUTEX_LOCK(&sbuf_pool_g->mutex);
	if (sbuf_pool_g->freelist != NULL)
	{
		sb = sbuf_pool_g->freelist;
		sbuf_pool_g->freelist = sb->next;
		sb->refcnt = 1;
	}
	MUTEX_UNLOCK(&sbuf_pool_g->mutex);

	return sb;
}
void sbuf_fill(sbuf_p sb, complex_t *samples, complex_t *fft, float *fft_mag, u64 time_ns, u64 flags)
{
	sb->flags = flags;
	sb->time_ns = time_ns;
	if(samples) 	memcpy(sb->samples, samples, sbuf_pool_g->elsize*sizeof(complex_t));
	if(fft) 	memcpy(sb->fft, fft, sbuf_pool_g->elsize*sizeof(complex_t));
	if(fft_mag)	memcpy(sb->fft_mag, fft_mag, sbuf_pool_g->elsize*sizeof(float));
}
void sbuf_read(sbuf_p sb, complex_t *samples, complex_t *fft, float *fft_mag, u64 *time_ns, u64 *flags)
{
	if(samples)
		memcpy(samples, sb->samples, sbuf_pool_g->elsize*sizeof(complex_t));
	if(fft)
		memcpy(fft, sb->fft, sbuf_pool_g->elsize*sizeof(complex_t));
	if(fft_mag)
		memcpy(fft_mag, sb->fft_mag, sbuf_pool_g->elsize*sizeof(complex_t));
	*time_ns = sb->time_ns;
	*flags = sb->flags;
}
void sbuf_addref(sbuf_p sb)
{
	MUTEX_LOCK(&sb->mutex);
	sb->refcnt++;
	MUTEX_UNLOCK(&sb->mutex);
}
static void sbuf_free(sbuf_p sb)
{
	MUTEX_LOCK(&sbuf_pool_g->mutex);
	sb->next = sbuf_pool_g->freelist;
	sbuf_pool_g->freelist = sb;
	MUTEX_UNLOCK(&sbuf_pool_g->mutex);
}
void sbuf_release(sbuf_p sb)
{
	unsigned ref;
	MUTEX_LOCK(&sb->mutex);
	ref = --sb->refcnt;
	MUTEX_UNLOCK(&sb->mutex);
	if (ref == 0)
	{
		sbuf_free(sb);
	}
}
