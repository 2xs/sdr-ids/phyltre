#include <stdio.h>
#include <phydet/mem.h>
#include <phydet/log.h>
#include <phydet/logger.h>
#include <phydet/tlv_file.h>

#define CHAN_LOG_BUF_SIZE 0x40000

int chan_logger_create(chan_logger_p *lp, const phydef_chan_t *cdef)
{
	int rc;
	chan_logger_p l;
	char *fpath;

	if (!(l = MEM_ALLOC(sizeof(*l))))
		return NO_MEM;

	l->cdef = cdef;

	if(-1==asprintf(&fpath, "%s/%s-%s.tlv",
		cdef->phy_def->logging.path,
		cdef->phy_def->name, cdef->name))
		return NO_MEM;

	rc = tlv_file_logger_create(&l->fl, fpath, CHAN_LOG_BUF_SIZE);
	free(fpath);
	if(rc)
		return rc;
	*lp = l;

	return SUCCESS;
}

void chan_logger_destroy(chan_logger_p l)
{
	tlv_file_logger_destroy(l->fl);
	MEM_FREE(l);
}
