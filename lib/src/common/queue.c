#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <assert.h>
#include <phydet/types.h>
#include <phydet/thread.h>

/* Queues */
typedef struct queue_s {
	MUTEX_T mutex;
	COND_T cond_empty;
	size_t capacity;
	size_t size;
	size_t read;
	size_t write;
	void*v[0];
} queue_t, *queue_p; 

int queue_create(queue_p *q, size_t capacity)
{
	queue_p r;

	if(!(r = malloc(offsetof(struct queue_s,v[capacity]))))
		return -1;
	
	r->capacity = capacity;
	r->size = 0;
	r->read = 0;
	r->write = 0;
	MUTEX_INIT(&r->mutex);
	COND_INIT(&r->cond_empty);
	*q = r;

	return 0;
}

void queue_destroy(queue_p q)
{
	MUTEX_DESTROY(&q->mutex);
	COND_DESTROY(&q->cond_empty);
	free(q);
}

int queue_get(queue_p q, void **elp)
{
	MUTEX_LOCK(&q->mutex);
	while (q->size == 0)
	{
		COND_WAIT(&q->cond_empty, &q->mutex);
	}
	*elp = q->v[q->read];
	q->read = (q->read+1) % q->capacity;
	q->size--;
	MUTEX_UNLOCK(&q->mutex);
	COND_BROADCAST(&q->cond_empty);

	return 0;
}

int queue_timedget(queue_p q, void **elp, int timeout_ms)
{
	struct timeval now, timeout;
	struct timeval tinc = { 0, timeout_ms * 1000.f};
	MUTEX_LOCK(&q->mutex);
	gettimeofday(&now, NULL);
	timeradd(&now, &tinc, &timeout);
	while(q->size == 0)
	{
		if (COND_TIMEDWAIT(&q->cond_empty, &q->mutex, timeout_ms))
		{
			gettimeofday(&now, NULL);
			if (timercmp(&now, &timeout, >))
			{
				/* TODO: honor timeout_ms */
				MUTEX_UNLOCK(&q->mutex);
				return -1;
			}
		}
	}
	*elp = q->v[q->read];
	q->read = (q->read+1) % q->capacity;
	q->size--;
	MUTEX_UNLOCK(&q->mutex);
	COND_BROADCAST(&q->cond_empty);

	return 0;
}

int queue_put(queue_p q, void *el)
{
	MUTEX_LOCK(&q->mutex);
	while(q->size == q->capacity)
	{
		COND_WAIT(&q->cond_empty, &q->mutex);
	}
	q->v[q->write] = el;
	q->write = (q->write+1) % q->capacity;
	q->size++;
		
	MUTEX_UNLOCK(&q->mutex);
	COND_BROADCAST(&q->cond_empty);

	return 0;
}

int queue_tryput(queue_p q, void *el)
{
	MUTEX_LOCK(&q->mutex);
	if(q->size == q->capacity)
	{
		MUTEX_UNLOCK(&q->mutex);
		return -1;
	}
	q->v[q->write] = el;
	q->write = (q->write+1) % q->capacity;
	q->size++;
		
	MUTEX_UNLOCK(&q->mutex);
	COND_BROADCAST(&q->cond_empty);

	return 0;
}

size_t queue_size(queue_p q)
{
	size_t s;

	MUTEX_LOCK(&q->mutex);
	s = q->size;
	MUTEX_UNLOCK(&q->mutex);

	return s;
}
