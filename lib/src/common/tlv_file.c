#include <stdio.h>
#include <assert.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <phydet/mem.h>
#include <phydet/det.h>
#include <phydet/tlv_file.h>
#include <phydet/log.h>

int tlv_file_logger_create(tlv_file_logger_p *lp, char *path, size_t buf_size)
{
	tlv_file_logger_p l;

	if (!(l = MEM_ALLOC(sizeof(*l))))
		return NO_MEM;

	if(!(l->buf = MEM_ALLOC(buf_size)))
	{
		MEM_FREE(l);
		return NO_MEM;
	}
	l->buf_size = buf_size;
	l->buf_pos = 0;

	snprintf(l->path, sizeof(l->path), "%s", path);
	if((l->fd = open(l->path, O_WRONLY|O_CREAT|O_TRUNC, 0600))==-1)
	{
		LOG_ERROR("Unable to open log file %s for writing", l->path);
		free(l->buf);
		free(l);
		return CONF_ERROR;
	}
	*lp = l;

	return SUCCESS;
}

int tlv_file_logger_write(tlv_file_logger_p l, void *data, size_t size)
{
	int rc;

	assert(size < l->buf_size);

	/* Flush buffer if required */
	if(size + l->buf_pos > l->buf_size)
	{
		if((rc=tlv_file_logger_flush(l)))
			return rc;
	}
	/* Write in buffer */
	memcpy(&l->buf[l->buf_pos], data, size);
	l->buf_pos += size;
	
	return SUCCESS;
}

int tlv_file_logger_write_metas(tlv_file_logger_p l,
	tlv_field_t **metas, size_t meta_count)
{
	int rc;
	size_t i;

	for (i=0; i<meta_count; i++)
		if((rc=tlv_file_logger_write(l, metas[i], sizeof(metas[i]->hdr) + metas[i]->hdr.size)))
			return rc;

	return SUCCESS;
}

int tlv_file_logger_flush(tlv_file_logger_p l)
{
	if (l->buf_pos > 0)
	{
		if(write(l->fd, l->buf, l->buf_pos) == -1)
			return ERROR;
		l->buf_pos = 0;
	}
	return SUCCESS;
}

void tlv_file_logger_destroy(tlv_file_logger_p l)
{
	LOG_INFO("Closing log file %s\n", l->path);
	tlv_file_logger_flush(l);
	close(l->fd);
	MEM_FREE(l->buf);
	MEM_FREE(l);
}

