#include <phydet/mem.h>
#include <phydet/thread.h>

static void* thread_ep(void *arg)
{
	thread_p t = arg;

	return t->func(t->data);
}

int thread_create(thread_p *tp, thread_func_t func, void *data)
{
	thread_p t;

	if(!(t = MEM_ALLOC(sizeof(*t))))
		return -1;
	*tp = t;

	t->stopped = 0;
	t->data = data;
	t->func = func;
	THREAD_CREATE(&t->thread, thread_ep, t);

	return 0;
}

void thread_stop(thread_p t)
{
	t->stopped = 1;
}

int thread_stopped(thread_p t)
{
	return t->stopped;
}

void thread_destroy(thread_p t)
{
	MEM_FREE(t);
}

int thread_join(thread_p t, void **retval)
{
	int rc = THREAD_JOIN(t->thread, retval);
	return rc;
}

int thread_set_affinity(thread_p t, int n)
{
#if 0
	cpu_set_t cpuset;

	CPU_ZERO(&cpuset);
	CPU_SET(n, &cpuset);
	pthread_setaffinity_np(t->thread, sizeof(cpuset), &cpuset);
	/* TODO: error checking */
#endif
	return 0;
}
