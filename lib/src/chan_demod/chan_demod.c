#include <string.h>
#include <phydet/mem.h>
#include <phydet/log.h>
#include <phydet/chan_demod.h>

extern chan_demod_intf_t chan_demod_fm_intf;

static struct chan_demod_lut_s {
	char *name;
	chan_demod_intf_t *intf;
} demod_lut[] = {
	{"fm", &chan_demod_fm_intf},
	{NULL},
};

int chan_demod_create(chan_demod_p *cp, const phydef_chan_t *cdef)
{
	int rc;
	struct chan_demod_lut_s *l;
	chan_demod_p c;
	const gen_conf_t *dconf = &cdef->phy_def->demod;

	if(!(c = MEM_ALLOC(sizeof(*c))))
		return -1;
	c->intf = NULL;

	c->req_hist = 0;
	c->cdef = cdef;

	for(l=demod_lut; l->name; l++)
	{
		if (!strcmp(l->name, dconf->type))
		{
			c->intf = l->intf;
			break;
		}
	}
	if (c->intf == NULL)
	{
		LOG_ERROR("Unknown demod %s", dconf->type);
		MEM_FREE(c);
		return -1;
	}
	if((rc = c->intf->create(c, dconf)))
	{
		MEM_FREE(c);
		return rc;
	}
	*cp = c;
	return 0;
}
