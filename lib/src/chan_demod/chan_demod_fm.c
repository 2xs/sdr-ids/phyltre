#include <math.h>
#include <assert.h>
#include <liquid/liquid.h>
#include <phydet/log.h>
#include <phydet/mem.h>
#include <phydet/chan_demod.h>
#include <phydet/clock_recov.h>
#include <phydet/dsp_util.h>

typedef struct demod_fm_s {
	complex_t prev_samp;
	/* parameters */
	float mod_index;
	float mod_index_tol;
	float sync_gain;
	float freq_drift_limit;
	long est_len;
	long est_skip;
	unsigned max_syms;
	int log_syms;
	int log_freq;
	int log_samp;
	int correct_cfo;
	/* rolling averages buffers */
	float *f_buf;		/* frequency buf */
	float *mod_buf;		/* mod index estimation buf */
	float f_sum;
	float mod_sum;
	float *dem_buf;
	unsigned char *sym_buf;
	unsigned buf_idx;
	unsigned in_count;	/* total input samples since reset*/
	/* auto params*/
	double samp_rate;
	unsigned max_input;
	unsigned osr;
	float mod_index_min, mod_index_max;
	float demod_gain;
	clock_recov_p sync;	/* symbol synchronizer */
} demod_fm_t, *demod_fm_p;

static int demod_fm_create(chan_demod_p c, const gen_conf_t *dconf)
{
	int i;
	demod_fm_p p;
	/* Retrieve input sample rate this way. FIXME: Not so convenient */
	double sr = c->cdef->phy_def->adapt.bitrate * c->cdef->phy_def->adapt.osr;

	LOG_DEBUG("demod_fm_create");

	if(!(p = MEM_ALLOC(sizeof(*p))))
		return -1;
	memset(p, 0, sizeof(*p));
	p->osr = 0;
	p->mod_index = 0;
	p->mod_index_tol = 0;
	p->sync_gain = 0;
	p->freq_drift_limit = 0;
	p->correct_cfo = 0;
	p->samp_rate = sr;

	c->req_hist += 1;	// fm demodulation

	p->osr = c->cdef->phy_def->adapt.osr;
	if (p->osr < 2)
	{
		LOG_ERROR("Demod FM requires osr >= 2");
		return CONF_ERROR;
	}
	p->log_freq = 0;
	p->log_syms = 0;

	for (i=0;i<dconf->num_args;i++)
	{
		if(!strcmp(dconf->args[i].name, "modIndex"))
			p->mod_index = strtof(dconf->args[i].value, NULL);
		else if(!strcmp(dconf->args[i].name, "modIndexRel"))
			p->mod_index_tol = strtof(dconf->args[i].value, NULL);
		else if(!strcmp(dconf->args[i].name, "estDuration"))
			p->est_len = (unsigned)sr*strtof(dconf->args[i].value, NULL);
		else if(!strcmp(dconf->args[i].name, "syncGain"))
			p->sync_gain = strtof(dconf->args[i].value, NULL);
		else if(!strcmp(dconf->args[i].name, "syncLimit"))
			p->freq_drift_limit = strtof(dconf->args[i].value, NULL);
		else if(!strcmp(dconf->args[i].name, "estSkip"))
			p->est_skip = (unsigned)sr*strtof(dconf->args[i].value, NULL);
		else if(!strcmp(dconf->args[i].name, "maxSyms"))
			p->max_syms = strtol(dconf->args[i].value, NULL, 10);
		else if(!strcmp(dconf->args[i].name, "logFreq"))
			p->log_freq = !strcasecmp(dconf->args[i].value, "true");
		else if(!strcmp(dconf->args[i].name, "logSyms"))
			p->log_syms = !strcasecmp(dconf->args[i].value, "true");
		else if(!strcmp(dconf->args[i].name, "logSamples"))
			p->log_samp = !strcasecmp(dconf->args[i].value, "true");
		else if(!strcmp(dconf->args[i].name, "correctCFO"))
			p->correct_cfo = !strcasecmp(dconf->args[i].value, "true");
		else{
			LOG_ERROR("Unknown parameter %s", dconf->args[i].name);
		}
	}
	if (p->mod_index==0 || p->mod_index_tol == 0 || p->est_len == 0
	 || p->sync_gain ==0 || p->freq_drift_limit == 0 || p->est_skip==0)
	{
		LOG_ERROR("Missing parameters");
		MEM_FREE(p);
		return -1;
	}
	if (p->max_syms < (p->est_skip+p->est_len)/p->osr)
	{
		LOG_ERROR("Max syms must be large enought for estimation + skip");
		MEM_FREE(p);
		return -1;
	}
	p->max_input = p->osr*p->max_syms;
	p->dem_buf = (float*)MEM_ALLOC(sizeof(float)*p->max_input);
	/* Clock recovery yields a varying sample rate. 
	 * Allocate max_syms * osr symbols to make sure we have 
	 * enough room for the extra output */
	p->sym_buf = (unsigned char*)MEM_ALLOC(sizeof(char)*p->max_input);
	p->mod_index_min = p->mod_index - p->mod_index_tol;
	p->mod_index_max = p->mod_index + p->mod_index_tol;
	// Assumes  BT=0.5
	p->demod_gain = (float)p->osr/(2*M_PI*p->mod_index/2);
	p->f_buf = MEM_ALLOC(p->est_len*sizeof(float));
	p->mod_buf = MEM_ALLOC(p->est_len*sizeof(float));
	c->data = p;
	
	if(clock_recov_create(&p->sync, (float)p->osr, .25*powf(p->sync_gain,2), p->sync_gain, p->freq_drift_limit))
		return 1;
	c->req_hist += p->sync->req_hist;

	return 0;
}

void demod_fm_destroy(chan_demod_p c)
{
	demod_fm_p p = c->data;

	clock_recov_destroy(p->sync);
	MEM_FREE(p->mod_buf);
	MEM_FREE(p->f_buf);
	MEM_FREE(p->dem_buf);
	MEM_FREE(p->sym_buf);
	MEM_FREE(p);
}

/* Reset metadata buffers */
static void demod_fm_reset(chan_demod_p c)
{
	demod_fm_p p = c->data;
	p->buf_idx = 0;
	p->in_count = 0;
	p->prev_samp = 0;
	p->in_count = 0;
	p->f_sum = 0.f;
	p->mod_sum = 0.f;
}

/* Calc instantaneous frequency (F) */
static inline float demod_fm_demod_samp(demod_fm_p p, complex_t s)
{
	float v;

	v = p->demod_gain * freq_diff(s, p->prev_samp);
	p->prev_samp = s;

	return v;
}

/* Update estimation buffers */
static void demod_fm_update_est(demod_fm_p p, float v)
{
	float v2;

	/* Update F & F squared rolling averages */
	v2 = v*v;
	if(p->in_count>=p->est_len)
	{
		p->f_sum -= p->f_buf[p->buf_idx];
		p->mod_sum -= p->mod_buf[p->buf_idx];
	}
	p->f_buf[p->buf_idx] = v;
	p->mod_buf[p->buf_idx] = v2;
	p->f_sum += v;		/* frequency error est */
	p->mod_sum += v2;	/* modulation index est */
	if(++p->buf_idx==p->est_len)
		p->buf_idx = 0;
	p->in_count++;
}

typedef struct PACKED {
	double time_ns;
	float freq_offset;
	float mod_index;
} demod_fm_meta_t;

static int demod_fm_execute(chan_demod_p c, double time_ns, const complex_t *in, size_t in_len,
		u8 **syms, size_t *syms_count,
		tlv_field_t **metas, size_t *metas_count)
{
	demod_fm_p p = c->data;
	unsigned i, cnt;
	size_t pre_size;
	float freq_offset, mod_index;
	float d;
	size_t max_metas;
	demod_fm_meta_t feta;


	max_metas = *metas_count;
	*metas_count = 0;

	pre_size = p->est_len+p->est_skip+1; // skip first glitched sample

	/* Make sure we dont overflow demod buffers */
	if (in_len > p->max_input)
		in_len = p->max_input;

	LOG_DEBUG("demod_fn in_len=%ld, pre_size=%ld", in_len, pre_size);

	if (in_len < pre_size)
	{
		LOG_DEBUG("Short read %ld < pre_size %ld", in_len, pre_size);
		return -1; /* short read */
	}

	/* Demodulate start of packet to estimate frequency offset*/
	for (i=0; i<pre_size; i++)
	{
		d = demod_fm_demod_samp(p, in[i]);
		demod_fm_update_est(p, d);
		p->dem_buf[i] = d;
	}
	freq_offset = p->f_sum / p->est_len;

	mod_index = sqrtf((p->mod_sum / p->est_len));
	mod_index = mod_index * 2 * p->mod_index / M_PI_2;

	/* Write metadatas */
	feta.time_ns = time_ns;
	feta.freq_offset = freq_offset;
	feta.mod_index = mod_index;
	assert (*metas_count+1 <= max_metas);
	metas[(*metas_count)++] = tlv_field_alloc_init('MDMF', &feta, sizeof(feta));

	LOG_DEBUG("packet len: %lu->%lu, foff: %f, mod_index: %f", in_len, in_len/p->osr,freq_offset, mod_index);

	/* Check if modulation matches expected */
	if (mod_index < p->mod_index_min || mod_index > p->mod_index_max)
	{
		*syms_count = 0;
		*syms = 0;
		LOG_DEBUG("skip invalid mod_index %2.2f", mod_index);
		return SUCCESS;
	}

	/* Demodulate rest of packet */
	for (; i<in_len; i++)
	{
		p->dem_buf[i] = demod_fm_demod_samp(p, in[i]);
	}

	/* Log instantaneous frequency */
	if (p->log_freq)
	{
		assert (*metas_count+1 <= max_metas);
		metas[(*metas_count)++] = tlv_field_alloc_init('QRFI', p->dem_buf, sizeof(p->dem_buf[0])*in_len);
	}
	if (p->log_samp)
	{
		assert (*metas_count+1 <= max_metas);
		metas[(*metas_count)++] = tlv_field_alloc_init('ASFI', in, sizeof(in[0])*in_len);
	}

	/* Correct freq offset */
	if (p->correct_cfo)
	{
		for (i=0; i<in_len; i++)
		{
			p->dem_buf[i] -= freq_offset;
		}
	}

#if 0
/* FIXME XXX TODO */
	float tmpbuf[0x1000];

	/* Synchronize */
	cnt = clock_recov_exec_(p->sync, 0.5f, p->sym_buf, p->dem_buf, in_len, tmpbuf);
	if (cnt > p->max_input)
	{
		LOG_ERROR("! cnt %u > max %u\n", cnt, p->max_input);
	}
	if (p->log_freq)
	{
		assert (*metas_count+1 <= max_metas);
		metas[(*metas_count)++] = tlv_field_alloc_init('QRFI', tmpbuf, sizeof(float)*cnt);
	}
#else
	/* Synchronize */
	cnt = clock_recov_exec_(p->sync, 0.5f, p->sym_buf, p->dem_buf, in_len, NULL);
	if (cnt > p->max_input)
	{
		LOG_ERROR("! cnt %u > max %u\n", cnt, p->max_input);
	}
#endif

	assert(cnt <= p->max_input);
	*syms_count = cnt;
	*syms = p->sym_buf;

	/* Log symbols */
	if (p->log_syms)
	{
		assert (*metas_count+1 <= max_metas);
		metas[(*metas_count)++] = tlv_field_alloc_init('SMYS', p->sym_buf, cnt*sizeof(p->sym_buf[0]));
#if 0
		fprintf(stderr, "Symbols: ");
		for(i=0;i<cnt;i++)
		{
			fprintf(stderr, "%c", p->sym_buf[i]+0x30);
		}
		fprintf(stderr, "\n");
#endif
	}
	
	return 0;
}

const chan_demod_intf_t chan_demod_fm_intf = {
	.create	 = demod_fm_create,
	.destroy = demod_fm_destroy,
	.reset	 = demod_fm_reset,
	.execute = demod_fm_execute,
};
