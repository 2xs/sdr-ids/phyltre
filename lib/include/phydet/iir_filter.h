#ifndef DEF_PHYDET_IIR
#define DEF_PHYDET_IIR
#include <phydet/mem.h>

typedef struct {
	float alpha;
	float comp;
	float value;
} iir_filter_t, *iir_filter_p;

static inline void iir_filter_create(iir_filter_p *fp, unsigned n)
{
	iir_filter_p f = MEM_ALLOC(sizeof(*f));

	f->alpha = 2.f/(1+n);
	f->comp = 1 - f->alpha;
	f->value = 0;

	*fp = f;
}

static inline void iir_filter_destroy(iir_filter_p f, unsigned n)
{
	MEM_FREE(f);
}

static inline float iir_filter_update(iir_filter_p f, float value)
{
	f->value = f->alpha*value + f->comp*f->value;
	return f->value;
}

#endif
