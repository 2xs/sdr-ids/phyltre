#ifndef DEF_CHAN_PHY_
#define DEF_CHAN_PHY_
#include <liquid/liquid.h>
#include <phydet/types.h>
#include <phydet/thread.h>
#include <phydet/pool.h>
#include <phydet/mem.h>
#include <phydet/queue.h>
#include <phydet/phydef.h>
#include <phydet/det.h>
#include <phydet/peak_det.h>
#include <phydet/circbuf.h>
#include <phydet/chan_demod.h>
#include <phydet/chan_decode.h>
#include <phydet/logger.h>

/* Physical layer chunk */
typedef struct chan_phy_chunk_s {
	u64 			id;
	pwr_det_evt_type_t	event;
	sbuf_p 			sb;
} chan_phy_chunk_t, *chan_phy_chunk_p;

int chan_phy_chunk_create(chan_phy_chunk_p *pp, u64 id, pwr_det_evt_type_t evt, sbuf_p sb);
void chan_phy_chunk_destroy(chan_phy_chunk_p p);

/* Channel adaptation */
typedef struct chan_phy_adapt_s {
	double in_freq, out_freq;
	double in_sr, out_sr;

	int decimation;
	int delay;
	int in_idx;
	size_t	dec_idx;
	firfilt_cccf filter;
	nco_crcf rotator;
	resamp_cccf resamp;
	unsigned max_resamp;
} chan_phy_adapt_t, *chan_phy_adapt_p;

/* \fn chan_phy_adapt_init
 * \brief Initialize channel adapter
 *
 * \param a		Channel adapter to initialize
 * \param cdef		Target channel definition
 * \param in_freq	Input center frequency
 * \param in_sr		Input samplerate
 */
int chan_phy_adapt_init(chan_phy_adapt_p a, const phydef_chan_t *cdef, double in_freq, double in_sr);

/* \fn chan_phy_adapt_samples
 * \brief	Apply filtering, rotation and resampling as required.
 * 
 * \param a		Chan adapter
 * \param out_samples	Output samples buffer (must be at least input_count/decimation)
 * \param in_samples	Input samples
 * \param input_count	Input count
 * \param output_count	Max output count
 */
int chan_phy_adapt_samples(chan_phy_adapt_p a, complex_t *out_samples, complex_t *in_samples, size_t input_count, size_t output_max);

/* \fn chan_phy_adapt_reset
 * \brief Reset internal state of channel adapter
 *
 * \param a	Adapter to reset
 */
void chan_phy_adapt_reset(chan_phy_adapt_p a);

/* \fn chan_phy_adapt_free
 * \brief Delete allocated memory
 *
 * \param a	Adapter to destroy
 */
void chan_phy_adapt_free(chan_phy_adapt_p a);

/* Channel physical layer */
typedef enum {
	CHAN_PHY_IDLE,
	CHAN_PHY_ACTIVE,
} chan_phy_state_t;

typedef struct chan_phy_s {
	/* Definition */
	const phydef_chan_t	*cdef;

	/* Input buffer */
	cbuf_p			cb;

	/* Adaptation */
	chan_phy_adapt_t 	adapt;
	u32			adapt_buf_sz;

	/* Detectors */
	peak_det_p		peak_det;

	/* Demodulation */
	chan_demod_p 		demod;

	/* Decoding */
	chan_decode_p 		decode;

	/* Logging */
	chan_logger_p		logger;

	/* State */
	u64			win_id;		// input window counter
	double			win_time;	// buffer 
	chan_phy_state_t	state;
	u64			next_bid;
} chan_phy_t, *chan_phy_p;

/* Channel phy thread */
typedef struct chan_phy_thr_s {
	thread_p thread;
	chan_phy_p chan;

	/* Input queue */
	queue_p		in_queue;
} *chan_phy_thr_p;

int chan_phy_create(chan_phy_p *cphyp, const phydef_chan_t *cdef, double in_freq, double in_sr, size_t buf_size);
void chan_phy_destroy(chan_phy_p chan);
int chan_phy_rcv(chan_phy_p chan, u64 chunk_id, pwr_det_evt_type_t evt, sbuf_p sb);
int chan_phy_rcv_timeout(chan_phy_p chan);
int chan_phy_write_buf(chan_phy_p chan, int fd);
int chan_phy_read_buf(chan_phy_p chan, complex_t *buf, size_t *cnt);

/* \fn chan_phy_thr_create
 * \brief Create a chan physical layer thread
 *
 * \param *thrp		Result pointer
 * \param cdef 		Channel definition
 * \param in_freq	Input frequency
 * \param in_sr		Input samplerate
 * \param queue_size	Receive queue block count
 */
int chan_phy_thr_create(
	chan_phy_thr_p *cthrp, const phydef_chan_t *cdef,
	double in_freq, double in_sr, size_t queue_size);

/* \fn chan_phy_thr_send
 * \brief Called by lower layers to enqueue a chunk of samples
 * 	  to upper layers.
 *
 * \param *thrp		Result pointer
 * \param cdef 		Channel definition
 * \param in_freq	Input frequency
 * \param in_sr		Input samplerate
 * \param queue_size	Receive queue block count
 */
int chan_phy_thr_send(chan_phy_thr_p chan, chan_phy_chunk_p pc);

/* \fn chan_phy_thr_send_blocking
 * \brief Blocking version, meant for file input. 
 *  Called by lower layers to enqueue a chunk of samples to upper layers.
 *
 * \param *thrp		Result pointer
 * \param cdef 		Channel definition
 * \param in_freq	Input frequency
 * \param in_sr		Input samplerate
 * \param queue_size	Receive queue block count
 */
int chan_phy_thr_send_blocking(chan_phy_thr_p chan, chan_phy_chunk_p pc);

/* \fn chan_phy_thr_stop
 * \brief	Stop the thread asynchronously. Can be called
 *		before chan_phy_thr_join to let threads finish in parallel.
 * \param thr	Thread to stop
 */
inline static void chan_phy_thr_stop(chan_phy_thr_p thr)
{
	thread_stop(thr->thread);
}


/* \fn	chan_phy_thr_join
 * \brief Stop & join chan thread
 * 
 * \param retval	Return value of thread function
*/
int chan_phy_thr_join(chan_phy_thr_p thr, void**retval);

/* \fn chan_phy_thr_destroy
 * \brief Destroy memory object
 * 
 * \param thr	Thread to destroy (must be joined first)
 */
void chan_phy_thr_destroy(chan_phy_thr_p thr);
#endif
