#ifndef DEF_CFIL_H_
#define DEF_CFIL_H_
#include <phydet/types.h>

typedef struct PACKED {
	double freq,bw,nf;
	u32 fft_bin;
} cfil_hdr_t;

typedef struct PACKED {
	u32 chan_index, event;
	float value, value2;
} cfil_evt_t;

typedef struct {
	int fd;
	/* buf sizes */
	u32 chan_max;
	u32 evt_size;
	/* cfil header */
	cfil_hdr_t hdr;
	/* channel list */
	u32 chan_count;
	char **chans;
	/* event buffer */
	u32 event_count;
	cfil_evt_t *events;
} cfil_t, *cfil_p;

/* \fn cfil_create
 * \brief Create a cfil reader
 * 
 * \param cp	Output cfil reader pointer
 * \param fd	valid file descriptor
 * \return	0 on success
 */
int cfil_create(cfil_p *cp, int fd);

/* \fn cfil_read_chunk
 * \brief Read a sample chunk with stim and related events
 * 
 * \param c		Cfil reader
 * \param samples	Output sample buffer
 * \param count		Sample buffer size, must match cfile buf size
 * \param time_ns	Output chunk time in nanoseconds
 * \return		0 on success
 */
int cfil_read_chunk(cfil_p c, complex_t *samples, unsigned count, u64 *time_ns);
/* \fn cfil_find_chan
 * \brief Get channel index in cfil header by its name
 * 
 * \param c	Cfil reader
 * \param chan	channel name
 * \return	channel index or negative on error
 */
int cfil_find_chan(cfil_p c, const char *chan);

/* \fn cfil_destroy
 * \brief free allocated memory and close file descriptor.
 * 
 * \param c	cfil reader to destroy
 */
void cfil_destroy(cfil_p c);

#endif
