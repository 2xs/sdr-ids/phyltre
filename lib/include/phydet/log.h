#ifndef DEF_LOG_H
#define DEF_LOG_H
#include <stdio.h>
#include <string.h>

#define __FILENAME__ (strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 : __FILE__)

#define LOG(fmt,...)	\
	fprintf(stderr, "%s:%d: " fmt "\n", __FILENAME__,__LINE__,##__VA_ARGS__)

#define LOG_ERROR LOG
#define LOG_INFO LOG
//#define LOG_DEBUG(...)	
#define LOG_DEBUG LOG

#endif
