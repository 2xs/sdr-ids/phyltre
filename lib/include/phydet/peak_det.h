#ifndef DEF_PEAK_DET_H_
#define DEF_PEAK_DET_H_
#include <phydet/rolavg.h>

typedef struct peak_s {
	unsigned length;
	unsigned max_idx;
	float max_val;	// max snr jump in dB
	float abs_val;	// power spectrum after jump in dBm
} peak_t;

typedef enum {
	PEAK_DET_WAIT_PEAK,
	PEAK_DET_IN_PEAK,
} peak_det_state_t;

/* Find peaks in mag input */
typedef struct peak_det_s {
	/* param */
	unsigned n;
	unsigned max_length;
	float min_snr;
	float min_snr_p;
	/* snr est */
	float *buf;
	unsigned idx;
	float sum_a;
	float sum_b;
	/* state */
	peak_det_state_t state;
	peak_t cur_peak;
} peak_det_t, *peak_det_p;

/* \fn peak_det_create
 * \brief Create a snr peak detector.
 * 	SNR is estimated by 10*log10(a/b)
 * 	with a and b averages of two following sequences of size n.
 *
 * \param dp		Result pointer
 * \param n		Averaging duration in sample count
 * \param min_snr	Min output peak in dB
 */
int peak_det_create(peak_det_p *dp, unsigned n, unsigned max_length, float min_snr);

void peak_det_destroy(peak_det_p d);

/* \fn peak_det_reset
 * \brief Reset internal state of peak detector
 *
 * \param d	Peak detector to reset
 */
void peak_det_reset(peak_det_p d);

/* \fn peak_det_update
 * \brief	Execute detector on one sample
 *
 * \param d		Detector context
 * \param mag2		Input power (squared magnitude)
 * \param cur_snr 	Optional output current snr jump estimation 
			(delayed by d->n)
 * \param peak		Output peak if any. Exact sample offset of max peak:
 *			CUR_SAMPLE_OFF - d->n - peak.length + peak->max_idx
 *
 * \return	1 if peak detected, 0 else
 */
int peak_det_update(peak_det_p d, float mag2, peak_t *peak, float *cur_snr);

#endif
