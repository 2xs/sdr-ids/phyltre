#ifndef DEF_BITSET_H_
#define DEF_BITSET_H_
#include <phydet/types.h>
#include <phydet/mem.h>

typedef struct bitset_s {
	u32 size;
	u32 long_size;
	u64 v[0];
} bitset_t, *bitset_p;

int bitset_create(bitset_p *bp, size_t capacity);
void bitset_destroy(bitset_p b);

static inline int bitset_set(bitset_p b, size_t num)
{
	if (num < b->size)
	{
		b->v[num>>6] = 1<<(num & 63);
		return 0;
	}
	return -1;
}

static inline int bitset_get(bitset_p b, size_t num)
{
	if (num < b->size)
	{
		return (b->v[num>>6] & 1<<(num&63)) != 0;
	}
	return -1;
}

static inline int bitset_zero(bitset_p b)
{
	int i;
	for(i=0;i<b->long_size;i++)
		b->v[i] = 0;
	return 0;
}

#endif
