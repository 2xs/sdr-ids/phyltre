#ifndef DEF_PHYDET_ROLAVG
#define DEF_PHYDET_ROLAVG

typedef struct {
	unsigned n;
	float *hist;
	float value;
	float sum;
	int idx;
} rolavg_t, *rolavg_p;

void rolavg_create(rolavg_p *r, unsigned n);
void rolavg_destroy(rolavg_p r);
float rolavg_update(rolavg_p r, float v);

#define USE_AVG_ROL
//#define USE_AVG_IIR
//#define USE_AVG_NONE

#ifdef USE_AVG_ROL
typedef rolavg_p AVG_T;
#define AVG_CREATE	rolavg_create
#define AVG_UPDATE	rolavg_update
#define AVG_DESTROY	rolavg_destroy
#elif defined(USE_AVG_IIR)
typedef iir_filter_p AVG_T;
#define AVG_CREATE	iir_filter_create
#define AVG_UPDATE	iir_filter_update
#elif defined(USE_AVG_NONE)
typedef void* AVG_T;
#define AVG_CREATE(a,b)
#define AVG_UPDATE(a,b) ((a)->value=b)
#else
#error "No averaging method defined"
#endif

#endif
