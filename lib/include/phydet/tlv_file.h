#ifndef PHYDET_TLV_H
#define PHYDET_TLV_H
#include <string.h>
#include <stdio.h>
#include <sys/unistd.h>
#include <sys/uio.h>
#include <phydet/types.h>
#include <phydet/cfil.h>
#include <phydet/mem.h>

typedef struct tlv_file_logger_s {
	char path[1024];
	int fd;
	unsigned char *buf;
	size_t buf_size;
	size_t buf_pos;
} tlv_file_logger_t, *tlv_file_logger_p;

int tlv_file_logger_create(tlv_file_logger_p *lp, char *path, size_t buf_size);
int tlv_file_logger_write(tlv_file_logger_p l, void *data, size_t size);
int tlv_file_logger_write_metas(tlv_file_logger_p l, tlv_field_t **metas, size_t meta_count);
int tlv_file_logger_flush(tlv_file_logger_p l);
void tlv_file_logger_destroy(tlv_file_logger_p l);

static inline int tlv_read_TL(int fd, u32 *type, u32 *length)
{
	int rc;
	u32 tl[2];

	if ((rc=read(fd, tl, sizeof(tl))) != sizeof(tl))
		return -1;
	*type = tl[0];
	*length = tl[1];

	return 0;
}

static inline int tlv_write_tag(int fd, u32 tag, u32 size, void *data)
{
	tlv_hdr_t tlv = {tag, size};
	struct iovec iov[2] = {
		{&tlv, sizeof(tlv)},
		{data, size}
	};

	if (writev(fd, (const struct iovec *)iov, 2) == -1)
	{
		perror("writev");
		return -1;
	}
	return 0;
}

static inline int tlv_write_field(int fd, tlv_field_t *f)
{
	if (write(fd, f, sizeof(f->hdr) + f->hdr.size) == -1)
	{
		perror("write");
		return -1;
	}
	return 0;
}

static inline tlv_field_t *tlv_field_alloc(u32 size)
{
	tlv_field_t *f;

	if ((f=(tlv_field_t *)MEM_ALLOC(sizeof(tlv_field_t)+size)))
		f->hdr.size = size;

	return f;
}

static inline tlv_field_t *tlv_field_alloc_init(u32 tag, const void *data, u32 size)
{
	tlv_field_t *f;

	if((f = tlv_field_alloc(size)))
	{
		f->hdr.tag = tag;
		memcpy(f->data, data, size);
	}

	return f;
}

static inline int tlv_write_CFIL(int fd, double freq, double bw, double noise_floor, unsigned fft_bin)
{
	cfil_hdr_t data = {freq, bw, noise_floor, fft_bin};
	return tlv_write_tag(fd, 'LIFC', sizeof(data), &data);
}

static inline int tlv_write_EFIL(int fd, double freq, double bw, double noise_floor, unsigned fft_bin)
{
	cfil_hdr_t data = {freq, bw, noise_floor, fft_bin};
	return tlv_write_tag(fd, 'LIFC', sizeof(data), &data);
}

static inline int tlv_write_CDEF(int fd, char *chan_name)
{
	return tlv_write_tag(fd, 'FEDC', strlen(chan_name), chan_name);
}

static inline int tlv_write_GTIM(int fd, u64 gmt_time)
{
	return tlv_write_tag(fd, 'MITG', sizeof(gmt_time), &gmt_time);
}

static inline int tlv_write_STIM(int fd, u64 samp_time)
{
	return tlv_write_tag(fd, 'MITS', sizeof(samp_time), &samp_time);
}

static inline int tlv_write_EVNT(int fd, u32 chan_index, u32 event, float value)
{
	cfil_evt_t data = {chan_index, event, value};
	return tlv_write_tag(fd, 'TNVE', sizeof(data), &data);
}

static inline int tlv_write_DATA(int fd, u32 count, complex_t *samples)
{
	return tlv_write_tag(fd, 'ATAD', count * sizeof(complex_t), samples);
}

static inline int tlv_write_DFFT(int fd, u32 count, complex_t *fft)
{
	return tlv_write_tag(fd, 'TFFD', count * sizeof(complex_t), fft);
}

static inline int tlv_write_NOIS(int fd, double noise_floor)
{
	return tlv_write_tag(fd, 'SION', sizeof(double), &noise_floor);
}

#endif
