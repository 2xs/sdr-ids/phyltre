#ifndef DEF_PHYDET_TYPES_H
#define DEF_PHYDET_TYPES_H
#include <complex.h>

#ifndef MIN
#define MIN(a,b) ((a)<(b)?(a):(b))
#endif

#ifndef MAX
#define MAX(a,b) ((a)>(b)?(a):(b))
#endif

#define PACKED __attribute__((packed))

#ifndef ARRAY_SIZE
#define ARRAY_SIZE(a) (sizeof(a)/sizeof(*(a)))
#endif

#ifndef offsetof
#define offsetof(s, m)	((size_t)&(((s*)0)->m))
#endif

typedef unsigned long long u64;
typedef unsigned int u32;
typedef unsigned short u16;
typedef unsigned char u8;

typedef u8 bool_t;
#define FALSE 0
#define TRUE 1

typedef struct urange_s {
	u32 first, last;
} urange_t;

typedef complex float complex_t;

typedef struct PACKED {
	u32 tag;
	u32 size;
} tlv_hdr_t;

typedef struct PACKED {
	double time_ns;
	float jump;
	float dBm_after;
} tlv_peak_t;

typedef struct PACKED {
	tlv_hdr_t hdr;
	char data[0];
} tlv_field_t;

typedef enum {
	SUCCESS = 0,
	ERROR = -1,
	NO_MEM = -2,
	CONF_ERROR = -10,
	SHORT_READ = -20,
	DECODE_FAILED = -21,
} status_t; 

#endif
