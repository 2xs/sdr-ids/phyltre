#ifndef DEF_DSP_UTIL_H
#define DEF_DSP_UTIL_H
#include <volk/volk.h>
#include <math.h>
#include <phydet/types.h>
#include <phydet/log.h>

#define USE_POPCNT

#ifdef USE_POPCNT
#define hamming32	__builtin_popcount
#define hamming64	__builtin_popcountl
#else

/* https://stackoverflow.com/a/109025 */
static inline unsigned hamming32(u32 i)
{
	i = i - ((i >> 1) & 0x55555555);
	i = (i & 0x33333333) + ((i >> 2) & 0x33333333);
	return (((i + (i >> 4)) & 0x0F0F0F0F) * 0x01010101) >> 24;
	return __builtin_popcount (i);
}

static inline unsigned hamming64(u64 i)
{
	i = i - ((i >> 1) & 0x5555555555555555);
	i = (i & 0x3333333333333333) + ((i >> 2) & 0x3333333333333333);
	i = ((i + (i >> 4)) & 0x0F0F0F0F0F0F0F0F);
	return (unsigned)((i*(0x0101010101010101))>>56);
}
#endif

/* This should go fast with  */
static inline u32 pack32bits(const u8 *bits)
{
	u32 res = 0;
	int i;

	for (i=0; i<32; i++)
		res |= bits[i]<<(31-i);

	return res;
}

float dsp_util_calc_time_power(const complex_t *in, u32 count);
float dsp_util_calc_freq_power(const float *in, u32 count);

/* convert mag squared to dbm */
static inline float mag2dbm(float val)
{
	return 10 * log10f(val);
}

static inline float dbm2mag(float val)
{
	return powf(10.f, val/10.f);
}

static inline float mag_squared(complex_t s)
{
	return __real__ s*__real__ s+__imag__ s*__imag__ s;
}

static inline float freq_diff(complex_t s, complex_t prev)
{
	return cargf(s * conjf(prev));
}

/* \fn find_bin_indexes
 * \brief Convert frequency/band to int range in fft output
 * 
 * \param fft_freq		Center frequency of full band
 * \param fft_bw		Bandwidth of full band
 * \param fft_size		Size of fft
 * \param freq_lo		Low frequency of channel
 * \param freq_hi		High frequency of channel 
 * \param range			Output range 
 */
int find_bin_indexes(double fft_freq, double fft_bw, unsigned fft_size, double freq_lo, double freq_hi, urange_t *range);
#endif
