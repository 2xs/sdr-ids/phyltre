#ifndef DEF_CHAN_DEMOD_H_
#define DEF_CHAN_DEMOD_H_
#include <phydet/types.h>
#include <phydet/tlv_file.h>
#include <phydet/phydef.h>

struct chan_demod_s;

typedef struct chan_demod_intf_s {
	int (*create)(struct chan_demod_s *dp, const gen_conf_t *dconf);
	void (*destroy)(struct chan_demod_s *d);
	void (*reset)(struct chan_demod_s *d);
	int (*execute)(struct chan_demod_s *c, double time_ns, const complex_t *in, size_t in_len,
		u8 **syms, size_t *syms_count,
		tlv_field_t **metas, size_t *metas_count);
} chan_demod_intf_t;

typedef struct chan_demod_s {
	chan_demod_intf_t *intf;
	void *data;
	int req_hist;
	const phydef_chan_t *cdef;
} chan_demod_t, *chan_demod_p;

/* \fn chan_demod_create
 * \brief Create a generic channel demodulator
 *
 * \param cp		Output channel demodulator pointer
 * \param cdef		Channel definition with valid modulation configuration
 *
 * \return 0 on success
 */
int chan_demod_create(chan_demod_p *cp, const phydef_chan_t *cdef);

/* \fn chan_demod_execute
 * \brief Demodulate a chunk of data and compute metadatas.
 *
 * \param c		Channel demodulator
 * \param time_ns	Input time
 * \param in		Complex input samples
 * \param in_len	Number of input samples
 * \param syms		Output symbol buffer
 * \param syms_count	In/Out Number of samples in symbol buffer
 * \param metas		Output metadata list
 * \param metas_count	In/Out Number of metadata in the list
 * 
 * \return 0 when success else status
 */
static inline int chan_demod_execute(chan_demod_p c, double time_ns, complex_t *in, size_t in_len,
		u8 **syms, size_t *syms_count,
		tlv_field_t **metas, size_t *metas_count)
{
	return c->intf->execute(c, time_ns, in, in_len, syms, syms_count, metas, metas_count);
}

/* \fn chan_demod_reset
 * \brief Reset state of demodulator
 */
static inline int chan_demod_reset(chan_demod_p c)
{
	c->intf->reset(c);
	return 0;
}

/* \fn chan_demod_destroy
 * \brief Free allocated resources
 */
static inline void chan_demod_destroy(chan_demod_p c)
{
	c->intf->destroy(c);
	MEM_FREE(c);
}
#endif
