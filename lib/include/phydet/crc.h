#ifndef DEF_CRC_H_
#define DEF_CRC_H_
#include <phydet/types.h>

void crc_tbl_init(u32 *tbl, u32 poly);
u32 crc_exec(const u32 *tbl, u32 init, const u8 *data, u32 size);

#endif
