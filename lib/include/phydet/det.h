#ifndef DEF_DET_H
#define DEF_DET_H
#include <phydet/types.h>
#include <phydet/pool.h>
#include <phydet/phydef.h>
#include <phydet/config.h>
#include <phydet/tlv_file.h>

#define USE_OUTPUT_SAMPLES

/* Channel events */
typedef enum {
	DET_EVENT_NONE = 0,
	DET_EVENT_MATCH_START,
	DET_EVENT_MATCH_END,
	DET_EVENT_MATCH_CONT,
} pwr_det_evt_type_t;

typedef struct {
	int chan_index;
	pwr_det_evt_type_t event;
	float value;
	float value2;
} pwr_det_out_evt_t;

typedef struct pwr_det_ctx_s pwr_det_ctx_t, *pwr_det_ctx_p;

/* \fn 		pwr_det_create
 * \brief 	Create a power detector context
 *
 * \param fft_bin_count	Number of bins for the fft
 * \param center_freq	Input frequency in Hertz
 * \param bw		Input bandwidth in Hertz
 * \param noise_floor	Noise floor estimation in dbm
 * \return	0 on success, negative on error
 */
int pwr_det_create(pwr_det_ctx_p *ctxp, pwr_det_parm_t *parm, double center_freq, double bw, double noise_floor_dbm);

/* \fn 		pwr_det_create
 * \brief 	Destroy the context
 *
 * \param ctx	A context to destroy
 */
void pwr_det_destroy(pwr_det_ctx_p ctx);

/* \fn pwr_det_out_block
 * \brief Read output block (if selected), in sb. Fills evts with associate 
 * events. Return number of active channels.
 * 
 * \param ctx
 * \param *sb		output sb pointer if any, must be released by caller
 * \param evts		Buffer for output events
 * \parma evt_max	Size of output events buffer
 *
 * \return		Number of events readen
 */
int pwr_det_out_block(pwr_det_ctx_p ctx, sbuf_p *sb, pwr_det_out_evt_t *evts, size_t evt_max);

/* \fn pwr_det_execute
 * \brief Execute the detector on one fft window
 *
 * \param ctx		Context
 * \param samples	Sample buffer
 * \param len		Sample count, must match the detector's fft size
 * \param samp_time	time in nanoseconds
 *
 * \return		Number of Start events, negative on error
 */
int pwr_det_execute(pwr_det_ctx_p ctx, complex_t *samples, unsigned len, u64 samp_time);

/* \fn pwr_det_tech_chan_add
 * \brief Add a channel detector in the given context
 * 
 * \param ctx		Context
 * \param tech_idx	tech index
 * \param name 		Channel name
 * \param center_freq 	Channel frequency
 *
 * \return		0 when success, negative on error
 */
int pwr_det_tech_chan_add(pwr_det_ctx_p ctx, unsigned tech_idx, char *name, double center_freq);

/* \fn pwr_det_tech_add
 * \brief Add a technology in the given context. Each technology gets its own psd average.
 * 
 * \param ctx		Context
 * \param name 		Channel name
 * \param min_duration	Minimal duration of a packet in seconds
 * \param bin_spec	Power bins specification
 *
 * \return		tech index on success, negative on error
 */
int pwr_det_tech_add(pwr_det_ctx_p ctx, char *name, band_sel_conf_t *band_sel);

/* \fn pwr_det_set_noise_floor
 * \brief Set noise floor in the context and update thresholds accordingly
 *
 * \param noise_floor_dbm
 */
int pwr_det_set_noise_floor(pwr_det_ctx_p ctx, float noise_floor_dbm);

#endif
