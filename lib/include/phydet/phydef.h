#ifndef DEF_PHYDEF_H
#define DEF_PHYDEF_H
#include <phydet/types.h>

typedef struct gen_arg_s {
	char *name;
	char *value;
} gen_arg_t;

/* Freq-domain energy detector configuration */
typedef struct phydef_bin_s {
	double min_freq, max_freq;
	double min_snr;
	double max_snr;
} phydef_bin_t;

#define PHYDEF_MAX_BINS 32
typedef struct phydef_bin_spec_t {
	unsigned bin_count;
	phydef_bin_t bins[PHYDEF_MAX_BINS];
} phydef_bin_spec_t;

typedef struct {
	phydef_bin_spec_t bin_spec;
	double min_duration;
} band_sel_conf_t;

/* Channel adaptation configuration */
typedef struct {
	double	filter_bw;
	double	transition_width;
	double	bitrate;
	int	osr;
} adapt_conf_t ;

/* Peak detector configuration */
typedef struct {
	double est_duration;
	double min_peak;
} peak_conf_t ;

/* Demod configuration */
#define MAX_GEN_ARGS 128
typedef struct {
	char *type;
	gen_arg_t args[MAX_GEN_ARGS];
	size_t num_args;
} gen_conf_t ;

typedef struct {
	char *path;
} logging_conf_t ;

#define PHYDEF_FLAG_MSK 1
/* Physical channel set */
struct phydef_chan_s;
typedef struct phydef_s {
	struct phydef_s *next;
	char *name;
	struct phydef_chan_s *channels;

	adapt_conf_t	adapt;
	band_sel_conf_t bandsel;
	peak_conf_t 	peakdet;
	gen_conf_t	demod;
	gen_conf_t	decode;
	logging_conf_t	logging;
} phydef_t;

typedef struct phydef_chan_s {
	struct phydef_chan_s *next;
	char *name;
	phydef_t *phy_def;
	double center_freq;
} phydef_chan_t, *phydef_chan_p;

void phydef_print(phydef_t *defs);
void phydef_destroy(phydef_t *defs);
/* \fn phydef_find_chan
 * \brief Find physical channel definition by name
 * 
 * \param phy_defs	List of phy definitions to search in
 * \param chan_str	Channel to find formatted as "phy_name/chan_name"
 * 
 * \return physical channel def or NULL on error
 */
phydef_chan_t *phydef_find_chan(phydef_t *phy_defs, char *chan_str);

#endif
