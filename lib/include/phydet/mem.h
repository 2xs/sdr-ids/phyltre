#ifndef DEF_PHYDET_MEM_H
#define DEF_PHYDET_MEM_H
#include <stdlib.h>
#include <volk/volk.h>

#define MEM_ALLOC_ALIGN(size) aligned_alloc(32, size)
#define MEM_ALLOC(size) malloc(size)
#define MEM_REALLOC(buf, size) realloc(buf, size)
#define MEM_FREE(mem) free(mem)

#endif
