#ifndef DEF_THREAD_H_
#define DEF_THREAD_H_
#include <pthread.h>

#define timespecadd(a,b,res) {						\
	(res)->tv_sec = (a)->tv_sec + (b)->tv_sec;			\
	(res)->tv_nsec = ((a)->tv_nsec + (b)->tv_sec)%1000000000;	\
	(res)->tv_sec += ((a)->tv_nsec + (b)->tv_sec)/1000000000;	}

/* Pthread */
typedef pthread_t THREAD_T;
typedef pthread_mutex_t MUTEX_T;
#define MUTEX_INIT(mutex) pthread_mutex_init(mutex, NULL)
#define MUTEX_DESTROY(mutex) pthread_mutex_destroy(mutex)
#define MUTEX_LOCK(mutex) pthread_mutex_lock(mutex)
#define MUTEX_UNLOCK(mutex) pthread_mutex_unlock(mutex)
static inline int pthread_mutex_lock_timeout(pthread_mutex_t *m, unsigned long long timeout_ns) {	\
	struct timespec t,to,ts = {timeout_ns/1000000000,timeout_ns%1000000000};
	clock_gettime(CLOCK_REALTIME, &t);
	timespecadd(&t,&ts,&to);
	return pthread_mutex_timedlock(m, &to);
}
#define MUTEX_TIMEDLOCK(mutex,timeout_ns) pthread_mutex_lock_timeout(mutex,timeout_ns)
typedef pthread_cond_t COND_T;
#define COND_INIT(cond) pthread_cond_init(cond, NULL)
#define COND_DESTROY(cond) pthread_cond_destroy(cond)
#define COND_WAIT(cond,mutex) pthread_cond_wait(cond,mutex)
#define COND_TIMEDWAIT(cond,mutex,timeout_ns) pthread_cond_wait_timeout(cond,mutex,timeout_ns)
static inline int pthread_cond_wait_timeout(pthread_cond_t *c, pthread_mutex_t *m, unsigned long long timeout_ns) {	\
	struct timespec t,to,ts = {timeout_ns/1000000000,timeout_ns%1000000000};
	clock_gettime(CLOCK_REALTIME, &t);
	timespecadd(&t,&ts,&to);
	return pthread_cond_timedwait(c, m, &to);
}
#define COND_BROADCAST(cond)  pthread_cond_broadcast(cond)
#define THREAD_CREATE(thr, func, data) pthread_create(thr, NULL, func, data)
#define THREAD_JOIN(thr, rvp) pthread_join(thr, rvp)

typedef void* (*thread_func_t)(void*);

typedef struct thread_s {
	THREAD_T thread;
	void *data;
	volatile int stopped;
	thread_func_t func;
} *thread_p;

/* \fn	thread_create
 * \brief	Allocate and start a thread
 *
 * \param tp	Output thread pointer
 * \param func	main function
 * \param data	main arguments
 * \return	0 on success
 */
int thread_create(thread_p *tp, thread_func_t func, void *data);
int thread_set_affinity(thread_p t, int n);

/* \fn thread_stop
 * \brief	Set stop bit
 * */
void thread_stop(thread_p t);

/* \fn thread_stopped
 * \brief	Get stop bit
 * */
int thread_stopped(thread_p t);

/* \fn	thread_join
 * \brief	Join the thread
 *
 * \param retval  Output return value of thread function 
 * \return	  0 on success
 */
int thread_join(thread_p t, void **retval);

/* \fn thread_destroy
 * \brief	Free memory allocated with thread
 */
void thread_destroy(thread_p t);

#endif
