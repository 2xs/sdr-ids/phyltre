#ifndef DEF_ACQ_SRC_H_
#define DEF_ACQ_SRC_H_
#include <phydet/types.h>
#include <phydet/fft.h>
#include <phydet/config.h>

/* Source drivers */
typedef enum {
	SRC_FILE,
	SRC_INVALID
} acq_src_type_t;

struct acq_src_drv_s;

/* Callback */
typedef int (*acq_stream_rx_cb_t)(struct acq_src_drv_s *drv, complex_t *samples, u32 count, u64 timestamp, void *cb_data);

/* Driver interface */
typedef int (*acq_src_open_t)(struct acq_src_drv_s *drv); 
typedef int (*acq_src_close_t)(struct acq_src_drv_s *drv); 
typedef int (*acq_src_set_freq_t)(struct acq_src_drv_s *drv, double freq); 
typedef int (*acq_src_set_bw_t)(struct acq_src_drv_s *drv, double bw);
typedef int (*acq_src_set_gain_t)(struct acq_src_drv_s *drv, double gain);
typedef int (*acq_src_start_stream_t)(struct acq_src_drv_s *drv);
typedef int (*acq_src_stop_stream_t)(struct acq_src_drv_s *drv);
typedef int (*acq_src_read_stream_t)(struct acq_src_drv_s *drv, complex_t *buf, size_t count, u64 *time_ns);

typedef struct {
	acq_src_open_t 		open;
	acq_src_close_t 	close;
	acq_src_set_freq_t 	set_freq;
	acq_src_set_bw_t 	set_bw;
	acq_src_set_gain_t 	set_gain;
	acq_src_start_stream_t 	start_stream;
	acq_src_stop_stream_t 	stop_stream;
	acq_src_read_stream_t 	read_stream;
} acq_src_drv_intf_t;

typedef struct acq_src_drv_s {
	acq_src_drv_intf_t *intf;
	char *args;
	void *data;
	double cur_freq;
	double cur_bw;
	double cur_gain;
	int block_dc;
	int streaming;
	complex_t dc_val;
} acq_src_drv_t;

/* \fn acq_src_create
 * \brief 	Create an acquisition source with given configuration
 * \param parm	Source configuration structure: 
 *	Typedef struct {
 *		char *name;		// Chosen source name
 *		char *driver;		// Driver name
 *		char *driver_args;	// Driver arguments
 * 		// Exemples: 	driver="file", driver_args="/tmp/in.iq"
 * 		// 		driver="soapy", driver_args="driver=hackrf"
 *		double start_freq;	// Frequency
 *		double start_bw;	// Bandwidth
 *		double start_gain;	// Gain in dB
 *		nf_type_t nf_type;  	// NF_AUTO or NF_FIXED
 *		double noise_floor;	// when NF_FIXED: Noise floor estimation in dBm
 *		int block_dc;		// enable DC blocking
 *	} acq_src_parm_t; 
 * \return Acquisition source pointer, or NULL
 */
acq_src_drv_t *acq_src_create(acq_src_parm_t *parm);
void acq_src_print(acq_src_parm_t *src);
acq_src_parm_t *acq_src_parm_parse(char *path);

void acq_src_destroy(struct acq_src_drv_s *drv);
int acq_src_set_freq(struct acq_src_drv_s *drv, double freq);
int acq_src_get_freq(struct acq_src_drv_s *drv, double *freq);
int acq_src_set_bw(struct acq_src_drv_s *drv, double bw);
int acq_src_get_bw(struct acq_src_drv_s *drv, double *bw);
int acq_src_set_gain(struct acq_src_drv_s *drv, double gain);
int acq_src_get_gain(struct acq_src_drv_s *drv, double *gain);
int acq_src_stream_rx(struct acq_src_drv_s *drv, acq_stream_rx_cb_t cb, int *cb_ret, void *cb_data);
int acq_src_start_stream(struct acq_src_drv_s *drv);
int acq_src_stop_stream(struct acq_src_drv_s *drv);
int acq_src_read_stream(struct acq_src_drv_s *drv, complex_t *buf, size_t count, u64 *time_ns);

int acq_src_determine_noise_floor(acq_src_drv_t *drv,  unsigned fft_size, win_type_t win_type, unsigned fft_count, float *floor);

#endif
