#ifndef DEF_PHYDEF_FFT_H
#define DEF_PHYDEF_FFT_H
#include <fftw3.h>
#include <phydet/types.h>

typedef enum win_type_e {
	WIN_INVALID = 0,
	WIN_RECT,
	WIN_HANN,
	WIN_HAMMING,
	WIN_FLATTOP,
} win_type_t;

typedef struct fft_s {
	int bin_count;
	u32 ignore_dc;
	float *window;
	fftwf_plan plan;
	fftwf_complex *plan_in;
	fftwf_complex *plan_out;
	/* FFT mag² buffer */
	float *magbuf;
} phydet_fft_t, *phydet_fft_p; 

int phydet_fft_create(phydet_fft_p *fft, unsigned num_bins, win_type_t win_type, u32 ignore_dc);

int phydet_fft_execute(phydet_fft_p ctx, complex_t *samples, float *magbuf);
void phydet_fft_destroy(phydet_fft_p ctx);

#endif
