#ifndef DEF_CLOCK_RECOV_H_
#define DEF_CLOCK_RECOV_H_
#include <phydet/types.h>
#include <phydet/mmse_filter.h>

typedef struct {
	mmse_filter_f_t *interp;
	float omega;
	float omega_lim;
	float gain_omega;
	float gain_mu;
	int req_hist;
} clock_recov_t, *clock_recov_p;

/* \fn clock_recov_exec
 * \brief Create a clock recovery object. The function is based on gnuradio's M&M clock_recovery algorithm.
 * 
 * \param cp		Output clock recov pointer
 * \param omega		Oversampling rate
 * \param gain_omega	Frequency adjustment gain
 * \param gain_mu	Phase adjustment gain	
 * \param omega_relative_limit	Max frequency adjustment
 *
 * \return status
 */
int clock_recov_create(clock_recov_p *cp, float omega, float gain_omega, float gain_mu, float omega_relative_limit);

/* \fn clock_recov_destroy
 * \brief Destroy the clock recovery object
 *
 * \param	c 	Whom to destroy
 */
void clock_recov_destroy(clock_recov_p c);
/* \fn clock_recov_exec
 * \brief Clock recovery + binary slicing. 
 *	Its using a 8 taps mmse interpolator, and zero-crossing error estimation
 * 	Warning: The clock_recov object keeps no states. The clock_recov_exec function is meant
 *	to work on a single buffer of data. It will output (input_len-8)/mu items.
 * 
 * \param c		clock
 * \param output	output symbols
 * \param input		Input unsynchronized demodulated samples
 * \param input_len	Number of input samples
 * \return Number of output syms
 */
//int clock_recov_exec(clock_recov_p c, float mu, u8 *output, const float *input, u32 input_len);
int clock_recov_exec_(clock_recov_p c, float mu, u8 *output, const float *input, u32 input_len, float *out_dbg);

static inline int clock_recov_exec(clock_recov_p c, float mu, u8 *output, const float *input, u32 input_len)
{
	return clock_recov_exec_(c, mu, output, input, input_len, NULL);
}

#endif
