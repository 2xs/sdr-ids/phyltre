#ifndef DEF_POOL_H_
#define DEF_POOL_H_
#include <phydet/types.h>
#include <phydet/thread.h>

typedef struct sbuf_s {
	struct sbuf_s *next;
	unsigned refcnt;
	MUTEX_T mutex;
	u32 size;
	u64 time_ns;
	u64 flags;
	complex_t *samples;
	complex_t *fft;
	float *fft_mag;
} sbuf_t, *sbuf_p;

/* \fn sbuf_pool_init
 * \brief Initialize the global memory pool for power detector output.
 *
 * \param count		Number of chunks to allocate
 * \param size		Sample capacity of a chunk
 * \return		0 on success
 */
int sbuf_pool_init(size_t count, size_t size);

/* \fn sbuf_pool_destroy
 * \brief Destroy the global sbuf pool
 */
void sbuf_pool_destroy(void);

/* \fn sbuf_pool_alloc
 * \brief Allocate a shared buffer from the global pool.
 *	  The buffer must be relased with sbuf_release.
 * \return Allocated sbuf or NULL.
 */
sbuf_p sbuf_alloc(void);

/* \fn sbuf_addref 
 * \brief Increment the reference counter
 * 
 * \param sb	Shared buffer to release.
 */
void sbuf_addref(sbuf_p sb);

/* \fn sbuf_release 
 * \brief Decrement the reference counter, free the buffer to pool is no more refs.
 * 
 * \param sb	Shared buffer to release.
 */
void sbuf_release(sbuf_p sb);
void sbuf_fill(sbuf_p sb, complex_t *samples, complex_t *fft, float *fft_mag, u64 time_ns, u64 flags);
void sbuf_read(sbuf_p sb, complex_t *samples, complex_t *fft, float *fft_mag, u64 *time_ns, u64 *flags);
void sbuf_pool_dump_state(void);

#endif
