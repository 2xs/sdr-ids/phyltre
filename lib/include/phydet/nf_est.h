#ifndef DEF_NF_EST_
#define DEF_NF_EST_
#include <phydet/fft.h>
#include <phydet/rolavg.h>

typedef struct nfe_s {
	size_t fft_size;
	size_t win_size;
	size_t widx;
	//phydet_fft_p fft;
	rolavg_t avg;
	float *magbuf;
	float *medbuf;
	float *win;
} nfe_t, *nfe_p ;

int nfe_create(nfe_p *nfep, unsigned fft_size, unsigned win_count);
void nfe_destroy(nfe_p nfe);
//int nfe_execute(nfe_p nfe, complex_t *samples, size_t count);
int nfe_execute(nfe_p nfe, float *psd, size_t count);
float nfe_average(nfe_p nfe);
float nfe_median(nfe_p nfe);

#endif

