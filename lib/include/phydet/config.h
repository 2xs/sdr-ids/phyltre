#ifndef DEF_CONFIG_H_
#define DEF_CONFIG_H_
#include <phydet/phydef.h>
#include <phydet/fft.h>

typedef struct pwr_det_parm_s {
	win_type_t win_type;
	u32 fft_bin_count;
	u32 ignore_dc;
	char *log_path;
} pwr_det_parm_t;

/* Acquisition source configuration */
typedef enum {
	NF_AUTO,
	NF_FIXED
} nf_type_t;

typedef struct {
	char *name;
	char *driver;
	char *driver_args;
	double start_freq;
	double start_bw;
	double start_gain;
	nf_type_t nf_type;
	double noise_floor;
	int block_dc;
} acq_src_parm_t; 

phydef_t *config_load_phydef(const char *path);
void config_de_phydef(phydef_t *);

pwr_det_parm_t *config_load_pwr_det(const char *path);
void config_del_pwr_det(pwr_det_parm_t *conf);

acq_src_parm_t *config_load_acq_src(const char *path);
void config_del_acq_src(acq_src_parm_t *conf);

#endif
