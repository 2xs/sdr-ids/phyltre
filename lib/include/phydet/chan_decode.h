#ifndef DEF_CHAN_DECODE_H_
#define DEF_CHAN_DECODE_H_
#include <phydet/phydef.h>

struct chan_decode_s;

typedef struct chan_decode_intf_s {
	int (*create)(struct chan_decode_s *d, const gen_conf_t *conf);
	void (*destroy)(struct chan_decode_s *d);
	int (*execute)(struct chan_decode_s *c, double time_ns, const u8 *syms, size_t syms_count,
		tlv_field_t **metas, size_t *metas_count);
} chan_decode_intf_t;

typedef struct chan_decode_s {
	chan_decode_intf_t *intf;
	void *data;
	const phydef_chan_t *cdef;
} chan_decode_t, *chan_decode_p;

/* \fn chan_decode_create
 * \brief Create a channel decoder for given channel definition
 * 
 * \param cp	Output channel decoder pointer
 * \param cdef	Channel definition
 * \return	status
 */
int chan_decode_create(chan_decode_p *cp, const phydef_chan_t *cdef);

/* \fn chan_decode_execute
 * \brief 	Execute the channel decoder for given input symbols.
 * 
 * \param c		Channel decoder object
 * \param time_ns	Input timestamp in ns
 * \param syms		Input symbols
 * \param sym_count	Number of input symbols
 * \param metas		Output metadata list
 * \param metas_count	In: capacity of metadata list/ Out: number of metadatas
 * \return		status
 */
static inline int chan_decode_execute(chan_decode_p c, double time_ns, const u8 *syms, size_t syms_count,
		tlv_field_t **metas, size_t *metas_count)
{
	return c->intf->execute(c, time_ns, syms, syms_count, metas, metas_count);
}

/* \fn chan_decode_destroy
 * \brief 	Free allocated resources
 * \param c	Channel decode to destroy
 */
static inline void chan_decode_destroy(chan_decode_p c)
{
	c->intf->destroy(c);
	MEM_FREE(c);
}

#endif
