#ifndef DEF_MMSE_FILTER_H_
#define DEF_MMSE_FILTER_H_
#include <phydet/interpolator_taps.h>

typedef struct {
	float *in_buf;
	float *out_buf;
	unsigned nsteps;
	unsigned ntaps;
} mmse_filter_f_t, *mmse_filter_f_p;

int mmse_filter_f_create(mmse_filter_f_p *mp);
int mmse_filter_f_destroy(mmse_filter_f_p m);
float mmse_filter_f_interp(mmse_filter_f_p m, const float *input, float mu);

#endif
