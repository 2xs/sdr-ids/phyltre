#ifndef DEF_IEEE802154BB_H_
#define DEF_IEEE802154BB_H_
#include <phydet/types.h>
#include <phydet/dsp_util.h>
#include <phydet/crc.h>


int ieee802154_find_chip(u8 chip, const u8 *syms, size_t syms_count);
int ieee802154_decode_bytes(const u8 *bits, u8 *bytes, u8 bytes_count);
int ieee802154_find_sfd(const u8 *syms, size_t sym_count);

extern u32 ieee802154_crctbl[256];

static inline u32 ieee802154_calccrc(const u8 *buf, unsigned len)
{
	return crc_exec(ieee802154_crctbl, 0, buf, len);
}

#endif
