#ifndef DEF_BTLEBB_H
#define DEF_BTLEBB_H
#include <phydet/crc.h>

#define ADV_ACCESS_ADDR 0x8e89bed6u

/* Preamble window size: sync:8, addr:32, pdu_hdr:16 */
#define BTLEBB_DETECT_WINDOW (8+32+16)

/* Position of pdu header in detection window  */
#define BTLEBB_PDUHDR_OFFSET (8+32)

/* Maximum hamming distance in 40 bits preamble */
#define BTLEBB_DETECT_MAX_ERR 5u

typedef union phdr_u {
	struct {
		u8 type:4;
		u8 rfu:1;
		u8 chsel:1;
		u8 txadd:1;
		u8 rxadd:1;
		u8 len;
	};
	uint16_t u16;
} phdr_t ;

/* Required init for crc and whitening */
void btlebb_init(void);

/* (Un)whiten for given seed (ie: channel number)
   Returns next seed */
u8 btlebb_whiten(u8 seed, u8 *buf, unsigned len);

/* Detect preamble in symbol (binary) stream, returns offset or -1, fills phdr and err */
int btlebb_detect_adv_preamble(u32 channel_num, const u8 *syms, unsigned syms_count, phdr_t *phdr, unsigned *err);

/* Unpack bytes */
void btlebb_unpack(u8 *dst_bytes, const u8 *src_syms, u8 byte_count);

extern u32 btle_crctbl[256];

static inline u32 btlebb_calccrc(u32 init, const u8 *buf, unsigned len)
{
	return crc_exec(btle_crctbl, init, buf, len);
}


#endif
