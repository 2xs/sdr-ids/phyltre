#ifndef DEF_LOGGER_H_
#define DEF_LOGGER_H_
#include <phydet/phydef.h>
#include <phydet/tlv_file.h>

typedef struct chan_logger_s {
	const phydef_chan_t *cdef;
	tlv_file_logger_p fl;
} chan_logger_t, *chan_logger_p;

int chan_logger_create(chan_logger_p *lp, const phydef_chan_t *cdef);
void chan_logger_destroy(chan_logger_p l);
static inline int chan_logger_write_metas(chan_logger_p l,
	tlv_field_t **metas, size_t meta_count)
{
	return tlv_file_logger_write_metas(l->fl, metas, meta_count);
}

static inline int chan_logger_write(chan_logger_p l,
	void *data, size_t size)
{
	return tlv_file_logger_write(l->fl, data, size);
}

static inline int chan_logger_flush(chan_logger_p l)
{
	return tlv_file_logger_flush(l->fl);
}

#endif
