#ifndef DEF_CIRCBUF_H_
#define DEF_CIRCBUF_H_

typedef struct cbuf_s {
	unsigned char* buffer;
	int fd;
	size_t size;
	size_t head, tail;
} cbuf_t, *cbuf_p;

/* Num avail to read */
static inline size_t cbuf_avail(cbuf_p cb)
{
	return cb->tail - cb->head;
}

/* Num free to write */
static inline size_t cbuf_free(cbuf_p cb)
{
	return cb->size - cbuf_avail(cb);
}

/* Get read pointer */
static inline void *cbuf_head(cbuf_p cb)
{
	return &cb->buffer[cb->head];
}

/* Get write pointer */
static inline void *cbuf_tail(cbuf_p cb)
{
	return &cb->buffer[cb->tail];
}

/* Advance read pointer */
static inline int cbuf_get(cbuf_p cb, size_t sz)
{
	if (cbuf_avail(cb) < sz)
		return -1;
	cb->head += sz;
	if(cb->head >= cb->size)
	{
		cb->head -= cb->size;
		cb->tail -= cb->size;
	}
	return 0;
}

/* Advance write pointer */
static inline int cbuf_put(cbuf_p cb, size_t sz)
{
	if (cbuf_free(cb) < sz)
		return -1;
	cb->tail += sz;
	return 0;
}

static inline void cbuf_reset(cbuf_p cb)
{
	cb->head = cb->tail = 0;
}

int cbuf_create(cbuf_p *cbp, size_t size);
void cbuf_destroy(cbuf_p cb);
void cbuf_reset(cbuf_p cb);
int cbuf_write(cbuf_p cb, void *data, size_t sz);
int cbuf_read(cbuf_p cb, void *data, size_t sz);

#endif
