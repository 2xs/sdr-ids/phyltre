#ifndef DEF_QUEUE_H_
#define DEF_QUEUE_H_

typedef struct queue_s queue_t, *queue_p;

int queue_create(queue_p *q, size_t capacity);
size_t queue_size(queue_p q);
void queue_destroy(queue_p q);
int queue_get(queue_p q, void **elp);
int queue_timedget(queue_p q, void **elp, int timeout_ms);
int queue_tryput(queue_p q, void *el);
int queue_put(queue_p q, void *el);

#endif
