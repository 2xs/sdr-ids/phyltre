#!/bin/sh
OUT="/mnt/disk/out.cfil"
IN=/mnt/lora_samples

set -e

for d in $IN/*; do
	echo "Directory $d"
	./params.py $d
	mkdir -p $d/log
	phyltre &>$d/log/phyltre.log
	chan_extract -p -a -c "LoRa/EU863_x4" -o $d/log/adapt.iq $OUT
	cp $OUT $d/log/
done
