#!/usr/bin/env python3
import sys
import pickle
import json
OUT="/mnt/disk/out.cfil"

def main():
	path = sys.argv[1]
	with open(path+"/params.p","rb") as f: 
		dat = pickle.load(f)
		rf = dat['RF']
		rx, = dat['recorders']
	#	print(dat)
		print("#File %s: "%path)
		print("#Transmit param: freq=%.2fMHz, bw=%.fKhz, sf=%d, cr=%s"%(rf['freq']/1e6,rf['bw']/1e3, rf['sf'], rf['cr']))
		rxfreq = rf['freq']+rx['RECORDINGOFFSET']
		print("#RX params: freq=%.2fMhz, bw=%.2fMhz"%(rxfreq, rx['samplerate']))

		with open("conf/acq_src.ini", "w") as fc:
			fc.write("[ACQSource]\n")
			fc.write("name=ACQ SRC\n")
			fc.write("driver=file\n")
			fc.write("driverArgs=%s/record0.dat\n"%path)
			fc.write("startBandwidth=%e\n"%rx['samplerate'])
			fc.write("startFrequency=%e\n"%rxfreq)
			fc.write("noiseFloor=auto\n")
			fc.close()

		with open("conf/pwr_det.ini", "w") as fp:
			fp.write("[FreqDomainDetector]\n");
			fp.write("fftBins=1024\n");
			fp.write("windowType=rectangular\n");
			fp.write("ignoreDC=true\n");
			fp.write("logFile=%s\n"OUT);
			fp.close()
	


if __name__ == '__main__': main()
