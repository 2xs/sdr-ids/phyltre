#!/usr/bin/python
import numpy as np
import sys

def randc(mean, std, num):
	return np.array( np.random.normal(mean,std,num)
			 + 1j*np.random.normal(mean,std,num),
			dtype = np.complex64)

def usage():
	sys.stderr.write("Usage: %s <sizeMSamples\n")
	sys.exit(1)

def main():
	BSIZE=8192
	if len(sys.argv)!=2:
		usage()
	l = int(sys.argv[1]) * 1024*1024
	for i in xrange(l/BSIZE):
		buf = randc(0,0.2,BSIZE)
		buf.tofile(sys.stdout)

if __name__ == '__main__':
	main()
